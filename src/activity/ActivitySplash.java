package activity;

import ui.splash.UISplash;

import com.azumio.android.common.Log;
import com.utils.VovaActivity;

import android.app.Activity;
import android.os.Bundle;

public class ActivitySplash extends VovaActivity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		
		super.onCreate(savedInstanceState);
		
		setContentView(new UISplash(this));

	}
	

}
