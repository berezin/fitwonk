package activity;

import java.util.HashMap;

import ui.main.UIMain;
import ui.main.UIMainContent;
import ui.main.screens.UIAbstractScreen;
import ui.main.screens.UIScreenCalendar;
import ui.main.screens.UIScreenDescription;
import ui.main.screens.UIScreenDialog;
import ui.main.screens.UIScreenMyDay;
import ui.main.screens.UIScreenSettings;
import ui.main.screens.UIScreenTargets;
import ui.main.screens.UIScreenDialogs;
import ui.main.screens.UIScreenTraining;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import data.DataDialogs.DataDialog;
import data.DataExercise;
import fitwonk.Api;

import com.utils.VovaActivity;
import com.utils.shorts.E;

public class ActivityMain extends VovaActivity
{

	public HashMap<Class, View> cache = new HashMap<>();

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent data)
	{

		super.onActivityResult(requestCode, resultCode, data);

		if(Activity.RESULT_OK == resultCode)
		{
			Api.Settings.handleGalleryResult(requestCode, data);
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{

		super.onCreate(savedInstanceState);

		setContentView(new UIMain(this));

		((ViewGroup) getWindow().getDecorView()).addView(new SurfaceView(this),
				new ViewGroup.LayoutParams(0, 0));

		Api.onAppStart();
		

	}

	static public void ToMain(Context context)
	{

		View view = ActivityMain.getInstance(context, UIScreenMyDay.class);

		E.Event(UIMainContent.REQUEST_SETUP, new UIMainContent.DataSetup(view));

	}

	static public void ToCalendar(Context context)
	{

		View view = ActivityMain.getInstance(context, UIScreenCalendar.class);

		E.Event(UIMainContent.REQUEST_SETUP, new UIMainContent.DataSetup(view));

	}

	static public void ToTraining(Context context)
	{

		View view = ActivityMain.getInstance(context, UIScreenTraining.class);

		E.Event(UIMainContent.REQUEST_SETUP, new UIMainContent.DataSetup(view));

	}

	static public void ToSettings(Context context)
	{

		View view = ActivityMain.getInstance(context, UIScreenSettings.class);

		E.Event(UIMainContent.REQUEST_SETUP, new UIMainContent.DataSetup(view));

	}

	static public void ToDialogs(Context context)
	{

		View view = ActivityMain.getInstance(context, UIScreenDialogs.class);

		E.Event(UIMainContent.REQUEST_SETUP, new UIMainContent.DataSetup(view));

	}

	static public void ToTargets(Context context)
	{

		View view = ActivityMain.getInstance(context, UIScreenTargets.class);

		E.Event(UIMainContent.REQUEST_SETUP, new UIMainContent.DataSetup(view));

	}

	static public void ToDialog(Context context, DataDialog dialog)
	{

		View view = ActivityMain.getInstance(context, UIScreenDialog.class,
				dialog);

		E.Event(UIMainContent.REQUEST_SETUP, new UIMainContent.DataSetup(view));

	}

	static public void ToDescription(Context context, DataExercise ex)
	{

		View view = ActivityMain.getInstance(context, UIScreenDescription.class,
				ex);

		E.Event(UIMainContent.REQUEST_SETUP, new UIMainContent.DataSetup(view));

	}

	@SuppressWarnings("rawtypes")
	public static View getInstance(Context context, Class cl)
	{
		return getInstance(context, cl, null);
	}

	@SuppressWarnings("rawtypes")
	public static View getInstance(Context context, Class cl, Object data)
	{

		ActivityMain activity = (ActivityMain) context;
		View v = activity.cache.get(cl);

		if(v == null)
		{

			try
			{
				v = (View) cl.getConstructors()[0].newInstance(activity);

				activity.cache.put(cl, v);

			} catch (Exception e)
			{
				v = new View(activity);
				e.printStackTrace();
			}

		}

		if(v instanceof UIAbstractScreen)
		{
			((UIAbstractScreen) v).Set(data);
		}

		return v;
	}

	@Override
	public void handle(Integer event, Object param)
	{
		super.handle(event, param);

		if(Api.Init.hasOKEvent(event))
		{
			cache.clear();
		}

	}
}
