package ui.common;

import com.utils.Dimen;
import com.utils.shorts.LPR;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import common.Paints;
import common.Palette;
import common.Type;
import drawable.DrawableProgress;

public class UIProgressAndTryAgain extends RelativeLayout
{

	View progress;
	TextView textTry;
	
	public UIProgressAndTryAgain(Context context)
	{
		super(context);
		
		progress = new View(getContext());
		DrawableProgress.Set(progress, Palette.BLUE);
		addView(progress, LPR.create(Dimen.d40, Dimen.d40).center().get());
		
		textTry = new TextView(getContext());
		textTry.setBackgroundColor(Palette.BLUE);
		Paints.SetText(textTry, Type.Futura, 14, 0xffffffff);
		textTry.setPadding(Dimen.d20, Dimen.d12, Dimen.d20, Dimen.d12);
		textTry.setText("ПОВТОРИТЬ");
		addView(textTry, LPR.createWrap().center().get());
	}
	
	public void setProgress() {
		progress.setVisibility(VISIBLE);
		textTry.setVisibility(INVISIBLE);
	}
	
	public void setTryAgain(OnClickListener listener) {
		progress.setVisibility(INVISIBLE);
		textTry.setVisibility(VISIBLE);
		
		textTry.setOnClickListener(listener);
	}

}
