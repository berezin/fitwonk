package ui.common;

import com.utils.EventsUtils.BindedRelativeLayout;

import android.content.Context;

public abstract class SmartRelativeLayout extends BindedRelativeLayout
{

	public SmartRelativeLayout(Context context)
	{
		super(context);
		
	}
	
	
	int w_ = 0;
	int h_ = 0;
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{

		int w = MeasureSpec.getSize(widthMeasureSpec);
		int h = MeasureSpec.getSize(heightMeasureSpec);
		
		if(w != w_ || h != h_) {
			w_ = w;
			h_ = h;
			
			onSetSize(w, h);
		}
		
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
	}
	
	abstract protected void onSetSize(int width, int height);
	

}
