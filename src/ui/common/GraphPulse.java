package ui.common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;
import common.Palette;
import com.utils.Dimen;

public class GraphPulse extends View
{
	static final int maxPoints = 100;
	Path chartPath = new Path();
	private int height = 1;
	long lastInvalidation = 0L;
	float max;
	float min;
	Paint p = new Paint();
	public int pos = 99;
	private int width = 1;
	public int xM = 100;
	public float xMinMaxRange = this.xrange;
	public int xm = 0;
	public float[] xpoints = new float[100];
	public float xrange = 3.0f;
	public int yM = 100;
	public int ym = 0;
	public float[] ypoints = new float[100];

	public GraphPulse(Context paramContext)
	{
		super(paramContext);
		init();
	}

	private void init()
	{
		setBackgroundColor(0xffffffff);
		this.p.setColor(Palette.BLUE);
		this.p.setAntiAlias(true);
		this.p.setStrokeWidth(Dimen.d3);
		//this.p.setStyle(Paint.Style.STROKE);
	}

	
	boolean hasChanges = true;
	public void addPoint(long paramLong, float paramFloat)
	{
		System.arraycopy(this.xpoints, 0, this.xpoints, 1,
				-1 + this.xpoints.length);
		System.arraycopy(this.ypoints, 0, this.ypoints, 1,
				-1 + this.ypoints.length);
		this.ypoints[0] = paramFloat;
		this.xpoints[0] = ((float) paramLong / 1000.0F);
		hasChanges = true;
		invalidate();
	}

	void calcMinMax()
	{
		float f1 = this.ypoints[0];
		this.max = f1;
		this.min = f1;

		for (int i = 0; i < 100; i++)
		{


			
			if(this.ypoints[i] < this.min)
				this.min = this.ypoints[i];
			if(this.ypoints[i] > this.max)
				this.max = this.ypoints[i];

		}
	}

	public void clear()
	{
		for (int i = 0; i < 100; i++)
		{
			this.ypoints[i] = 0.0F;
			this.xpoints[i] = 0.0F;
		}
		
		hasChanges = true;
	}

	
	float getPointY(int i) {
		return this.height - (this.ypoints[i] - this.min)
				/ (this.max - this.min) * (this.height - Dimen.d10);
	}
	
	float[] points = new float[500];
	int count = 0;
	protected void onDraw(Canvas paramCanvas)
	{

		paramCanvas.drawColor(0xffffffff);
		
		float f2 = -Dimen.d15 + this.width;
		float f3 = getPointY(0);
		
		
		
		if(hasChanges) {
			hasChanges = false;
			float f1 = this.xpoints[0];
			calcMinMax();
			//this.chartPath.reset();
			//this.chartPath.moveTo(f2, f3);
			float f5 = 0f;
			float f6 = 0f;
			
			count = 0;
			float lastX = f2;
			float lastY = f3;
			
			for (int i = 1; i < 100; i++)
			{
				float f4 = f1 - this.xpoints[i];

				if(f4 > this.xrange)
				{
					break;
				}

				f5 = this.width - f4 * this.width / this.xrange
						- Dimen.d15;
				
				f6 = getPointY(i);
				
				points[count++] = lastX;
				points[count++] = lastY;
				points[count++] = f5;
				points[count++] = f6;
				
				lastX = f5;
				lastY = f6;
				
				//this.chartPath.lineTo(f5, f6);

				if(f5 <= 0f) {
					break;
				}

			}
			
			//this.chartPath.lineTo(f5, this.height);
			//this.chartPath.lineTo(f2, this.height);
			
		}
		p.setStyle(Paint.Style.STROKE);
		//paramCanvas.drawPath(this.chartPath, this.p);
		paramCanvas.drawLines(points, 0, count, this.p);
		p.setStyle(Paint.Style.FILL);
		paramCanvas.drawCircle(f2, f3, Dimen.d4, this.p);

	}

	protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2,
			int paramInt3, int paramInt4)
	{
		this.width = (paramInt3 - paramInt1);
		this.height = (paramInt4 - paramInt2);
	}
}
