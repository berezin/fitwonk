package ui.common;

import java.io.IOException;
import com.azumio.instantheartrate.dsp.BeatListener;
import com.azumio.instantheartrate.dsp.HeartBeat;
import com.liqimai.heart.ImageProcessing;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.RelativeLayout;
import com.utils.shorts.LPR;
import com.utils.VovaActivity;
import com.utils.VovaActivity.IActivityHandler;

@SuppressWarnings("deprecation")
public abstract class UIAbstractHRPulseView extends RelativeLayout implements
		IActivityHandler, SurfaceHolder.Callback, Camera.PreviewCallback
{

	private Camera camera;
	private SurfaceHolder surfaceHolder;
	private SurfaceView preview;
	private HeartBeat hb;
	private Boolean isActive = false;
	private Boolean hasResult = false;
	
	private Boolean isFake = false;
	private Integer fakePulse = 0;
	public void setFakeResult(Integer pulse) {
		
		isFake = true;
		fakePulse = pulse;
	}
	
	public boolean hasResult()
	{
		return hasResult;
	}

	public abstract void onSetText(String str);

	public abstract void onSetState(boolean active);

	public abstract void onSetPulse(Integer pulse);

	public abstract void onSetGraph(Long l, Float f);

	public abstract void onBeat();

	public abstract void onGraphClear();

	public abstract void onProgress(int progress);

	public UIAbstractHRPulseView(Context context)
	{
		super(context);

		// setBackgroundColor(0xffff0000);

		preview = new SurfaceView(getContext());
		addView(preview, LPR.create(1, 1).get());
		preview.setVisibility(VISIBLE);
		surfaceHolder = preview.getHolder();
		surfaceHolder.addCallback(this);
		surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		
	}

	final public void Restart()
	{

		Activate();
	}

	void Activate()
	{
		if(!isActive)
		{

			if(isFake) {
				isActive = true;
				hasResult = true;
				onSetPulse(fakePulse);
				onSetText(String.valueOf(fakePulse));
				onProgress(100);
				Deactivate();
				return;
			}
			
			
			hasResult = false;

			onSetText("--");
			onProgress(0);
			onSetPulse(0);
			onGraphClear();

			hb = new HeartBeat(preview, getContext());

			hb.running = true;

			hb.setBPMListener(new BeatListener()
			{

				@Override
				public void onValidatedRR(long arg0, int arg1)
				{
					// Log.w("Pulse", "hb onValidatedRR "+arg0+" "+arg1+" ");
				}

				@Override
				public void onValidRR(long arg0, int arg1)
				{
					// Log.w("Pulse", "hb onValidRR "+arg0+" "+arg1+" ");
				}

				@Override
				public void onSample(long arg0, long arg1, float arg2,
						float arg3)
				{
					onSetGraph(arg0, arg2);
				}

				@Override
				public void onHBStop()
				{

				}

				@Override
				public void onHBStart()
				{

				}

				@Override
				public void onHBError()
				{

				}

				@Override
				public void onCameraError(Exception arg0, Parameters arg1)
				{

				}

				@Override
				public void onBeat(int arg0, int arg1)
				{
					if(arg0 != 0)
					{

						UIAbstractHRPulseView.this.onBeat();
						onSetPulse(arg0);
						onSetText(String.valueOf(arg0));
						onProgress(arg1 * 100 / 12);

						if(arg1 >= 12)
						{

							hasResult = true;
							Deactivate();
						}

					} else if(arg1 == 0)
					{

						onProgress(0);
						onSetText("--");

					}

				}
			});

			camera = Camera.open();
			hb.c = camera;
			hb.cameraOpen = true;
			
			

			if(isCreated)
			{
				setSurface(surfaceHolder);
			}

			isActive = true;
			onSetState(true);
			
			
			
		}
	}

	void Deactivate()
	{

		if(isActive)
		{

			if(camera != null)
			{
				camera.setPreviewCallback(null);
				camera.stopPreview();
				camera.release();
				camera = null;
				
			}
			
			if(hb != null) {
				
				hb.oldCameraApi = true;
				hb.cameraOpen = false;
				hb.c = null;
				hb.setBPMListener(null);
				hb = null;
			}

			isActive = false;
			onSetState(false);
		}
	}

	@Override
	protected void onAttachedToWindow()
	{
		super.onAttachedToWindow();
		VovaActivity.AddHandler(getContext(), this);
		
		postDelayed(new Runnable()
		{

			@Override
			public void run()
			{
				Activate();
			}
		}, 200);

	}

	@Override
	protected void onDetachedFromWindow()
	{
		super.onDetachedFromWindow();
		VovaActivity.RemoveHandler(getContext(), this);
		Deactivate();
	}

	@Override
	public void onDestroy()
	{

	}

	@Override
	public void onResume()
	{

	}

	@Override
	public void onStart()
	{

	}

	@Override
	public void onPause()
	{
		Deactivate();
	}

	@Override
	public void onStop()
	{

	}

	@Override
	public boolean isBlockBack()
	{

		return false;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height)
	{

	}

	boolean isCreated = false;

	public void surfaceCreated(SurfaceHolder holder)
	{
		isCreated = true;

	}

	public void setSurface(SurfaceHolder holder)
	{

		try
		{
			camera.setPreviewDisplay(holder);
			camera.setPreviewCallback(this);
		} catch (IOException e)
		{
			e.printStackTrace();
		}

		Camera.Parameters cameraParam = camera.getParameters();
		cameraParam.setPreviewFormat(ImageFormat.NV21);
		camera.setDisplayOrientation(90);
		camera.setParameters(cameraParam);
		cameraParam = camera.getParameters();
		cameraParam.setFlashMode(Parameters.FLASH_MODE_TORCH);
		camera.setParameters(cameraParam);
		camera.startPreview();

		// camera.startPreview();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder)
	{

	}

	int pointMatrixSize = 5;
	int pointsSize = pointMatrixSize * pointMatrixSize;
	int pixels[] = new int[pointsSize];
	int rgb[] = null;

	Long lastSearchRed = 0L;
	Long delaySearchRed = 500L;
	boolean isRed = false;

	public void onPreviewFrame(byte[] data, Camera camera)
	{

		Long time = System.currentTimeMillis();

		if(time - lastSearchRed > delaySearchRed)
		{

			lastSearchRed = time;

			int frameHeight = camera.getParameters().getPreviewSize().height;
			int frameWidth = camera.getParameters().getPreviewSize().width;

			if(rgb == null)
			{

				rgb = new int[pointsSize];

				int stepX = frameWidth / (2 * (pointMatrixSize + 1));
				int stepY = frameHeight / (2 * (pointMatrixSize + 1));
				int offsetX = frameWidth / 4;
				int offsetY = frameHeight / 4;

				int point = 0;
				for (int j = 0; j < pointMatrixSize; j++)
				{

					for (int i = 0; i < pointMatrixSize; i++)
					{

						pixels[point++] = offsetX + i * stepX
								+ (offsetY + j * stepY) * frameWidth;

					}

				}

			}

			ImageProcessing.decodeYUV(rgb, pixels, data, frameWidth,
					frameHeight);

			for (int i = 0; i < pointsSize; i++)
			{

				int c = rgb[i];

				int r = Color.red(c);
				int g = Color.green(c);
				int b = Color.blue(c);

				if(r - 20 < b || r - 20 < g)
				{
					
					onSetPulse(0);
					onSetText("--");
					onProgress(0);
					onGraphClear();
					
					if(isRed)
					{
						hb.getHrProcessor().reset();
					}

					isRed = false;
					return;
				}
			}

			isRed = true;

		}

		if(isRed)
		{
			hb.onPreviewFrame(data, camera);
		} else
		{

		}
	}

}
