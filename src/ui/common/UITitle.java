package ui.common;

import common.Paints;
import common.Res;
import common.Type;

import com.utils.CanvasUtils;
import com.utils.Dimen;
import com.utils.EventsUtils.BindedRelativeLayout;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.text.Layout.Alignment;
import android.text.StaticLayout;

public class UITitle extends BindedRelativeLayout
{

	static Bitmap bLeft;
	static Bitmap bRight;
	static Bitmap bFill;
	
	static public Integer Height = Dimen.d60;
	static public Integer HeightImage = Dimen.d30;

	StaticLayout sl;

	public UITitle(Context context)
	{
		super(context);
		
		if(bLeft == null) {
			
			bLeft= BitmapFactory.decodeResource(getResources(), Res.IMG_TITLE_LEFT);
			bRight= BitmapFactory.decodeResource(getResources(), Res.IMG_TITLE_RIGHT);
			bFill= BitmapFactory.decodeResource(getResources(), Res.IMG_TITLE_FILL);
		}

	}
	
	
	
	public void Set(String strTitle) {
		sl = new StaticLayout(strTitle, Paints.GetPaint(Type.Futura, 20, 0xff000000), 1000, Alignment.ALIGN_NORMAL, 1f, 0f, false);
	}

	
	@Override
	protected void dispatchDraw(Canvas canvas)
	{
		if(sl!=null)
		{
			float wTxt = sl.getLineWidth(0);
			float hTxt = sl.getHeight();
			float h = Height;
			int w = getMeasuredWidth();
			
			float padImg = Dimen.d5;
			int hImg = HeightImage;
			int wImgLeft = bLeft.getWidth() * hImg / bLeft.getHeight();
			int xImgLeft = (int) ((w - wTxt) / 2 - padImg - wImgLeft);
			int yImg = (int) ((h - hImg) / 2);
			
			int wImgRight = bRight.getWidth() * hImg / bRight.getHeight();
			int xImgRight = (int) ((w + wTxt) / 2 + padImg);
			
			canvas.save();
			canvas.translate( (w - wTxt) / 2, (h - hTxt) / 2);
			sl.draw(canvas);
			canvas.restore();
			
			
			CanvasUtils.DrawImage(canvas, bLeft, new Rect(xImgLeft, yImg, xImgLeft + wImgLeft, yImg + hImg), true);
			
			CanvasUtils.DrawImageXY(canvas, bFill, new Rect(0, yImg, xImgLeft, yImg + hImg), true);
			
			CanvasUtils.DrawImage(canvas, bRight, new Rect(xImgRight, yImg, xImgRight + wImgRight, yImg + hImg), true);
			
			CanvasUtils.DrawImageXY(canvas, bFill, new Rect(xImgRight + wImgRight, yImg, w, yImg + hImg), true);
			
		}
		
		
		super.dispatchDraw(canvas);
	}


	@Override
	public void handle(Integer event, Object param)
	{
		
	}

}
