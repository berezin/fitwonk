package ui.common;

import java.util.ArrayList;

import com.utils.AnimationUtils.ResizeAnimation;
import com.utils.Dimen;
import com.utils.Utils;
import com.utils.shorts.LPL;
import com.utils.shorts.LPR;
import com.utils.shorts.Q;
import com.utils.ui.ViewRelativeLayout;

import android.animation.Animator;
import android.animation.LayoutTransition;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import common.painter.ParentRelativeLayout;
import common.painter.ParentViewLayout;

public class UISectionList extends RelativeLayout
{

	
	public static class Adapter {
		
		ArrayList<UIItem> list;
		ArrayList<Integer> opened;
		
		public Adapter(ArrayList<UIItem> list, ArrayList<Integer> opened) {
			this.list = list;
			this.opened = opened;
			
			if(this.opened == null) {
				this.opened = new ArrayList<>();
			}
		}
		
	}
	
	public static class UISectionItem extends ParentRelativeLayout implements UIItem, OnClickListener {

		boolean isOpen = false;
		boolean isNeedOpen = true;
		
		public UISectionItem(Context context)
		{
			super(context);
			setOnClickListener(this);
			
		}
		
		public void onOpen(boolean isOpen) {
			
		}
		
		public void setOpening(boolean isNeedOpen) {
			this.isNeedOpen = isNeedOpen;
		}


		@Override
		public void onClick(View v)
		{
			
			if(!isNeedOpen) {
				return;
			}
			
			int index = list.indexOf(this);
			
			isOpen = !isOpen;
			
			onOpen(isOpen);
			
			int visibility = isOpen? VISIBLE: GONE;
			
			for(int i = index + 1; i < list.size(); i++) {
				
				UIItem ui = list.get(i);
				
				if(ui instanceof UISectionItem) {
					break;
				}
				
				((View)ui).setVisibility(visibility);
				
				if(isOpen)
					((UISubItem) ui).onShow();
				else
					((UISubItem) ui).onHide();
				
			}
			
			
		}

		
		@Override
		public Integer Height()
		{
			return Dimen.d50;
		}
		

		ArrayList<UIItem> list;

		@Override
		public void AllItems(ArrayList<UIItem> list)
		{
			this.list = list;
		}

		@Override
		public int FixWidth()
		{
			return (int) Q.getW();
		}

		@Override
		public int FixHeight()
		{
			
			return Height();
		}
	}
	
	public static class UISectionItemView extends ParentViewLayout implements UIItem, OnClickListener {

		boolean isOpen = false;
		boolean isNeedOpen = true;
		
		public UISectionItemView(Context context)
		{
			super(context);
			setOnClickListener(this);
			
		}
		
		public void onOpen(boolean isOpen) {
			
		}
		
		public void setOpening(boolean isNeedOpen) {
			this.isNeedOpen = isNeedOpen;
		}


		@Override
		public void onClick(View v)
		{
			
			if(!isNeedOpen) {
				return;
			}
			
			int index = list.indexOf(this);
			
			isOpen = !isOpen;
			
			onOpen(isOpen);
			
			int visibility = isOpen? VISIBLE: GONE;
			
			for(int i = index + 1; i < list.size(); i++) {
				
				UIItem ui = list.get(i);
				
				if(ui instanceof UISectionItem || ui instanceof UISectionItemView) {
					break;
				}
				
				((View)ui).setVisibility(visibility);
				
				if(ui instanceof UISubItemView) {
					if(isOpen)
						((UISubItemView) ui).onShow();
					else
						((UISubItemView) ui).onHide();
				}
				else if(ui instanceof UISubItem) {
					if(isOpen)
						((UISubItem) ui).onShow();
					else
						((UISubItem) ui).onHide();
				}
				
				
				
			}
			
			
		}

		
		@Override
		public Integer Height()
		{
			return Dimen.d50;
		}
		

		ArrayList<UIItem> list;

		@Override
		public void AllItems(ArrayList<UIItem> list)
		{
			this.list = list;
		}

		@Override
		public int FixWidth()
		{
			return (int) Q.getW();
		}

		@Override
		public int FixHeight()
		{
			
			return Height();
		}
	}
	
	public static class UISubItem extends ParentRelativeLayout implements UIItem {

		public UISubItem(Context context)
		{
			super(context);
		}
		
		public void onShow() {
			
		}
		
		public void onHide() {
			
		}

		@Override
		public int FixWidth()
		{
			return (int) Q.getW();
		}

		@Override
		public int FixHeight()
		{
			
			return Height();
		}

		@Override
		public Integer Height()
		{
			return Dimen.d50;
		}
		
		ArrayList<UIItem> list;

		@Override
		public void AllItems(ArrayList<UIItem> list)
		{
			this.list = list;
		}
	}
	
	public static class UISubItemView extends ParentViewLayout implements UIItem {

		public UISubItemView(Context context)
		{
			super(context);
		}
		
		public void onShow() {
			
		}
		
		public void onHide() {
			
		}

		@Override
		public int FixWidth()
		{
			return (int) Q.getW();
		}

		@Override
		public int FixHeight()
		{
			
			return Height();
		}

		@Override
		public Integer Height()
		{
			return Dimen.d50;
		}
		
		ArrayList<UIItem> list;

		@Override
		public void AllItems(ArrayList<UIItem> list)
		{
			this.list = list;
		}
	}
	
	public static class UISubItemWithDesc extends UISubItem implements OnClickListener {

		
		protected boolean isMin = true;
		
		public UISubItemWithDesc(Context context)
		{
			super(context);
	
			setOnClickListener(this);
			
		}
		
		public void onOpen() {
			
		}
		
		public void onClose() {
			
		}
		
		@Override
		public int FixHeight()
		{
			if(isMin)
				return Height();
			else 
				return HeightDesc();
		}

		public Integer HeightDesc()
		{
			return Dimen.d150;
		}

		@Override
		public void onClick(View v)
		{
			isMin = !isMin;
			
			if(isMin) {
				onClose();
				setLayoutParams(LPL.create(Height()).get());
				
			}
			else {
				
				onOpen();
				setLayoutParams(LPL.create(HeightDesc()).get());
				
			}
		}
		
	}
	
	public static class UISubItemWithDescView extends UISubItemView implements OnClickListener {

		
		protected boolean isMin = true;
		
		public UISubItemWithDescView(Context context)
		{
			super(context);
	
			setOnClickListener(this);
		}
		
		public void onOpen() {
			
		}
		
		public void onClose() {
			
		}
		
		@Override
		public int FixHeight()
		{
			if(isMin)
				return Height();
			else 
				return HeightDesc();
		}

		public Integer HeightDesc()
		{
			return Dimen.d150;
		}

		@Override
		public void onClick(View v)
		{
			isMin = !isMin;
			
			if(isMin) {
				onClose();
				
				//clearAnimation();
				//ResizeAnimation anim = new ResizeAnimation(this, Height());
				//anim.start();
				
				setLayoutParams(LPL.create(Height()).get());
				requestLayout();
			}
			else {
				
				onOpen();
				
				//clearAnimation();
				//ResizeAnimation anim = new ResizeAnimation(this, HeightDesc());
				//anim.start();
				
				setLayoutParams(LPL.create(HeightDesc()).get());
				requestLayout();
			}
		}
		
	}
	
	public static abstract interface UIItem {
		public Integer Height();
		public void AllItems(ArrayList<UIItem> list);
	}

	
	
	ScrollView scroll;
	LinearLayout linear;
	Adapter adapter;
	static public boolean IS_LOCKED_SWITCH = false;
	
	public UISectionList(Context context)
	{
		super(context);
		
		scroll = new ScrollView(getContext()) {
			@Override
			public boolean onInterceptTouchEvent(MotionEvent arg0)
			{

				try
				{
					if(!IS_LOCKED_SWITCH)
					{
						return super.onInterceptTouchEvent(arg0);
					} else
						return false;
				} catch (Exception e)
				{
					return false;
				}
			}

			@Override
			public boolean onTouchEvent(MotionEvent arg0)
			{

				if(!IS_LOCKED_SWITCH)
				{
					return super.onTouchEvent(arg0);
				} else
					return false;
			}
		};
		addView(scroll, LPR.create().get());
		
		linear = new LinearLayout(getContext());
		linear.setOrientation(1);
		
		if(Utils.getOsVersion() > 10)
		{
			
			LayoutTransition lt = new LayoutTransition();
			lt.setDuration(200);
			
			lt.enableTransitionType(LayoutTransition.APPEARING);
			lt.enableTransitionType(LayoutTransition.CHANGE_APPEARING);
			lt.enableTransitionType(LayoutTransition.CHANGE_DISAPPEARING);
			lt.enableTransitionType(LayoutTransition.CHANGING);
			lt.enableTransitionType(LayoutTransition.DISAPPEARING);
			
			linear.setLayoutTransition(lt);
		}
		
		scroll.addView(linear);

	}
	
	
	
	
	public void Set(Adapter adapter) {
		this.adapter = adapter;
		
		linear.removeAllViews();
		
		Integer index = -1;
		boolean isOpen = false;
		
		for(UIItem v : adapter.list) {
			
			if(v instanceof UISectionItem) {
				index ++;
				
				isOpen = adapter.opened.contains(index);
					
				((UISectionItem)v).isOpen = isOpen;
				
			}
			else if(v instanceof UISectionItemView) {
				index ++;
				
				isOpen = adapter.opened.contains(index);
					
				((UISectionItemView)v).isOpen = isOpen;
				
			}
			else {
				
				
				((View)v).setVisibility(isOpen?VISIBLE:GONE);
				
				if(v instanceof UISubItem)
				{
					if(isOpen)
						((UISubItem) v).onShow();
					else
						((UISubItem) v).onHide();
					
				}
				else if(v instanceof UISubItemView)
				{
					if(isOpen)
						((UISubItemView) v).onShow();
					else
						((UISubItemView) v).onHide();
					
				}
				
			}
			
			v.AllItems(adapter.list);
			linear.addView(((View)v), LPL.create(v.Height()).get());
		}
		
	}

}
