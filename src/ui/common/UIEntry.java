package ui.common;

import com.utils.Dimen;

import android.content.Context;
import android.widget.EditText;
import common.Paints;
import common.Type;

public class UIEntry extends EditText
{

	public UIEntry(Context context)
	{
		super(context);
		
		Paints.SetText(this, Type.Futura, 14, 0xff000000);
		setHintTextColor(0x44000000);
		setBackgroundColor(0x11000000);
		setPadding(Dimen.d20, Dimen.d12, Dimen.d20, Dimen.d12);
		setSingleLine(true);
		
	}

}
