package ui.main.screens;

import com.utils.VovaActivity;
import com.utils.VovaActivity.IActivityHandler;

import android.content.Context;
import common.Palette;
import ui.common.SmartRelativeLayout;

public abstract class UIAbstractScreen extends SmartRelativeLayout implements IActivityHandler
{

	public UIAbstractScreen(Context context)
	{
		super(context);
		
		super.setBackgroundColor(Palette.GRAY_BACKGROUND);
	}
	
	@Override
	public void setBackgroundColor(int color)
	{

	}
	
	@Override
	protected void onAttachedToWindow()
	{
		
		super.onAttachedToWindow();
		VovaActivity.AddHandler(getContext(), this);
	}
	
	@Override
	protected void onDetachedFromWindow()
	{
		
		super.onDetachedFromWindow();
		VovaActivity.RemoveHandler(getContext(), this);
	}
	
	

	@Override
	public void onDestroy()
	{
		
	}

	@Override
	public void onStart()
	{
		
	}

	@Override
	public void onStop()
	{
		
	}

	@Override
	public void onResume()
	{
		
	}

	@Override
	public void onPause()
	{
		
	}

	@Override
	public boolean isBlockBack()
	{
		
		return false;
	}

	public void Set(Object data) {
		
	}
}
