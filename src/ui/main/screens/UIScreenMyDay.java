package ui.main.screens;

import com.utils.shorts.LPR;

import com.utils.Dimen;
import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import ui.common.UITitle;
import ui.main.screens.myday.UICalendarInfo;
import ui.main.screens.myday.UITrainerInfo;

public class UIScreenMyDay extends UIAbstractScreen
{
	ScrollView scroll;
	RelativeLayout frameContent;
	UICalendarInfo uiCalendar;
	UITitle uiTitle;
	UITrainerInfo uiTrainer;

	public UIScreenMyDay(Context context) {
		super(context);
		
		scroll = new ScrollView(getContext());
		addView(scroll, LPR.create().marginTop(UITitle.Height).get());
		
		frameContent = new RelativeLayout(getContext());
		scroll.addView(frameContent, LPR.createMW().get());
		
		uiTitle = new UITitle(getContext());
		uiTitle.Set("МОЙ ДЕНЬ");
		addView(uiTitle, LPR.create(UITitle.Height).get());
		
		uiCalendar = new UICalendarInfo(getContext());
		uiCalendar.setId(1);
		frameContent.addView(uiCalendar, LPR.createMW().get());
		//uiEating= new UIEatingInfo(getContext());
		//addView(uiEating);
		uiTrainer= new UITrainerInfo(getContext());
		frameContent.addView(uiTrainer, LPR.createMW().below(1).marginTop(Dimen.d4).get());
		
	}

	@Override
	public void handle(Integer event, Object param) {
		
	}

	@Override
	protected void onSetSize(int width, int height) {

	}

}
