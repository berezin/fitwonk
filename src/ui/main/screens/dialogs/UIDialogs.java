package ui.main.screens.dialogs;

import java.util.ArrayList;

import android.content.Context;
import common.Res;
import data.DataDialogs.DataDialog;
import fitwonk.Api;
import ui.common.UISectionList;

import com.utils.InternalService;
import com.utils.shorts.A;
import com.utils.shorts.E;
import com.utils.EventsUtils.IBinder;

public class UIDialogs extends UISectionList implements IBinder
{

	boolean isFirst = true;
	
	InternalService service = new InternalService()
	{
		
		
		public void start() {
			
			if(isFirst) {
				isFirst = false;
				super.start();
			}
			else {
				start(getTimeout());
			}
			
		}
		
		@Override
		public void process()
		{
			Api.Dialog.requestData();
		}
		
		@Override
		public int getTimeout()
		{
			return 20000;
		}
	};
	
	public UIDialogs(Context context)
	{
		super(context);

	}

	@Override
	protected void onAttachedToWindow()
	{
		super.onAttachedToWindow();
		E.Bind(this);
		
		if(isFirst)
		{
			
			Set();
		}
		
		service.start();
	}

	@Override
	protected void onDetachedFromWindow()
	{
		super.onDetachedFromWindow();
		E.Unbind(this);
		service.stop();
	}

	void Set()
	{
		ArrayList<UIItem> views = new ArrayList<>();
		
		UISectionIconText uiSection = new UISectionIconText(getContext());
		uiSection.SetSelected();
		uiSection.setOpening(false);
		uiSection.Set(Res.IC_DIALOGS, "СПИСОК ЧАТОВ");
		views.add(uiSection);

		if(Api.Dialog.getData()!=null) {
			
			ArrayList<DataDialog> dialogs = Api.Dialog.getData().dialog;

			if(dialogs!=null)
			for (DataDialog d : dialogs)
			{
				UISectionDialog uiDialog = new UISectionDialog(getContext());
				uiDialog.Set(d);
				views.add(uiDialog);
				
				//views.add(new UISectionDialogDivider(getContext()));
			}
		}
		
		Adapter adapter = new Adapter(views, null);

		Set(adapter);
	}

	@Override
	public void handle(Integer event, Object param)
	{

		if(Api.Dialog.SETTING_DIALOGS.equals(event))
		{
			Set();
		}

	}

}
