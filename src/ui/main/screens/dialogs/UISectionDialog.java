package ui.main.screens.dialogs;

import common.Paints;
import common.Palette;
import common.Res;
import common.Type;
import common.painter.TextDrawer;
import common.painter.ViewDrawer;
import common.painter.ViewDrawer.Layout;
import data.DataDialogs.DataDialog;
import ui.common.UISectionList.UISectionItem;
import ui.common.UISectionList.UISectionItemView;

import com.utils.shorts.LPR;
import com.utils.Dimen;
import activity.ActivityMain;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class UISectionDialog extends UISectionItemView
{

	ViewDrawer background;

	TextDrawer textTheme;
	TextDrawer textLast;
	DataDialog dialog;

	public UISectionDialog(Context context)
	{
		super(context);
		
		

		background = new ViewDrawer(this);
		background.setBackgroundColor(0xffffffff);
		background.getRipple().setClickDelayed(true);
		setClickable(false);
		
		background.layout(FixWidth(), HeightContent());
		addDrawer(background);

		textTheme = new TextDrawer(this);

		textLast = new TextDrawer(this);

		ViewDrawer line = new ViewDrawer(this);
		line.setBackgroundColor(0x22000000);
		line.layout().calcLineH(Dimen.d1).marginBottom(Dimen.d4).bottom();
		addDrawer(line);

		line = new ViewDrawer(this);
		line.setBackgroundColor(0x22000000);
		line.layout().calcLineH(Dimen.d1);
		addDrawer(line);

	}

	private int HeightContent()
	{

		return Dimen.d60;
	}

	@Override
	protected void asyncSet()
	{
		if(dialog.Comments != null && dialog.Comments.size() > 0)
		{

			textTheme.set(dialog.Comments.get(0).text, FixWidth() - Dimen.d30,
					Paints.GetPaint(Type.Futura, 14, Palette.BLUE));
			addDrawer(textTheme);

			textLast.set(dialog.Comments.get(dialog.Comments.size() - 1).text,
					FixWidth() - Dimen.d30,
					Paints.GetPaint(Type.FuturaLight, 14, Palette.GRAY));
			addDrawer(textLast);

			Layout.calcCenterLayoutsV(HeightContent(),
					textTheme.layout().marginLeft(Dimen.d15),
					textLast.layout().marginLeft(Dimen.d15));

		}
	}

	public void Set(final DataDialog dialog)
	{

		this.dialog = dialog;

		background.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				ActivityMain.ToDialog(getContext(), dialog);
			}
		});

		runAsyncSet();

	}

	@Override
	public Integer Height()
	{
		return Dimen.d64;
	}

}
