package ui.main.screens.dialogs;

import common.Paints;
import common.Palette;
import common.Type;
import common.painter.ImageDrawer;
import common.painter.TextDrawer;
import common.painter.ViewDrawer;
import fitwonk.Icons;
import fitwonk.Icons.ICallback;
import ui.common.UISectionList.UISectionItemView;

import com.utils.Dimen;
import android.content.Context;
import android.graphics.Bitmap;

public class UISectionIconText extends UISectionItemView
{

	TextDrawer text;
	ImageDrawer icon;
	
	ViewDrawer background;
	
	Integer ic;
	boolean isIcSetup = false;
	boolean isSelected = false;
	boolean isColorSetup = true;
	
	
	@Override
	public void onOpen(boolean isOpen)
	{
		super.onOpen(isOpen);
		
		if(isOpen) {
			SetSelected();
		}
		else {
			SetUnselected();
		}

	}
	
	public UISectionIconText(Context context)
	{
		super(context);
		
		background = new ViewDrawer(this);
		background.setBackgroundColor(0xffffffff);
		background.layout(FixWidth(), HeightContent()).marginTop(Dimen.d4);
		addDrawer(background);
		
		text = new TextDrawer(this);
		addDrawer(text);
		
		icon = new ImageDrawer(this);
		icon.layout(Dimen.d40, Dimen.d40).marginTop(Dimen.d4).calcCenterV(HeightContent()).marginLeft(Dimen.d10);
		addDrawer(icon);
		
		
		ViewDrawer line = new ViewDrawer(this);
		line.setBackgroundColor(0x22000000);
		line.layout().calcLineH(Dimen.d1).bottom();
		addDrawer(line);
		
		line = new ViewDrawer(this);
		line.setBackgroundColor(0x22000000);
		line.layout().calcLineH(Dimen.d1).marginTop(Dimen.d4);
		addDrawer(line);
		
	}
	
	
	public void SetSelected() {
		if(!isSelected) {
			isColorSetup = false;
			isSelected = true;
			isIcSetup = false;
			SetIc();
			SetColors();
		}
	}
	
	public void SetUnselected() {
		if(isSelected) {
			isColorSetup = false;
			isSelected = false;
			isIcSetup = false;
			SetIc();
			SetColors();
		}
	
	}
	
	public void Set(Integer ic, String str) {
		
		isIcSetup = false;
		this.ic = ic;
		
		text.set(str, Paints.GetPaint(Type.Futura, 16, Palette.BLUE));
		text.layout().marginLeft(Dimen.d60).marginTop(Dimen.d4).calcCenterV(HeightContent());
		
		SetIc();
		SetColors();
	}
	
	private void SetColors() {
		if(isColorSetup) {
			return;
		}
		
		isColorSetup = true;
		background.setBackgroundColor(isSelected?Palette.BLUE:0xffffffff);
		text.setTextColor(!isSelected?Palette.BLUE:0xffffffff);
	}
	
	private void SetIc() {
		
		if(isIcSetup) {
			return;
		}
		
		icon.Clear();
		
		if(ic != null) {
			
			isIcSetup = true;
			
			Icons.Get(ic, isSelected?0xffffffff:Palette.BLUE, new ICallback()
			{
				
				@Override
				public void post(Bitmap bitmap)
				{
					icon.Set(bitmap, true);
				}
			});

		}
	}
	
	@Override
	public Integer Height()
	{
		return Dimen.d64;
	}
	

	public Integer HeightContent()
	{
		return Dimen.d60;
	}
	

}
