package ui.main.screens.dialogs;

import com.utils.Dimen;
import android.content.Context;
import ui.common.UISectionList.UISectionItem;

public class UISectionDialogDivider extends UISectionItem
{


	public UISectionDialogDivider(Context context)
	{
		super(context);
		
		setBackgroundColor(0x00ffffff);
		
		
	}
	
	
	
	@Override
	public Integer Height()
	{
		return Dimen.d4;
	}
	

}
