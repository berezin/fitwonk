package ui.main.screens.myday;

import java.util.ArrayList;

import common.Paints;
import common.Res;
import common.Type;
import data.DataExercise;
import data.DataTraining;
import fitwonk.Api;

import com.utils.shorts.LPR;
import com.utils.shorts.Q;
import com.utils.Dimen;
import android.content.Context;
import android.graphics.Canvas;
import android.view.Gravity;
import android.widget.RelativeLayout;
import android.widget.TextView;
import ui.common.SmartRelativeLayout;
import ui.main.screens.main.calendar.UICalendarInfoExercise;

public class UICalendarInfo extends SmartRelativeLayout
{
	
	RelativeLayout frameDay;
	TextView textDayCounter;
	TextView textDay;
	ArrayList<UICalendarInfoExercise> uiExercises = new ArrayList<>();
	
	ArrayList<DataExercise> exercises;
	DataTraining training;

	public UICalendarInfo(Context context)
	{
		
		super(context);
		setBackgroundColor(0xffffffff);
		
		int wBluePanel = InfoConst.WIDTH_BLUE_PANEL() - Dimen.d5;
		
		frameDay = new RelativeLayout( getContext() );
		addView(frameDay, LPR.create(wBluePanel, LPR.WRAP).centerV().get() );
		
		textDayCounter = new TextView( getContext() );
		textDayCounter.setGravity(Gravity.CENTER_HORIZONTAL);
		
		int sizeCounterReal = (wBluePanel / 2);
		float sizeCounter =  sizeCounterReal / Q.getScale();
		
		Paints.SetText(textDayCounter, Type.Futura, (int) sizeCounter, 0xffffffff);
		textDayCounter.setText( ""+Api.Init.getCacheData().currentDay() );
		frameDay.addView( textDayCounter, LPR.createMW().centerH().get() );
		
		textDay = new TextView(getContext() );
		textDay.setGravity(Gravity.CENTER_HORIZONTAL);
		Paints.SetText(textDay, Type.FuturaLight, (int) ((wBluePanel / 5) / Q.getScale()), 0xffffffff );
		textDay.setText(Q.getStr(Res.STR_INFO_CALENDAR_DAY).toUpperCase() );
		frameDay.addView(textDay, LPR.createMW().centerH().marginTop(sizeCounterReal + sizeCounterReal / 3 ).get() );
		
		
		training = Api.Init.getCacheData().currentTraining();
		exercises = Api.Init.getCacheData().currentExercises();
		
		if(training!=null)
		{
			int offsetY = 0;
			int h = Dimen.d60;
			int wPanel = InfoConst.WIDTH_BLUE_PANEL();
			
			for(DataExercise ex : exercises) {
				
				UICalendarInfoExercise ui = new UICalendarInfoExercise(getContext());
				ui.Set(ex);
				addView(ui, LPR.create(h).margins(wPanel, offsetY).get());
				offsetY += h;
				uiExercises.add(ui);
			}
		}
		
		
		
	}

	@Override
	public void handle(Integer event, Object param)
	{
		
	}

	@Override
	protected void onSetSize(int width, int height)
	{
		
		if(training!=null)
		{
			
		
		}
		
		
	}
	
	@Override
	protected void dispatchDraw(Canvas canvas)
	{
		
		InfoConst.DrawBluePanel(canvas, getMeasuredHeight());
		super.dispatchDraw(canvas);
	}


}
