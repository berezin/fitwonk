package ui.main.screens.myday;

import android.content.Context;
import android.graphics.Canvas;
import ui.common.SmartRelativeLayout;

public class UIEatingInfo extends SmartRelativeLayout
{

	public UIEatingInfo(Context context)
	{
		super(context);
		setBackgroundColor(0xffffffff);
	}

	@Override
	public void handle(Integer event, Object param)
	{
		
	}

	@Override
	protected void onSetSize(int width, int height)
	{

	}
	
	@Override
	protected void dispatchDraw(Canvas canvas)
	{
		InfoConst.DrawBluePanel(canvas, getMeasuredHeight());
		super.dispatchDraw(canvas);
	}

}
