package ui.main.screens.myday;

import java.util.ArrayList;

import com.utils.shorts.LPL;
import com.utils.shorts.LPR;
import com.utils.Dimen;

import com.fitwonk.R;
import common.Paints;
import common.Type;
import fitwonk.Api;
import fitwonk.Bages;
import ui.common.SmartRelativeLayout;
import android.content.Context;
import android.graphics.Canvas;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class UITrainerInfo extends SmartRelativeLayout
{

	ImageView imageBage;
	LinearLayout frameTexts;

	public UITrainerInfo(Context context)
	{
		super(context);
		setBackgroundColor(0xffffffff);

		int wBluePanel = InfoConst.WIDTH_BLUE_PANEL();
		int sizeImage = (int) (wBluePanel * 0.8f);
		int marginImage = (wBluePanel - sizeImage) / 2;

		imageBage = new ImageView(getContext());
		imageBage.setImageBitmap(Bages.CurrentHighlightBitmap());
		addView(imageBage, LPR.create(sizeImage, sizeImage)
				.marginLeft(marginImage).centerV().get());

		ArrayList<CharSequence> list = new ArrayList<>();

		// TODO

		list.add("Тренер: " + Api.Init.getCacheData().getTrainerName());
		list.add("Цель: " + Api.Init.getCacheData().goal);
		list.add("Тренировок: " + Api.Init.getCacheData().trainingFinished);
		list.add("До следующего уровня: " + Api.Init.getCacheData().trainingLevelUp);
		list.add("Последние замеры: "
				+ Api.Init.getCacheData().getLastAnthropometry());

		frameTexts = new LinearLayout(getContext());
		frameTexts.setOrientation(1);
		addView(frameTexts, LPR.createWrap()
				.margins(wBluePanel + Dimen.d15, 0, Dimen.d8)
				.centerV().get());

		int i = 0;
		int n = list.size();
		int padBetween = Dimen.d10;
		int padH = Dimen.d20;
		
		for (CharSequence t : list)
		{
			TextView text = new TextView(getContext());
			Paints.SetText(text, Type.FuturaLight, 14, 0xff444444);
			text.setText(t);
			text.setPadding(0, i==0?padH:padBetween, Dimen.d5, i==n-1?padH:0);
			frameTexts.addView(text);
			i++;
		}

	}

	@Override
	public void handle(Integer event, Object param)
	{

	}

	@Override
	protected void onSetSize(int width, int height)
	{

	}

	@Override
	protected void dispatchDraw(Canvas canvas)
	{
		InfoConst.DrawBluePanel(canvas, getMeasuredHeight());
		super.dispatchDraw(canvas);
	}

}
