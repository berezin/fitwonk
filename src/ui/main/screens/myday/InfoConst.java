package ui.main.screens.myday;

import common.Palette;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import com.utils.shorts.Q;
import com.utils.Dimen;

public class InfoConst
{

	public static Integer WIDTH_BLUE_PANEL() {
		
		return (int) (Q.getW() * 0.35f);
	}
	
	public static Paint PAINT_BLUE = new Paint() {{
		setColor(Palette.BLUE);
	}};
	
	public static Paint PAINT_BLUE_DARK = new Paint() {{
		setColor(Palette.DARK_BLUE);
	}};
	
	public static Paint PAINT_BOARD_LINE = new Paint() {{
		setColor(0xffdddddd);
	}};
	
	
	public static void DrawBluePanel(Canvas canvas, int height) {
		
		int width = WIDTH_BLUE_PANEL();
		int widthScreen = (int) Q.getW();
		
		Rect rect = new Rect(0, 0, width, height);
		canvas.drawRect(rect, PAINT_BLUE);
		
		rect = new Rect(width - Dimen.d5, 0, width, height);
		canvas.drawRect(rect, PAINT_BLUE_DARK);
		
		rect = new Rect(0, 0, widthScreen, Dimen.d1);
		canvas.drawRect(rect, PAINT_BOARD_LINE);
		
		rect = new Rect(0, height - Dimen.d1 , widthScreen, height);
		canvas.drawRect(rect, PAINT_BOARD_LINE);
		
	}
	
	
}
