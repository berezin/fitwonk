package ui.main.screens;

import com.utils.shorts.LPR;
import android.content.Context;
import fitwonk.Api;
import ui.common.SmartRelativeLayout;
import ui.common.UITitle;
import ui.main.screens.calendar.UICalendar;
import ui.main.screens.targets.UITargets;

public class UIScreenTargets extends UIAbstractScreen
{

	UITitle uiTitle;
	UITargets uiTargets;
	
	public UIScreenTargets(Context context)
	{
		super(context);
		
		uiTitle = new UITitle(getContext());
		uiTitle.Set("МОИ ЦЕЛИ");
		addView(uiTitle, LPR.create(UITitle.Height).get());
		
		uiTargets = new UITargets(getContext());
		addView(uiTargets, LPR.create().marginTop(UITitle.Height).get());
	}

	
	
	@Override
	public void handle(Integer event, Object param)
	{
		
	}

	@Override
	protected void onSetSize(int width, int height)
	{
		
	}

}
