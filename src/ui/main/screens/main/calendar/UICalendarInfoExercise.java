package ui.main.screens.main.calendar;

import common.Paints;
import common.Palette;
import common.Type;
import data.DataExercise;
import fitwonk.Icons;
import fitwonk.Icons.ICallback;

import com.utils.shorts.LPR;
import com.utils.ui.ImageLayout;
import com.utils.Dimen;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class UICalendarInfoExercise extends RelativeLayout
{
	
	ImageView image;
	//Image image;
	TextView text;

	public UICalendarInfoExercise(Context context)
	{
		super(context);
		text = new TextView(getContext());
		addView(text, LPR.createWrap().marginLeft(Dimen.d50).marginRight(Dimen.d5).centerV().get());
		Paints.SetText(text, Type.FuturaLight, 14, 0xff444444);
		
		image = new ImageView(getContext());
		//image = new Image(getContext());
		//image.setDefaultImageView();
		
		
		addView(image, LPR.create(Dimen.d50, Dimen.d50).centerV().get());
	}
	
	
	public static Paint PAINT_BOARD_LINE = new Paint() {{


		setColor(0xffeeeeee);
	}};
	
	public void Set(DataExercise ex) {
		text.setText(ex.name);
		
		Icons.Get(ex.icon, Palette.BLUE, new ICallback()
		{
			
			@Override
			public void post(Bitmap bitmap)
			{
				//image.Set(bitmap, true);
				image.setImageBitmap(bitmap);
			}
		});
		
		//I.LoadImageView(image, ex.icon, null, true);
	}
	
	@Override
	protected void dispatchDraw(Canvas canvas)
	{
		
		//Rect rect = new Rect(0, getMeasuredHeight() - Dimen.d1,  getMeasuredWidth(), getMeasuredHeight());
		//canvas.drawRect(rect, PAINT_BOARD_LINE);
		
		super.dispatchDraw(canvas);
	}

}
