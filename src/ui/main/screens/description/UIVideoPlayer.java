package ui.main.screens.description;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.VideoView;
import app.App;
import common.Res;
import drawable.DrawableProgress;
import ui.common.SmartRelativeLayout;

import com.utils.shorts.LPR;
import com.utils.shorts.D;
import com.utils.Dimen;

public class UIVideoPlayer extends SmartRelativeLayout
{

	FrameLayout frame;
	View progress;
	VideoView videoView;
	boolean isStartPreparing = true;
	
	ImageView imagePlay;
	ImageView imageBack;
	
	@SuppressWarnings("deprecation")
	public UIVideoPlayer(Context context, String url)
	{
		super(context);
		
		setBackgroundDrawable(Res.BACKGROUND_BLUE());
		
		progress = new View(getContext());
		DrawableProgress.Set(progress);
		addView(progress, LPR.create(Dimen.d50, Dimen.d50).center().get());
		
		String vidAddress = url;
		String proxyUrl = App.getProxy().getProxyUrl(vidAddress);
		
		videoView = new VideoView(getContext());
		
		addView(videoView, LPR.create(1,1).center().get());
		videoView.setVideoPath(proxyUrl);
		
		videoView.setOnPreparedListener(new OnPreparedListener()
		{
			
			@Override
			public void onPrepared(MediaPlayer mp)
			{
				isStartPreparing = false;
				
				mp.seekTo(1);
				postPause();
				
				imageBack.setVisibility(VISIBLE);
				imagePlay.setVisibility(VISIBLE);
				
				
				videoView.setLayoutParams(LPR.create().center().get());
				progress.setVisibility(INVISIBLE);
			}
		});
		
		videoView.setOnCompletionListener(new OnCompletionListener()
		{
			
			@Override
			public void onCompletion(MediaPlayer mp)
			{
				mp.seekTo(1);
				postPause();
				imagePlay.setImageResource(Res.IC_PLAY);
				
				
			}
		});
		
		
	
		
		
		isStartPreparing = true;
		videoView.start();
		
		videoView.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				
				if(!isStartPreparing) {
					if(!videoView.isPlaying()) {
						videoView.start();
					}
					else {
						videoView.pause();
					}
				}
				
			}
		});
		
		int pad = Dimen.d10;
		imagePlay = new ImageView(getContext());
		imagePlay.setPadding(pad, pad, pad, pad);
		imagePlay.setImageResource(Res.IC_PLAY);
		imagePlay.setBackgroundDrawable(D.Highlight(0x00000000, 0x22ffffff));
		addView(imagePlay);
		
		imagePlay.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
			
				if(videoView.isPlaying()) {
					
					videoView.pause();
					imagePlay.setImageResource(Res.IC_PLAY);
				}
				else {
					videoView.start();
					imagePlay.setImageResource(Res.IC_PAUSE);
				}
				
			}
		});
		
		imageBack = new ImageView(getContext());
		imageBack.setImageResource(Res.IC_SKIP_PREVIOUS);
		imageBack.setPadding(pad, pad, pad, pad);
		addView(imageBack);
		imageBack.setBackgroundDrawable(D.Highlight(0x00000000, 0x22ffffff));
		imageBack.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
			
				videoView.seekTo(1);
				postPause();
				
				imagePlay.setImageResource(Res.IC_PLAY);
				
			}
		});
		
		imageBack.setVisibility(INVISIBLE);
		imagePlay.setVisibility(INVISIBLE);
		
		
		frame = new FrameLayout(getContext());
		frame.setBackgroundColor(0x00000000);
		addView(frame, LPR.create().get());
	}

	@Override
	public void handle(Integer event, Object param)
	{
		
	}

	@Override
	protected void onSetSize(int width, int height)
	{
		
		int size = Dimen.d60;
		int center = width / 2;
		
		imageBack.setLayoutParams(LPR.create(size, size).bottom().marginLeft(center - size).get());
		imagePlay.setLayoutParams(LPR.create(size, size).bottom().marginLeft(center).get());
		
	}
	
	void postPause() {
		postDelayed(new Runnable()
		{
			public void run()
			{
				videoView.pause();
			}
		}, 10);
	}
	

}
