package ui.main.screens.description;

import com.utils.shorts.I;

import activity.ActivityMain;
import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import common.Paints;
import common.Palette;
import common.Res;
import common.Type;
import common.UtilText;
import data.DataExercise;
import data.DataMedia;
import com.utils.shorts.LPL;
import com.utils.shorts.LPR;
import com.utils.shorts.Q;
import com.utils.ui.ImageLayout;
import com.utils.Dimen;

public class UIExcersiceDescription extends RelativeLayout
{
	RelativeLayout frameTheme;
	TextView textTheme;
	ImageLayout imageBack;
	ScrollView scroll;
	LinearLayout linear;
	
	
	public UIExcersiceDescription(Context context)
	{
		super(context);
		
		frameTheme = new RelativeLayout(getContext());
		frameTheme.setBackgroundColor(0xffffffff);
		addView(frameTheme, LPR.create(Dimen.d60).get());

		textTheme = new TextView(getContext());
		textTheme.setGravity(Gravity.CENTER_VERTICAL);
		frameTheme.addView(textTheme, LPR.create(LPR.MATCH, LPR.MATCH)
				.marginLeft(Dimen.d60).get());
		
		
		Paints.SetText(textTheme, Type.Futura, 14, 0xff000000);

		imageBack = new ImageLayout(getContext());
		imageBack.setDefaultImageView();
		frameTheme.addView(imageBack,
				LPR.create(Dimen.d40, Dimen.d40).centerV()
						.marginLeft(Dimen.d10).get());
		imageBack.Set(Res.IC_BACK, false);
		imageBack.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				ActivityMain.ToCalendar(getContext());
			}
		});

		View line = new View(getContext());
		line.setBackgroundColor(0xffdddddd);
		frameTheme.addView(line, LPR.create(Dimen.d1).bottom().get());
		
		line = new View(getContext());
		line.setBackgroundColor(0xffdddddd);
		frameTheme.addView(line, LPR.create(Dimen.d1).get());
		
		
		scroll = new ScrollView(getContext());
		scroll.setSmoothScrollingEnabled(false);
		addView(scroll, LPR.create().marginTop(Dimen.d60).get());
		//scroll.setBackgroundDrawable(Res.BACKGROUND_BLUE());
		scroll.setBackgroundColor(0xffffffff);
		
		linear = new LinearLayout(getContext());
		linear.setOrientation(1);
		scroll.addView(linear);
		
	}
	
	public void Set(DataExercise ex) {
		
		textTheme.setText(ex.name);
		
		linear.removeAllViews();
		
		AddText(ex.instruction);
		
		AddMedia(ex);
		
		//AddUrl("https://www.youtube.com/embed/ZXsQAXx_ao0");
		
		AddBottom();
		
		
		
		scroll.smoothScrollTo(0,0);
		scroll.fullScroll(ScrollView.FOCUS_UP);
		
		postScroll();
	}
	
	
	void postScroll() {
		postDelayed(new Runnable()
		{
			
			@Override
			public void run()
			{
				scroll.smoothScrollTo(0,0);
				scroll.fullScroll(ScrollView.FOCUS_UP);
			}
			
		}, 50);
	}
	

	public void AddMedia(DataExercise ex) {
		if(ex.media!=null) {
			
			for(DataMedia media:ex.media) {
				AddMedia(media);
			}
			
		}
	}
	
	public void AddMedia(DataMedia media) {
		
		if(media.isImage()) {
			AddImage(media.URL);
		}
		else if(media.isVideo()) {
			AddVideo(media.getVideoURL());
		}
		
		AddText(media.description);
		

		
	}
	
	public void AddText(String str) {
		
		if(str == null) {
			return;
		}
		
		TextView text = new TextView(getContext());
		Paints.SetText(text, Type.FuturaLight, 16, 0xff000000);
		int padH = Dimen.d15;
		int padV = Dimen.d10;
		text.setPadding(padH, padV, padH, 0);
		text.setText(UtilText.trim(Html.fromHtml(str)));
		
		linear.addView(text);
	}
	
	public void AddVideo(String link) {
		if(link == null) {
			return;
		}
		
		UIVideoPlayer ui = new UIVideoPlayer(getContext(), link);
		linear.addView(ui, LPL.create((int) (Q.getW() * 0.75f)).marginTop(Dimen.d10).get());
	}
	
	public void AddImage(String link) {
		
		if(link == null) {
			return;
		}
		
		ImageLayout image = new ImageLayout(getContext());
		I.Load(image, link, null, true);
		
		linear.addView(image, LPL.create((int) (Q.getW() * 0.75f)).marginTop(Dimen.d10).get());
	}
	
	public void AddUrl(String url) {
		if(url == null) {
			return;
		}
		
		WebView web = new WebView(getContext());
		
		WebSettings settings = web.getSettings();
	    settings.setJavaScriptEnabled(true);
	    settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
	    
	    web.loadUrl(url);
	    web.setBackgroundColor(Color.TRANSPARENT);

		linear.addView(web, LPL.create((int) (Q.getW() * 0.75f)).marginTop(Dimen.d10).get());
	}
	

	private void AddBottom()
	{
		View v = new View(getContext());
		v.setBackgroundColor(Palette.GRAY_BACKGROUND);
		linear.addView(v, LPL.create(Dimen.d50).get());
	}

}
