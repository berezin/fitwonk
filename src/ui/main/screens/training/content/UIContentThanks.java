package ui.main.screens.training.content;

import android.content.Context;
import android.view.Gravity;
import android.widget.RelativeLayout;
import android.widget.TextView;
import common.Paints;
import common.Type;
import com.utils.shorts.LPR;
import ui.main.screens.training.view.UITrainingTitleThanks;

public class UIContentThanks extends RelativeLayout
{
	UITrainingTitleThanks uiTitle;
	TextView text;
	TextView textDesc;
	RelativeLayout frameCenter;
	
	public UIContentThanks(Context context)
	{
		super(context);
		
		uiTitle = new UITrainingTitleThanks(getContext());
		addView(uiTitle, LPR.createWrap().get());
		
		frameCenter = new RelativeLayout(getContext());
		
		
		text = new TextView(getContext());
		text.setGravity(Gravity.CENTER_HORIZONTAL);
		Paints.SetText(text, Type.Futura, 34, 0xffffffff);
		text.setText("СПАСИБО!");
		text.setId(1);
		frameCenter.addView(text, LPR.createMW().get());
		
		textDesc = new TextView(getContext());
		textDesc.setGravity(Gravity.CENTER_HORIZONTAL);
		Paints.SetText(textDesc, Type.FuturaLight, 14, 0xffffffff);
		textDesc.setText("Следующая тренировка завтра");
		frameCenter.addView(textDesc, LPR.createMW().below(1).get());
		
		addView(frameCenter, LPR.createMW().centerV().get());
		
	}

}
