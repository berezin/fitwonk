package ui.main.screens.training.content;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import common.Paints;
import common.Res;
import common.Type;
import fitwonk.Api;
import com.utils.shorts.LPR;
import com.utils.shorts.A;
import com.utils.shorts.Q;
import com.utils.Dimen;

import ui.common.SmartRelativeLayout;
import ui.main.screens.training.dialog.UIDialogComment;

public class UIContentBottomRating extends SmartRelativeLayout 
{

	TextView text;
	ImageView[] images;

	public UIContentBottomRating(Context context)
	{
		super(context);

		text = new TextView(getContext());
		text.setGravity(Gravity.CENTER);
		addView(text);

		images = new ImageView[5];

		for (int i = 0; i < 5; i++)
		{

			ImageView img = new ImageView(context);
			img.setImageResource(Res.IMG_PLAYER_RATES[i]);
			addView(img);
			images[i] = img;
			
			final int index = i + 1;
			img.setOnClickListener(new OnClickListener()
			{
				
				@Override
				public void onClick(View v)
				{
					
					for(ImageView im:images) {
						if( im != v ) {
							A.toup_alpha(im, 300, 0, 1f, 0.5f, null);
						}
						else {
							A.jump(im, 300, Dimen.d5);
						}
					}
					
					Api.Init.getCacheData().currentTraining().rate = index;
					
					UIDialogComment comment = new UIDialogComment(getContext());
					comment.show();
					
					
				}
			});
			
		}

	}

	@Override
	public void handle(Integer event, Object param)
	{

	}

	@Override
	protected void onSetSize(int width, int height)
	{
		int marginB = height / 6;
		int marginT = 0;
		
		int marginL = width / 9;
		int marginR = marginL;
		
		int h1 = (height - marginB - marginT);
		int h2 = (int) (h1 * 0.7f);
		h1 -= h2;
		
		int marginBetween = (width - marginL - marginR - 5 * h2) / 4;
		
		int textSize = (int) Q.getTextSize(h1 / 2);
		Paints.SetText(text, Type.Futura, textSize, 0xff000000);
		text.setText("ОЦЕНИТЕ ТРЕНИРОВКУ");
		text.setLayoutParams(LPR.create(h1).marginTop(marginT).get());
		
		for(int i = 0; i < 5; i++) {
			images[i].setLayoutParams(LPR.create(h2, h2).margins(marginL + i * (h2 + marginBetween), marginT + h1).get());
		}
		
	
	}

}
