package ui.main.screens.training.content;

import data.DataExercise;
import data.DataTraining.DataExerciseSet;
import drawable.DrawableProgress;
import fitwonk.Api;
import com.utils.shorts.LPR;
import com.utils.Dimen;
import ui.main.screens.training.view.UITrainingIndicatorBig;
import ui.main.screens.training.view.UITrainingIndicatorSmall;

import java.io.IOException;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.VideoView;
import app.App;

public class UIContentExercise extends RelativeLayout
{
	
	int currentSecond = -1;
	float percentCurrent = -1f;
	
	DataExerciseSet set;
	DataExercise ex;
	//
	UITrainingIndicatorSmall uiSmallIndicator;
	UITrainingIndicatorBig uiBigIndicator;
	RelativeLayout frameCenter;
	//
	RelativeLayout frameVideo;
	View progress;
	VideoView videoView;
	
	public static interface ICallback {
		void onStopPlayback();
		void onPreparePlayback();
	}
	
	ICallback callback;
	
	public void setCallback(ICallback callback) {
		this.callback = callback;
	}
	
	public UIContentExercise(Context context, DataExerciseSet set)
	{
		super(context);
		this.set = set;
		ex = Api.Init.getCacheData().getExercise(set.exerciseId);
		
		frameCenter = new RelativeLayout(getContext());
		addView(frameCenter, LPR.create(UITrainingIndicatorSmall.SIZE() + UITrainingIndicatorBig.SIZE() - Dimen.d10, LPR.WRAP).center().get());
		
		
		uiSmallIndicator = new UITrainingIndicatorSmall(getContext());
		frameCenter.addView(
				uiSmallIndicator,
				LPR.create(UITrainingIndicatorSmall.SIZE(), UITrainingIndicatorSmall.SIZE()).right().centerV().get());
		uiSmallIndicator.SetName("ТРЕНИРОВКА");
		
		uiBigIndicator = new UITrainingIndicatorBig(getContext());
		frameCenter.addView(
				uiBigIndicator,
				LPR.create(UITrainingIndicatorBig.SIZE(), UITrainingIndicatorBig.SIZE()).centerV().get());
		uiBigIndicator.SetName("УПРАЖНЕНИЕ");
		
		frameVideo = new RelativeLayout(getContext());
		frameVideo.setBackgroundColor(0x22000000);
		addView(frameVideo, LPR.create().get());
		
		progress = new View(getContext());
		DrawableProgress.Set(progress);
		frameVideo.addView(progress, LPR.create(Dimen.d50, Dimen.d50).center().get());
		
		
		StateTraining();
	}
	
	
	public boolean IsHelp() {
		return isHelp;
	}
	
	public boolean IsPlaying() {
		return videoView.isPlaying();
	}
	
	boolean isHelp = false;
	
	public void StateTraining() {
		
		isHelp = false;
		if(videoView!=null)
		{
			
			if(videoView.isPlaying())
			{
				videoView.stopPlayback();
				
			}
			
			frameVideo.removeView(videoView);
			videoView = null;
		}
		
		frameVideo.setVisibility(GONE);
		frameCenter.setVisibility(VISIBLE);
	}
	
	public void StateHelp() {
		
		isHelp = true;
		frameVideo.setVisibility(VISIBLE);
		frameCenter.setVisibility(GONE);
		progress.setVisibility(VISIBLE);
		String vidAddress = ex.getVideoURL();
		String proxyUrl = App.getProxy().getProxyUrl(vidAddress);
		
		//Uri vidUri = Uri.parse(proxyUrl);
		
		videoView = new VideoView(getContext());
		
		frameVideo.addView(videoView, LPR.create(1,1).center().get());
		videoView.setVideoPath(proxyUrl);
		videoView.setOnPreparedListener(new OnPreparedListener()
		{
			
			@Override
			public void onPrepared(MediaPlayer mp)
			{
				if(videoView!=null) {
					
					if(callback!=null) {
						callback.onPreparePlayback();
					}
					progress.setVisibility(INVISIBLE);
					videoView.setLayoutParams(LPR.create().center().get());
				}
			}
		});
		
		videoView.setOnCompletionListener(new OnCompletionListener()
		{
			
			@Override
			public void onCompletion(MediaPlayer mp)
			{
				StateTraining();
				if(callback!=null) {
					callback.onStopPlayback();
					
				}
				
				mp.release();
			}
		});
		
		
		
		
	
		videoView.start();
		
	}
	
	public void Set(Long counter, Long duration, Long trainingCounter, Long trainingDuration)
	{

		
		int v = (int) ((duration - counter)/1000L);
		float pCurrent =  Math.min((float) (100f * (counter + 100) / duration), 100f);
		float pCurrentSmall =  Math.min((float) (100f * (trainingCounter + 100) / trainingDuration), 100f);
		
		if(v != currentSecond) {
			
			
			uiBigIndicator.SetTime(counter);
			uiSmallIndicator.SetTime(trainingCounter);
			currentSecond = v;
		}
		
		
		
		uiSmallIndicator.SetPercent(pCurrentSmall);
		uiBigIndicator.SetPercent(pCurrent);
		
	}

	public void resumeHelp()
	{
		videoView.start();
	}
	
	public void restartHelp()
	{
		videoView.seekTo(0);
		videoView.start();
	}
	
	public void pauseHelp()
	{
		videoView.pause();
	}
	
	

}
