package ui.main.screens.training.content;

import com.utils.shorts.LPR;
import ui.main.screens.training.view.UITrainingCounterAndDesc;
import android.content.Context;
import android.widget.RelativeLayout;

public class UIContentStart extends RelativeLayout
{
	UITrainingCounterAndDesc uiCounter;
	Long currentCounter;
	
	public UIContentStart(Context context)
	{
		super(context);
		
		uiCounter = new UITrainingCounterAndDesc(getContext());
		uiCounter.Set("Приготовьтесь\nтренировка скоро начнется");
		addView(uiCounter,LPR.createWrap().center().get());
		
	}
	
	public void Set(Long counter)
	{
		
		int v = (int) (5 - (counter/1000L));
		uiCounter.Set(v);
		
		currentCounter = counter;
	}
	
	

}
