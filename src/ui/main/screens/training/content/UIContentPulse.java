package ui.main.screens.training.content;

import android.content.Context;
import android.view.View;
import com.utils.shorts.LPR;
import com.utils.Dimen;

import ui.common.UIAbstractHRPulseView;
import ui.main.screens.training.view.UITrainingIndicatorPulse;
import ui.main.screens.training.view.UITrainingTitlePulse;

public class UIContentPulse extends UIAbstractHRPulseView
{

	UITrainingTitlePulse uiTitle;
	UITrainingIndicatorPulse uiIndicator;
	
	
	ICallback callback;
	
	public static interface ICallback {
		
		void onSetGraph(Long l, Float f);
		void onClearGraph();
		void onSetState(boolean active);
		void onSetPulse(Integer pulse);
	}
	
	public void setCallback(ICallback callback) {
		this.callback = callback;
	}
	
	public UIContentPulse(Context context)
	{
		super(context);
		
		uiTitle = new UITrainingTitlePulse(getContext());
		uiTitle.setId(1);
		addView(uiTitle, LPR.createMW().get());
		
		uiIndicator = new UITrainingIndicatorPulse(getContext());
		addView(uiIndicator, LPR.create().below(1).margin(Dimen.d10).get());
		
		uiIndicator.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Restart();
			}
		});
		
	}

	@Override
	public void onSetText(String str)
	{

		uiIndicator.SetPulse(str);
		
	}
	boolean active = true;
	@Override
	public void onSetState(boolean active)
	{
		
		this.active = active;
		if(callback!=null) {
			callback.onSetState(active);
		}
		
		
	}

	@Override
	public void onSetPulse(Integer pulse)
	{
		if(callback!=null) {
			callback.onSetPulse(pulse);
		}
	}
	
	@Override
	public void onBeat()
	{
		uiIndicator.SetBeat();
	}

	@Override
	public void onGraphClear()
	{
		if(callback!=null) {
			callback.onClearGraph();
		}
	}
	
	@Override
	public void onSetGraph(Long l, Float f)
	{
		if(callback!=null) {
			callback.onSetGraph(l,f);
		}
	}

	@Override
	public void onProgress(int progress)
	{
		uiIndicator.SetProgress(progress);
	}

	

}
