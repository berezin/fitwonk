package ui.main.screens.training.content;

import com.utils.shorts.LPR;
import ui.main.screens.training.view.UITrainingCounterAndDesc;
import android.content.Context;
import android.widget.RelativeLayout;

public class UIContentRest extends RelativeLayout
{
	UITrainingCounterAndDesc uiCounter;
	Long currentCounter;
	
	
	
	public UIContentRest(Context context)
	{
		super(context);
		
		uiCounter = new UITrainingCounterAndDesc(getContext());
		addView(uiCounter,LPR.createWrap().center().get());
	
		
	}
	
	public void Set(Long counter, Long duration)
	{
		
		int v = (int) Math.ceil((duration - counter)/1000);
		if(uiCounter.Set(v))
		{
			uiCounter.Set(declension(v));
		}
		
		
		
		currentCounter = counter;
	}
	
	String[] expressions = {"секунда осталась", "секунды осталось", "секунд осталось"};
	
	public String declension(int i) 
    {
        String result = "";
        
        int count = i % 100; 
        
        if (count >= 5 && count <= 20){ 
        	result = expressions[2];
        }
        else{ 
            count = count % 10; 
            if (count == 1) { 
                result = expressions[0]; 
            } else if (count >= 2 && count <= 4) { 
                result = expressions[1]; 
            } else { 
                result = expressions[2]; 
            }
        } 
        
        return result.toUpperCase(); 
    }
	
	

}
