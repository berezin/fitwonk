package ui.main.screens.training.content;

import android.content.Context;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TextView;
import common.Paints;
import common.Type;
import fitwonk.Bages;
import fitwonk.Bages.DataBage;
import com.utils.shorts.LPR;

import ui.common.SmartRelativeLayout;
import ui.main.screens.training.view.UITrainingTitleThanks;

public class UIContentRating extends SmartRelativeLayout
{

	UITrainingTitleThanks uiTitle;
	ImageView image;
	TextView textNextLevel;
	
	public UIContentRating(Context context)
	{
		super(context);
		
		uiTitle = new UITrainingTitleThanks(getContext());
		addView(uiTitle, LPR.createWrap().get());
		
		DataBage bage = Bages.Next();
		
		
		image = new ImageView(getContext());
		image.setImageBitmap(bage.getHighlightBitmap());
		addView(image);
		
		textNextLevel = new TextView(getContext());
		textNextLevel.setGravity(Gravity.CENTER_HORIZONTAL);
		Paints.SetText(textNextLevel, Type.FuturaLight, 14, 0xffffffff);
		if(bage.max!=null) {
			textNextLevel.setText("До следующего уровня\nосталось "+(bage.max - bage.min)+" тренировок");
		}
		else {
			textNextLevel.setText("Достигнут максимальный уровень");
		}
		addView(textNextLevel);

	}

	@Override
	public void handle(Integer event, Object param)
	{
		
	}

	@Override
	protected void onSetSize(int width, int height)
	{
		int marginT = UITrainingTitleThanks.Height();
		int h1 = height - marginT;
		int h2 = (int) (h1 * 0.2f);
		h1 -= h2;
		int marginL = (width - h1) / 2;
		
		image.setLayoutParams(LPR.create(h1, h1).margins(marginL, marginT).get());
		textNextLevel.setLayoutParams(LPR.create(h2).marginTop(marginT + h1).get());
		
	}

}
