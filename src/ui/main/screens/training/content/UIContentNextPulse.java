package ui.main.screens.training.content;

import android.content.Context;
import android.view.Gravity;
import android.widget.RelativeLayout;
import android.widget.TextView;
import common.Type;
import com.utils.shorts.LPR;
import com.utils.CharsStyler;

public class UIContentNextPulse extends RelativeLayout
{

	TextView text;
	
	public UIContentNextPulse(Context context)
	{
		super(context);
		
		text = new TextView(getContext());
		addView(text, LPR.create().get());
		text.setTextColor(0xffffffff);
		text.setGravity(Gravity.CENTER);
		
		text.setTypeface( Type.FuturaLight() );
		
		CharSequence str = CharsStyler.create()
		.appendWithSize("ТРЕНИРОВКА\nЗАВЕРШЕНА\n", 24)
		.appendWithSize("ДАЛЕЕ", 14)
		.appendWithSize("\n ", 24)
		.appendWithSize("ЗАМЕРИТЬ ПУЛЬС ", 19).Get();
		
		text.setText(str);
		
	}

}
