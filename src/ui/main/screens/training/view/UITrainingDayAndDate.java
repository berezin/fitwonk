package ui.main.screens.training.view;

import common.Paints;
import common.Palette;
import common.Type;
import fitwonk.Api;

import com.utils.shorts.LPR;
import com.utils.Dimen;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class UITrainingDayAndDate extends RelativeLayout
{

	
	public static Integer Height = Dimen.d50;
	
	TextView textDayIndex;
	TextView textDayWord;
	View lineTop, lineBottom;
	
	public UITrainingDayAndDate(Context context)
	{
		super(context);
		
		setBackgroundColor(0xffffffff);
		
		textDayIndex = new TextView(getContext());
		textDayIndex.setGravity(Gravity.CENTER_VERTICAL);
		textDayIndex.setId(1);
		addView(textDayIndex, LPR.create(LPR.WRAP, LPR.MATCH).marginTop(0).marginLeft(Dimen.d20).get());
		
		
		textDayWord = new TextView(getContext());
		textDayWord.setGravity(Gravity.CENTER_VERTICAL);
		addView(textDayWord, LPR.create().marginTop(0).rightOf(1).get());
		
		lineBottom = new View(getContext());
		lineBottom.setBackgroundColor(0xffdddddd);
		addView(lineBottom, LPR.create(Dimen.d1).bottom().get());
		
		lineTop = new View(getContext());
		lineTop.setBackgroundColor(0xffdddddd);
		addView(lineTop, LPR.create(Dimen.d1).get());
		
		Paints.SetText(textDayIndex, Type.Futura, 25, 0xff000000);
		textDayIndex.setText(""+Api.Init.getCacheData().currentDay() + " ");
		Paints.SetText(textDayWord, Type.FuturaLight, 16, 0xff000000);
		textDayWord.setText("ДЕНЬ");
		
	}

}
