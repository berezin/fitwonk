package ui.main.screens.training.view;

import java.util.Date;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.widget.RelativeLayout;
import android.widget.TextView;
import common.DateFormats;
import common.Paints;
import common.Palette;
import common.Type;
import com.utils.shorts.LPR;
import com.utils.shorts.Q;
import com.utils.Dimen;
import ui.main.screens.UIScreenTraining;

public class UITrainingIndicatorBig extends RelativeLayout
{

	
	static Integer _SIZE;
	static public Integer SIZE() {
		if(_SIZE == null) {
			
			_SIZE = (int) (UIScreenTraining.HeightImage() * 1.4);
			
		}
		
		return _SIZE;
	}
	static RectF _RECT2;
	static public RectF RECT2() {
		if(_RECT2 == null) {
			
			_RECT2 = new RectF(Dimen.d15, Dimen.d15, SIZE() - Dimen.d15, SIZE() - Dimen.d15);
			
		}
		
		return _RECT2;
	}
	
	static Integer _R;
	static public Integer RADIUS() {
		if(_R == null) {
			
			_R = SIZE()/2 - 1;
			
		}
		
		return _R;
	}
	
	static Integer _R2;
	static public Integer RADIUS2() {
		if(_R2 == null) {
			
			_R2 = RADIUS() - Dimen.d15;
			
		}
		
		return _R2;
	}
	
	static Paint paintWhite = new Paint() {{

		setColor(0xffffffff);
		setAntiAlias(true);
	}};
	
	static Paint paintBorder = new Paint() {{

		setColor(0x11000000);
		setAntiAlias(true);
		setStrokeWidth(Dimen.d1);
		setStyle(Style.STROKE);
	}};
	static Paint paintArcGray = new Paint() {{

		setColor(0xffdddddd);
		setAntiAlias(true);
		setStrokeWidth(Dimen.d10);
		setStyle(Style.STROKE);
	}};
	static Paint paintArcBlue = new Paint() {{

		setColor(Palette.DARK_BLUE);
		setAntiAlias(true);
		setStrokeWidth(Dimen.d10);
		setStyle(Style.STROKE);
	}};
	
	//
	TextView textPercent;
	TextView textName;
	TextView textTime;
	RelativeLayout frameCenter;
	//
	float currentPercent = 0f;
	int currentPercentInt = -1;
	
	public UITrainingIndicatorBig(Context context)
	{
		super(context);
		//
		frameCenter = new RelativeLayout(getContext());
		
		addView(frameCenter, LPR.createWrap().center().get());
		//
		textPercent = new TextView(getContext());
		textPercent.setId(1);
		
		int sizeReal = (int) (SIZE() / 4.5f);
		float size =  sizeReal / Q.getScale();

		Paints.SetText(textPercent, Type.Futura, (int) size, Palette.BLUE);
		textPercent.setText("");
		frameCenter.addView(textPercent, LPR.createWrap().centerH().get());
		//
		textName = new TextView(getContext());
		textName.setId(2);
		
		sizeReal = (SIZE() / 11);
		size =  sizeReal / Q.getScale();
		
		Paints.SetText(textName, Type.FuturaLight, (int) size, Palette.BLUE);
		textName.setText("");
		frameCenter.addView(textName, LPR.createWrap().below(1).centerH().get());
		//
		textTime = new TextView(getContext());
		
		//sizeReal = (SIZE() / 5);
		//size =  sizeReal / Q.getScale();
		
		Paints.SetText(textTime, Type.FuturaLight, (int) size, 0xff000000);
		textTime.setText("");
		frameCenter.addView(textTime, LPR.createWrap().below(2).centerH().get());
		
		
	}
	
	public void SetPercent(float percent) {
		
		int p = Math.round(percent);
		if(currentPercentInt != p)
		{
			currentPercentInt = p;
			textPercent.setText(""+ p +"%");
		}
		
		currentPercent = percent;
		invalidate();
		
	}
	
	public void SetName(String name) {
		
		textName.setText(name);
		
	}
	
	public void SetTime(Long time) {
		
		textTime.setText(DateFormats.Duration.format(new Date(time)));
		
	}
	
	@Override
	protected void dispatchDraw(Canvas canvas) {
		
		int c = getMeasuredWidth()/2;
		
		canvas.drawCircle(c, c, RADIUS(), paintWhite);
		canvas.drawCircle(c, c, RADIUS(), paintBorder);
		canvas.drawCircle(c, c, RADIUS2(), paintArcGray);
		canvas.drawArc(RECT2(), -90, 360 * currentPercent / 100, false, paintArcBlue);
		
		super.dispatchDraw(canvas);
	}
	
	

}
