package ui.main.screens.training.view;

import android.content.Context;
import android.view.Gravity;
import android.widget.RelativeLayout;
import android.widget.TextView;
import common.Paints;
import common.Type;
import com.utils.shorts.LPR;
import com.utils.Dimen;

public class UITrainingTitleExercise extends RelativeLayout
{

	
	TextView textTitle;
	TextView textDesc;
	RelativeLayout frame;
	
	static public Integer HEIGHT = Dimen.d60;
	
	public UITrainingTitleExercise(Context context)
	{
		super(context);
		
		frame = new RelativeLayout(getContext());
		addView(frame, LPR.createWrap().center().get());
		
		textTitle = new TextView(getContext());
		textTitle.setGravity(Gravity.CENTER_HORIZONTAL);
		textTitle.setId(1);
		Paints.SetText(textTitle, Type.FuturaLight, 12, 0xffffffff);
		textTitle.setText("");
		frame.addView(textTitle, LPR.createMW().get());
		
		textDesc = new TextView(getContext());
		textDesc.setGravity(Gravity.CENTER_HORIZONTAL);
		Paints.SetText(textDesc, Type.FuturaLight, 14, 0xffffffff);
		textDesc.setText("");
		frame.addView(textDesc, LPR.createMW().below(1).get());

	}
	
	public void Set(String title, String desc) {
		
		if(title!=null) {
			textTitle.setText(title);
			textTitle.setVisibility(VISIBLE);
		}
		else {
			textTitle.setVisibility(GONE);
		}
		
		if(desc!=null) {
			textDesc.setText(desc);
			textDesc.setVisibility(VISIBLE);
		}
		else {
			textDesc.setVisibility(GONE);
		}
	}
	
	

}
