package ui.main.screens.training.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import common.Paints;
import common.Palette;
import common.Res;
import common.Type;
import ui.common.SmartRelativeLayout;

import com.utils.shorts.LPL;
import com.utils.shorts.LPR;
import com.utils.shorts.A;
import com.utils.shorts.Q;
import com.utils.Dimen;

public class UITrainingIndicatorPulse extends SmartRelativeLayout
{
	
	
	int progress = 0;
	RelativeLayout frameCenter;
	RelativeLayout frameInfo;
	ImageView imageHeart;
	
	
	TextView textPulse;
	TextView textPulseInfo;

	public UITrainingIndicatorPulse(Context context)
	{
		super(context);
		
		frameCenter = new RelativeLayout(getContext());
		addView(frameCenter, LPR.createWrap().center().get());
		
		textPulse = new TextView(getContext());
		textPulse.setId(1);
		frameCenter.addView(textPulse, LPR.createWrap().get());
		
		frameInfo = new RelativeLayout(getContext());
		frameCenter.addView(frameInfo, LPR.createWrap().rightOf(1).get());
		
		imageHeart = new ImageView(getContext());
		imageHeart.setImageResource(Res.IMG_PLAYER_HEART);
		frameInfo.addView(imageHeart);
		
		textPulseInfo = new TextView(getContext());
		frameInfo.addView(textPulseInfo);
		
		
		
	}
	
	static Integer stroke = Dimen.d15;
	
	static Paint paintWhite = new Paint() {{
		setColor(0xffffffff);
		setAntiAlias(true);
	}};
	
	static Paint paintGray = new Paint() {{
		setColor(0xffdddddd);
		setAntiAlias(true);
		setStrokeWidth(stroke);
		setStyle(Style.STROKE);
	}};
	
	static Paint paintBlue = new Paint() {{
		setColor(Palette.BLUE);
		setAntiAlias(true);
		setStrokeWidth(stroke);
		setStyle(Style.STROKE);
	}};
	
	
	public void SetProgress(int progress) {
		
		this.progress = progress;
		invalidate();
	}
	
	public void SetPulse(String str) {
		
		textPulse.setText(str);
	}
	
	public void SetBeat() {
		
		A.scale(imageHeart, 100, 1.2f);
	}
	
	@Override
	protected void dispatchDraw(Canvas canvas)
	{
		
		int w = getMeasuredWidth();
		int h = getMeasuredHeight();
		int margin = (w - h) / 2;
		
		float cx = w / 2;
		float cy = h / 2;
		stroke = h / 25;
		paintBlue.setStrokeWidth(stroke);
		paintGray.setStrokeWidth(stroke);
		float r1 = cy;
		float rd = stroke * 1.5f;
		float r2 = r1 - rd;
		
		canvas.drawCircle(cx, cy, r1, paintWhite);
		canvas.drawCircle(cx, cy, r2, paintGray);
		canvas.drawArc(new RectF(margin + rd,rd,w - margin - rd, h - rd), -90, 360 * progress / 100, false, paintBlue);
		
		super.dispatchDraw(canvas);
		
	}

	@Override
	public void handle(Integer event, Object param)
	{
		
	}

	@Override
	protected void onSetSize(int width, int height)
	{
		
		
	
		String str = textPulse.getText().toString();
		
		int sizeReal = (height / 4);
		float size =  sizeReal / Q.getScale();
		Paints.SetText(textPulse, Type.Futura, (int) size, Palette.BLUE);
		textPulse.setText(str);
		
		
		
		int heartSize = sizeReal / 3;
		int heartW = (int) (sizeReal * 1.4f);
		
		frameInfo.setLayoutParams( LPR.create(LPR.WRAP, LPR.WRAP).marginLeft(sizeReal / 24).marginTop(sizeReal / 3).rightOf(1).get() );
		
		imageHeart.setLayoutParams(LPR.create(heartW, heartSize).get());
		textPulseInfo.setLayoutParams(LPR.create(LPR.WRAP, LPR.WRAP).marginTop((int) (heartSize * 1.3f)).get() );
		
		sizeReal = (sizeReal / 3);
		size =  sizeReal / Q.getScale();
		Paints.SetText(textPulseInfo, Type.FuturaLight, (int) size, Palette.BLUE);
		textPulseInfo.setText("УД/МИН");
		
		
		
		
		
	}
	
	

}
