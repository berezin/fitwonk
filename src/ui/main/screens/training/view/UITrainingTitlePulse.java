package ui.main.screens.training.view;

import android.content.Context;
import android.view.Gravity;
import android.widget.RelativeLayout;
import android.widget.TextView;
import common.Type;
import com.utils.shorts.LPR;
import com.utils.CharsStyler;
import com.utils.Dimen;

public class UITrainingTitlePulse extends RelativeLayout
{

	TextView title;
	
	
	public UITrainingTitlePulse(Context context)
	{
		super(context);
		title = new TextView(getContext());
		addView(title, LPR.createMW().marginTop(Dimen.d10).get());
		title.setTextColor(0xffffffff);
		title.setGravity(Gravity.CENTER_HORIZONTAL);
		title.setTypeface( Type.FuturaLight() );
		
		CharSequence str = CharsStyler.create()
		.appendWithSize("ТРЕНИРОВКА ЗАВЕРШЕНА", 12)
		.appendWithSize("\nЗАМЕРИТЬ ПУЛЬС\n", 22)
		.appendWithSize("Приложите и легко удерживайте палец\nна объективе камеры и вспышке", 12).Get();
		
		title.setText(str);
		
	}

}
