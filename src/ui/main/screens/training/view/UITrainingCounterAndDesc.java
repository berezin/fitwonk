package ui.main.screens.training.view;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import common.Paints;
import common.Type;
import com.utils.shorts.LPR;
import com.utils.shorts.A;
import com.utils.Dimen;

public class UITrainingCounterAndDesc extends RelativeLayout
{
	int _v = 0;
	TextView textCounter;
	TextView textDesc;
	View viewEmpty;

	public UITrainingCounterAndDesc(Context context)
	{
		super(context);

		textCounter = new TextView(getContext());
		textCounter.setId(1);
		Paints.SetText(textCounter, Type.Futura, 90, 0xffffffff);
		textCounter.setText("");
		addView(textCounter, LPR.createWrap().centerH().get());

		textDesc = new TextView(getContext());
		textDesc.setId(2);
		Paints.SetText(textDesc, Type.FuturaLight, 14, 0xffffffff);
		textDesc.setText("");
		textDesc.setGravity(Gravity.CENTER_HORIZONTAL);
		addView(textDesc, LPR.createWrap().below(1).centerH().get());

		viewEmpty = new View(getContext());
		addView(viewEmpty,
				LPR.create(0, Dimen.d10).below(2).centerH().get());

	}

	public void Set(String descStr)
	{
		textDesc.setText(descStr);
	}

	public boolean Set(int value)
	{
		if(this._v != value)
		{
			this._v = value;

			textCounter.setText("" + _v);
			A.scale_and_hide(textCounter, 300, 0.5f, 1f, false, null);
			return true;
		}
		return false;
	}

}
