package ui.main.screens.training.view;

import android.content.Context;
import android.view.Gravity;
import android.widget.RelativeLayout;
import android.widget.TextView;
import common.DateFormats;
import common.Type;
import data.DataTraining;
import fitwonk.Api;
import com.utils.shorts.LPR;
import com.utils.CharsStyler;
import com.utils.Dimen;

public class UITrainingTitleThanks extends RelativeLayout
{

	TextView text;
	
	
	public static Integer Height() {
		return Dimen.d60;
	}
	
	public UITrainingTitleThanks(Context context)
	{
		super(context);
		
		text = new TextView(getContext());
		text.setTextColor(0xffffffff);
		text.setGravity(Gravity.CENTER|Gravity.BOTTOM);
		text.setTypeface(Type.FuturaLight());
		DataTraining t = Api.Init.getCacheData().currentTraining();
		String dur = DateFormats.Duration((t.finishedAt - t.startedAt) * 1000L);
		
		String strDesc = "Время "+dur+"  Упражнений "+Api.Init.getCacheData().currentExercises().size()+"  Пульс "+t.pulse+" ";
		
		CharSequence str = CharsStyler.createWithSize("ТРЕНИРОВКА ЗАВЕРШЕНА", 18).appendWithSize("\n ",22)
				.appendWithSize(strDesc, 14).Get();
		
		text.setText(str);
		addView(text, LPR.create(Height()).get());
		
		
	}

}
