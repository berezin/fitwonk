package ui.main.screens.training.view;

import android.content.Context;
import android.view.Gravity;
import android.widget.RelativeLayout;
import android.widget.TextView;
import common.Paints;
import common.Type;
import com.utils.shorts.LPR;

public class UITrainingTitleNextExercise extends RelativeLayout
{

	
	TextView textTitle;
	TextView textDesc;
	RelativeLayout frame;

	public UITrainingTitleNextExercise(Context context)
	{
		super(context);
		
		frame = new RelativeLayout(getContext());
		addView(frame, LPR.createWrap().center().get());
		
		textTitle = new TextView(getContext());
		textTitle.setGravity(Gravity.CENTER_HORIZONTAL);
		textTitle.setId(1);
		Paints.SetText(textTitle, Type.FuturaLight, 13, 0xff000000);
		frame.addView(textTitle, LPR.createMW().get());
		textTitle.setText("");
		
		textDesc = new TextView(getContext());
		textDesc.setGravity(Gravity.CENTER_HORIZONTAL);
		Paints.SetText(textDesc, Type.FuturaLight, 15, 0xff000000);
		frame.addView(textDesc, LPR.createMW().below(1).get());
		textDesc.setText("");
	}
	
	public void Set(String title, String desc) {
		
		
		
		if(title!=null) {
			
			title+=": " + desc;
			textTitle.setText(title);
			textTitle.setVisibility(VISIBLE);
		}
		else {
			textTitle.setVisibility(GONE);
		}
		
		textDesc.setVisibility(GONE);
	}
	
	

}
