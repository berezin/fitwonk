package ui.main.screens.training.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import common.AbstractTitleDialogView;
import common.Paints;
import common.Palette;
import common.Type;
import fitwonk.Api;
import com.utils.shorts.LPR;
import com.utils.shorts.E;
import com.utils.Dimen;
import ui.main.screens.training.process.ProcessRating;

public class UIDialogComment extends AbstractTitleDialogView
{

	EditText edit;
	
	public UIDialogComment(Context context)
	{
		super(context);
		setTitle("ОТЗЫВ");
		
		addButton("ОТПРАВИТЬ").setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				Api.Init.getCacheData().currentTraining().comment = edit.getText().toString();
				hide();
			}
		});
		
		TextView bCancel = addButton("ПРОПУСТИТЬ");
		bCancel.setTextColor(Palette.GRAY);
		bCancel.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				Api.Init.getCacheData().currentTraining().comment = null;
				hide();
			}
		});
		
	}
	
	@Override
	public void hide()
	{
		
		super.hide();
		
		postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				E.Event(ProcessRating.REQUEST_NEXT);
			}
		}, 400);
	}

	@Override
	public void createDialogBundle(Bundle b)
	{
		
	}

	@Override
	public void setDialogBundle(Bundle b)
	{
		
	}


	@Override
	public boolean isCancelable()
	{
		return false;
	}

	@Override
	public View getContentView()
	{
		
		RelativeLayout frame = new RelativeLayout(getContext());
		
		edit = new EditText(getContext());
		Paints.SetText(edit, Type.Futura, 14, 0xff000000);
		edit.setText("");
		edit.setSingleLine();
		edit.setHint("Введите текст...");
		edit.setHintTextColor(0x44000000);
		edit.setBackgroundColor(0x11000000);
		
		frame.addView(edit, LPR.createMW().margins(Dimen.d15, 0, Dimen.d15).get());
		
		return frame;
	}

	
	@Override
	public void handle(Integer event, Object param)
	{
		
	}


	
	
	

}
