package ui.main.screens.training.process;

import android.view.View;
import android.view.View.OnClickListener;
import common.Res;
import fitwonk.Training;
import fitwonk.Training.Process;
import ui.main.screens.training.content.UIContentNextPulse;

public class ProcessNextPulse extends Process
{

	UIContentNextPulse ui;
	
	@Override
	public Boolean hasTimer()
	{
		return false;
	}
	
	
	
	
	@Override
	public void onActivate()
	{
		
		super.onActivate();
		
		ui = new UIContentNextPulse(player.getContext());
		player.getUI().setContent(ui);
		
		player.getUI().getPlayButton().setImageResource(Res.IMG_PLAYER_PULSE);
		player.getUI().getPlayButton().setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				player.playNext();
			}
		});
		
	}
	
}
