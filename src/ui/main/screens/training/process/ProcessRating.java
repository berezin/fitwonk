package ui.main.screens.training.process;

import android.view.View;
import common.Test;
import fitwonk.Bages;
import fitwonk.Training.Process;
import com.utils.shorts.E;
import com.utils.EventsUtils.IBinder;
import ui.main.screens.training.content.UIContentBottomRating;
import ui.main.screens.training.content.UIContentRating;
import ui.main.screens.training.content.UIContentThanks;

public class ProcessRating extends Process implements IBinder
{

	View uiContent;
	UIContentBottomRating uiBottom;
	static public Integer REQUEST_NEXT = E.Id("ProcessRating.REQUEST_NEXT");
	
	
	@Override
	public Boolean hasTimer()
	{
		return false;
	}
	
	
	@Override
	public void onActivate()
	{
		super.onActivate();
		
		E.Bind(this);
		
		player.getUI().getPlayButton().setVisibility(View.INVISIBLE);
		
		if(Test.isNextBageState || Bages.IsNextBageState()) {
			uiContent = new UIContentRating(player.getContext());
			player.getUI().setContent(uiContent);
		}
		else {
			uiContent = new UIContentThanks(player.getContext());
			player.getUI().setContent(uiContent);
		}
		
		
		
		uiBottom = new UIContentBottomRating(player.getContext());
		player.getUI().setContentBottom(uiBottom);
		
	}
	
	@Override
	public void onDeactivate()
	{
		super.onDeactivate();
		E.Unbind(this);
	}


	@Override
	public void handle(Integer event, Object param)
	{
		if(event.equals(REQUEST_NEXT)) {
			player.playNext();
		}
	}



	
}
