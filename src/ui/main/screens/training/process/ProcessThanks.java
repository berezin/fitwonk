package ui.main.screens.training.process;

import activity.ActivityMain;
import android.view.View;
import android.view.View.OnClickListener;
import common.Res;
import fitwonk.Api;
import fitwonk.Training.Process;
import com.utils.shorts.E;
import com.utils.EventsUtils.IBinder;
import ui.main.screens.training.content.UIContentThanks;

public class ProcessThanks extends Process implements IBinder
{

	UIContentThanks ui;
	
	@Override
	public Boolean hasTimer()
	{
		return false;
	}
	
	
	@Override
	public void onActivate()
	{
		
		super.onActivate();
		
		E.Bind(this);
		
		
		player.getUI().getPlayButton().setImageResource(Res.IMG_PLAYER_OK);
		player.getUI().getPlayButton().setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				ActivityMain.ToCalendar(v.getContext());
			}
		});
		
		ui = new UIContentThanks(player.getContext());
		player.getUI().setContent(ui);
		
		Api.Training.Save();
		
		
	}
	
	@Override
	public void onDeactivate()
	{
		super.onDeactivate();
		
		E.Unbind(this);
		
	}


	@Override
	public void handle(Integer event, Object param)
	{
		if(event.equals(Api.Training.EVENT_SAVED)) {
			
			
		}
		else if(event.equals(Api.Training.EVENT_ERROR)){
			
			
		}
	}
	
}
