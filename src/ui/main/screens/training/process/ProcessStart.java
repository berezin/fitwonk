package ui.main.screens.training.process;

import android.view.View;
import common.Test;
import data.DataExercise;
import data.DataTraining.DataExerciseSet;
import fitwonk.Api;
import fitwonk.Training;
import fitwonk.Training.Process;
import ui.main.screens.training.content.UIContentStart;

public class ProcessStart extends Process
{

	UIContentStart ui;
	
	
	@Override
	public Long getDuration()
	{
		if(Test.isSpeedTraining) {
			return Test.trainingFirstDuration;
		}
		
		return 5000L;
	}
	
	@Override
	public void onActivate()
	{
		super.onActivate();
		
		ui = new UIContentStart(player.getContext());
		
		player.getUI().setContent(ui);
		setStandartPlayPause();
		
		DataExerciseSet exSet = Api.Init.getCacheData().currentTraining().sets.get(0);
		DataExercise ex = Api.Init.getCacheData().getExercise(exSet.exerciseId);
		
		player.getUI().getTitleExercise().Set("Упражнение 1", ex.name);
		
		player.getUI().getMuteButton().setVisibility(View.VISIBLE);
		player.getUI().getHelpButton().setVisibility(View.VISIBLE);
	}
	
	@Override
	public void onUpdate(Long position)
	{
		
		ui.Set(position);
		
	}
}
