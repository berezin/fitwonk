package ui.main.screens.training.process;

import java.util.Date;

import activity.ActivityMain;
import android.view.View;
import android.view.View.OnClickListener;
import common.DateFormats;
import common.Res;
import common.Test;
import data.DataExercise;
import data.DataTraining;
import data.DataTraining.DataExerciseSet;
import fitwonk.Api;
import fitwonk.Training;
import fitwonk.Training.Process;
import ui.main.screens.training.content.UIContentExercise;
import ui.main.screens.training.content.UIContentExercise.ICallback;

public class ProcessExercise extends Process
{

	UIContentExercise ui;
	DataTraining training;
	DataExerciseSet set;
	DataExercise ex;
	Integer index;
	Long duration;
	boolean isLast = false;
	Integer definedRepeat = 0;
	
	public ProcessExercise(Integer index, DataExerciseSet s) {
		set = s;
		this.index = index;
		duration = s.plan.duration * 1000L;
	}
	
	@Override
	public Long getDuration()
	{
		if(Test.isSpeedTraining) {
			return Test.trainingExerciseDuration;
		}
		
		return duration;
	}
	
	
	@Override
	public void onActivate()
	{
		super.onActivate();
		
		ex = Api.Init.getCacheData().getExercise(set.exerciseId);
		training = Api.Init.getCacheData().currentTraining();
		
		ui = new UIContentExercise(player.getContext(), set);
		player.getUI().setContent(ui);
		
		if(index == 0) {
			training.startedAt = System.currentTimeMillis() / 1000L;
		}
		
		set.startedAt = System.currentTimeMillis() / 1000L;
		
		if( index + 1 != training.sets.size()) {
			player.getUI().getTitleNextExercise().Set(
					"Далее",
					Api.Init.getCacheData().getExercise(
							training.sets.get(index + 1).exerciseId).name);
		}
		else {
			isLast = true;
		}
		
		player.getUI().getTitleExercise().Set("Упражнение "+(index+1), ex.name);
		setPlayPauseWithButtons();
		
		
		player.getUI().getTextAllDuration().setText(DateFormats.Duration.format(new Date(player.getDuration())));
		
		
		ui.setCallback(new ICallback()
		{
			
			@Override
			public void onStopPlayback()
			{
				setPlayPauseWithButtons();
				player.resume();
				player.getUI().getPlayButton().performClick();
			}

			@Override
			public void onPreparePlayback()
			{
				player.getUI().getExitButton().setVisibility(View.INVISIBLE);
			}
		});
		
		player.getUI().getHelpButton().setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				if(ui.IsHelp()) {
					
				}
				else {

					setPlayPauseVideoPlayback();
					player.pause();
					ui.StateHelp();
				}
			}
		});
		
		
	}
	
	@Override
	public void onDeactivate()
	{
		super.onDeactivate();
		
		
		set.fact = set.plan;
		set.fact.repeats = definedRepeat;
		set.finishedAt = System.currentTimeMillis() / 1000L;
		
		if(isLast)
			training.finishedAt = System.currentTimeMillis() / 1000L;
		
	}
	
	public void setPlayPauseWithButtons() {
		
		player.getUI().getPlayButton().setImageResource(Res.IMG_PLAYER_PAUSE);
		
		player.getUI().getRestartButton().setVisibility(View.INVISIBLE);
		player.getUI().getExitButton().setVisibility(View.INVISIBLE);
		
		player.getUI().getMuteButton().setVisibility(View.VISIBLE);
		
		player.getUI().getHelpButton().setVisibility(ex.getVideoURL()!=null?View.VISIBLE:View.INVISIBLE);
		
		player.getUI().getTextCurrentPosition().setVisibility(View.VISIBLE);
		player.getUI().getTextAllDuration().setVisibility(View.VISIBLE);
		
		player.getUI().getPlayButton().setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				if(player.isPlay()) {
					player.getUI().getPlayButton().setImageResource(Res.IMG_PLAYER_START);
					
					player.getUI().getRestartButton().setVisibility(View.VISIBLE);
					player.getUI().getExitButton().setVisibility(View.VISIBLE);
					player.getUI().getRestartButton().setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							player.getUI().getRestartButton().setVisibility(View.INVISIBLE);
							player.getUI().getExitButton().setVisibility(View.INVISIBLE);
							player.getUI().getPlayButton().setImageResource(Res.IMG_PLAYER_PAUSE);
							onRestart();
							player.play();
						}
					});
					player.getUI().getExitButton().setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							
							ActivityMain.ToCalendar(v.getContext());
							
						}
					});
					player.pause();
				}
				else {
					player.getUI().getPlayButton().setImageResource(Res.IMG_PLAYER_PAUSE);
					player.getUI().getRestartButton().setVisibility(View.INVISIBLE);
					player.getUI().getExitButton().setVisibility(View.INVISIBLE);
					player.resume();
				}
			}
		});
		
	}
	
	void setPlayPauseVideoPlayback() {
		
		player.getUI().getPlayButton().setImageResource(Res.IMG_PLAYER_PAUSE);
		
		player.getUI().getMuteButton().setVisibility(View.INVISIBLE);
		player.getUI().getHelpButton().setVisibility(View.INVISIBLE);
		
		player.getUI().getTextCurrentPosition().setVisibility(View.INVISIBLE);
		player.getUI().getTextAllDuration().setVisibility(View.INVISIBLE);
		
		player.getUI().getExitButton().setVisibility(View.VISIBLE);
		player.getUI().getExitButton().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				
				setPlayPauseWithButtons();
				ui.StateTraining();
				player.resume();
				player.getUI().getPlayButton().performClick();
			}
		});
		player.getUI().getRestartButton().setVisibility(View.INVISIBLE);
		
		player.getUI().getPlayButton().setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				if(ui.IsPlaying()) {
					player.getUI().getPlayButton().setImageResource(Res.IMG_PLAYER_START);
					player.getUI().getExitButton().setVisibility(View.VISIBLE);
					player.getUI().getRestartButton().setVisibility(View.VISIBLE);
					player.getUI().getRestartButton().setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							
							player.getUI().getPlayButton().setImageResource(Res.IMG_PLAYER_PAUSE);
							player.getUI().getExitButton().setVisibility(View.INVISIBLE);
							player.getUI().getRestartButton().setVisibility(View.INVISIBLE);
							ui.restartHelp();
						}
					});
					
					ui.pauseHelp();
				}
				else {
					
					player.getUI().getPlayButton().setImageResource(Res.IMG_PLAYER_PAUSE);
					player.getUI().getExitButton().setVisibility(View.INVISIBLE);
					player.getUI().getRestartButton().setVisibility(View.INVISIBLE);
					ui.resumeHelp();
					
				}
			}
		});
	}
	
	@Override
	public void onRestart()
	{
		
		super.onRestart();
		
		
		
	}
	
	public Boolean isDurationPart() {
		return true;
	}
	
	@Override
	public void onUpdate(Long position)
	{
		
		ui.Set(position, getDuration(), player.getCurrentPosition(), player.getDuration());
		player.getUI().getTextCurrentPosition().setText(DateFormats.Duration.format(new Date(player.getCurrentPosition())));
	}
}
