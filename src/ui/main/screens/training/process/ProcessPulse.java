package ui.main.screens.training.process;

import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import common.Paints;
import common.Test;
import fitwonk.Api;
import fitwonk.Training;
import fitwonk.Training.Process;
import com.utils.shorts.LPR;
import com.utils.shorts.D;
import com.utils.Dimen;

import ui.common.GraphPulse;
import ui.main.screens.training.content.UIContentPulse;
import ui.main.screens.training.content.UIContentPulse.ICallback;

public class ProcessPulse extends Process implements ICallback
{

	
	GraphPulse graph;
	Integer pulse;
	TextView textComplete;
	UIContentPulse uiPulse;
	
	@Override
	public Boolean hasTimer()
	{
		return false;
	}

	@Override
	public Boolean isDurationPart()
	{

		return false;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onActivate()
	{
		super.onActivate();
		
		uiPulse = new UIContentPulse(player.getContext());
		player.getUI().setContent(uiPulse);
		uiPulse.setCallback(this);
		
		RelativeLayout bottom = new RelativeLayout(player.getContext());
		bottom.setBackgroundColor(0xffffffff);
		graph = new GraphPulse(player.getContext());
		bottom.addView(graph, LPR.create(Dimen.d50).bottom().get());
		FrameLayout fl = new FrameLayout(player.getContext());
		fl.setBackgroundColor(0x00ffffff);
		bottom.addView(fl, LPR.create(Dimen.d50).bottom().get());
		
		int widthButton = Dimen.d80;
		int heightButton = Dimen.d35;
		int marginTop = Dimen.d5;
		
		textComplete = new TextView(player.getContext());
		Paints.SetText(textComplete, 13, 0xff000000);
		textComplete.setGravity(Gravity.CENTER);
		
		
		
		textComplete.setBackgroundDrawable(D.Highlight(0xffe6e6e6, 0xffffffff));
		bottom.addView(textComplete, LPR.create(widthButton, heightButton).
				marginTop(marginTop).centerH().get());
		textComplete.setVisibility(View.INVISIBLE);
		
		
		if(Test.isFakePulse) {
			
			uiPulse.setFakeResult(Test.fakePulse);
		}
		else {
			
		}
		
		player.getUI().setContentBottom(bottom);
		player.getUI().getPlayButton().setVisibility(View.INVISIBLE);
		
		
		
	}
	
	@Override
	public void onClearGraph()
	{
		graph.clear();
	}

	@Override
	public void onDeactivate()
	{
		super.onDeactivate();
	}

	@Override
	public void onSetGraph(Long l, Float f)
	{
		graph.addPoint(l, f);
	}

	@Override
	public void onSetPulse(Integer pulse)
	{
		this.pulse = pulse;
	}

	@Override
	public void onSetState(boolean active)
	{
		textComplete.setVisibility(!active?View.VISIBLE:View.INVISIBLE);
		
		if(!active)
		if( !uiPulse.hasResult() ) {
			textComplete.setText("Измерить");
			textComplete.setOnClickListener(new OnClickListener()
			{
				
				@Override
				public void onClick(View v)
				{
					uiPulse.Restart();
				}
			});
		}
		else {
			textComplete.setText("Завершить");
			textComplete.setOnClickListener(new OnClickListener()
			{
				
				@Override
				public void onClick(View v)
				{
					Api.Init.getCacheData().currentTraining().pulse = pulse;
					player.playNext();
				}
			});
		}
		
	}

}
