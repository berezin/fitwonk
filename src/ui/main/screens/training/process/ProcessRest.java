package ui.main.screens.training.process;

import android.view.View;
import common.Test;
import data.DataExercise;
import data.DataTraining.DataExerciseSet;
import fitwonk.Api;
import fitwonk.Training;
import fitwonk.Training.Process;
import ui.main.screens.training.content.UIContentRest;

public class ProcessRest extends Process
{

	UIContentRest ui;
	DataExerciseSet set;
	DataExerciseSet setNext;
	Integer index;
	Long duration;
	
	public ProcessRest(Integer index, DataExerciseSet set, DataExerciseSet setNext) {
		this.set = set;
		this.setNext = setNext;
		this.index = index;
		
		duration = set.plan.resttime * 1000L;
	}
	
	
	@Override
	public Long getDuration()
	{
		if(Test.isSpeedTraining) {
			return Test.trainingRestDuration;
		}
		
		return duration;
	}
	
	@Override
	public Boolean isDurationPart()
	{
		return true;
	}
	
	@Override
	public void onActivate()
	{
		super.onActivate();
		
		ui = new UIContentRest(player.getContext());
		
		player.getUI().setContent(ui);
		setStandartPlayPause();
		

		player.getUI().getTitleExercise().Set(null, "ОТДЫХ "+ set.plan.resttime +" СЕКУНД");
		
		//Api.Init.getData().currentTraining().sets.get(index+1)
	
		player.getUI().getTitleNextExercise().Set("Далее", Api.Init.getCacheData().getExercise(setNext.exerciseId).name);
		player.getUI().getMuteButton().setVisibility(View.VISIBLE);
		player.getUI().getHelpButton().setVisibility(View.VISIBLE);
	}
	
	@Override
	public void onUpdate(Long position)
	{
		
		ui.Set(position, duration);
		
	}
}
