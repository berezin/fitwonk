package ui.main.screens;

import com.utils.shorts.LPR;

import activity.ActivityMain;
import android.content.Context;
import data.DataExercise;
import ui.common.UITitle;
import ui.main.screens.description.UIExcersiceDescription;

public class UIScreenDescription extends UIAbstractScreen
{

	UITitle uiTitle;
	UIExcersiceDescription uiExercise;
	
	public UIScreenDescription(Context context)
	{
		super(context);
		
		uiTitle = new UITitle(getContext());
		uiTitle.Set("ТРЕНИРОВКИ");
		addView(uiTitle, LPR.create(UITitle.Height).get());
		
		
		uiExercise = new UIExcersiceDescription(getContext());
		addView(uiExercise, LPR.create().marginTop(UITitle.Height).get());
		
		
	}
	
	@Override
	public void Set(Object data)
	{
		try
		{
			DataExercise ex = (DataExercise) data;
			uiExercise.Set(ex);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		
	}

	
	
	@Override
	public boolean isBlockBack()
	{
		ActivityMain.ToCalendar(getContext());
		return true;
	}

	@Override
	public void handle(Integer event, Object param)
	{
		
	}

	@Override
	protected void onSetSize(int width, int height)
	{
		
	}

}
