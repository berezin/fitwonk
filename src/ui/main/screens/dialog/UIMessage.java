package ui.main.screens.dialog;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.widget.RelativeLayout;
import common.DateFormats;
import common.Paints;
import common.Type;
import data.DataDialogs.DataComment;
import fitwonk.Api;
import com.utils.shorts.Q;
import com.utils.Dimen;

public class UIMessage extends RelativeLayout
{

	
	//static
	static TextPaint paintText  = Paints.GetPaint(Type.FuturaLight, 14, 0xff000000);
	static TextPaint paintDate = Paints.GetPaint(Type.FuturaLight, 10, 0xff888888);
	static Paint paintGreen = new Paint() {{
		setColor(0xffdfffcb);
		setAntiAlias(true);
	}};
	
	static Paint paintWhite = new Paint() {{
		setColor(0xffffffff);
		setAntiAlias(true);
	}};
	
	static int padTop = Dimen.d10;
	static int padContentTop = Dimen.d10;
	static int padContentBottom = Dimen.d10;
	static int padContentBetween= Dimen.d5;
	static int padContentLeft = Dimen.d15;
	static int padContentRight = Dimen.d15;
	static int padHComtemtSum = padContentLeft + padContentRight;
	
	static int padHBig = Dimen.d40;
	static int padHSmall = Dimen.d20;
	static int padHSum = padHBig + padHSmall;
	
	//dinamic
	StaticLayout slText;
	StaticLayout slDate;
	Path path = new Path();
	
	float wText = 0;
	float wDate = 0;
	float hText = 0;
	float hDate = 0;
	
	float xText;
	float yText;
	float xDate;
	float yDate;
	
	RectF rect = new RectF();
	boolean isMy = false;
	Paint paint;
	
	public UIMessage(Context context)
	{
		super(context);

	}
	

	public void Set(DataComment comment) {

		path.reset();
		slText = new StaticLayout(comment.text, paintText, (int) (Q.getW() - padHSum - padHComtemtSum), Alignment.ALIGN_NORMAL, 1f, 1f, true);
		wText = 0f;
		
		for(int i = 0; i < slText.getLineCount(); i++) {
			float w = slText.getLineWidth(i);
			if(w > wText) {
				wText = w;
			}
		}
		hText = slText.getHeight();
		yText = padTop + padContentTop;
		
		slDate = new StaticLayout(DateFormats.Time(comment.createdAt), paintDate, 1000, Alignment.ALIGN_NORMAL, 1f, 1f, true);
		wDate = slDate.getLineWidth(0);
		
		if(wDate > wText ) {
			wText = wDate;
		}
		
		hDate = slDate.getHeight();
		yDate = yText + hText + padContentBetween;
		
		if(comment.userId!= null && comment.userId.equals(Api.Init.getCacheData().id)) {
			isMy = true;
			paint = paintGreen;
			xText = Q.getW() - padHSmall - padContentRight - wText;
		}
		else {
			isMy = false;
			paint = paintWhite;
			xText = padContentLeft + padHSmall;
		}
		
		
		
		xDate = xText + wText - wDate;
		
		
		rect.set(xText - padContentLeft,
				yText - padContentTop,
				xText + wText + padContentRight + 1,
				yText + hText + padContentBetween + hDate + padContentBottom);
		
		//Log.w("rect "+comment.text, rect.toShortString());
		
		//path.addRect(rect, Direction.CCW);
		path.addRoundRect(rect, Dimen.d20, Dimen.d20, Direction.CCW);
		
		if(isMy) {
			float w = Dimen.d20;
			float w1 = Dimen.d25;
			float w2 = w / 2;
			float w12 =  Dimen.d15;
			float x = (rect.right - w);
			float y = (rect.bottom - w);
			
			path.moveTo(x, y + w2);
			path.cubicTo(x + (w1)/2, y + w2 - (w2 - w) / 2 + Dimen.d5, x + w1, y + w, x + w1, y + w);
			path.cubicTo(x + w1 + (w12 - w1)/2,y + w  - w / 2 + Dimen.d7, x + w12, y, x + w12, y);

			path.close();
			
			
		} else {
			
			float w = Dimen.d20;
			float w1 = Dimen.d25;
			float w2 = w / 2;
			float w12 =  Dimen.d15;
			float x = (rect.left + w);
			float y = (rect.bottom - w);
			
			path.moveTo(x - w12, y);
			path.cubicTo(x - w12 + (w12 - w1)/2,y + w / 2 + Dimen.d7,x - w1, y + w, x - w1, y + w);
			path.cubicTo(x - w1 + (w1)/2, y + w + (w2 - w) / 2 + Dimen.d5, x, y + w2, x, y + w2);

			path.close();
		}
		
		invalidate();
		
	}
	
	
	public Integer Height() {
		return padTop + padContentTop + slText.getHeight() + padContentBetween + slDate.getHeight() + padContentBottom;
	}
	
	

	@Override
	protected void dispatchDraw(Canvas canvas)
	{
		super.dispatchDraw(canvas);
		
		canvas.drawPath(path, paint);

		canvas.save();
		canvas.translate(xText, yText);
		
		slText.draw(canvas);
		
		canvas.restore();
		
		canvas.save();
		canvas.translate(xDate, yDate);
		
		slDate.draw(canvas);
		
		canvas.restore();

	}
	

}
