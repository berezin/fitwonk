package ui.main.screens.dialog;

import java.util.ArrayList;

import com.utils.shorts.A;
import com.utils.shorts.D;
import com.utils.shorts.K;

import activity.ActivityMain;
import android.content.Context;
import android.support.v4.widget.NestedScrollView;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import common.Paints;
import common.Res;
import common.Type;
import data.DataDialogs.DataComment;
import data.DataDialogs.DataDialog;
import fitwonk.Api;
import com.utils.shorts.LPL;
import com.utils.shorts.LPR;
import com.utils.EventsUtils.BindedRelativeLayout;
import com.utils.Dimen;
import com.utils.VovaActivity;
import com.utils.VovaActivity.IActivityHandler;

public class UIDialog extends BindedRelativeLayout implements IActivityHandler
{

	RelativeLayout frameTheme;
	TextView textTheme;
	ImageView imageBack;
	NestedScrollView scroll;
	View background;
	LinearLayout linear;
	DataDialog dialog;
	EditText edit;

	@SuppressWarnings("deprecation")
	public UIDialog(Context context)
	{
		super(context);

		frameTheme = new RelativeLayout(getContext());
		frameTheme.setBackgroundColor(0xffffffff);
		addView(frameTheme, LPR.create(Dimen.d60).get());

		textTheme = new TextView(getContext());
		textTheme.setGravity(Gravity.CENTER_VERTICAL);
		frameTheme.addView(textTheme, LPR.create(LPR.MATCH, LPR.MATCH)
				.marginLeft(Dimen.d60).get());

		Paints.SetText(textTheme, Type.Futura, 14, 0xff000000);

		imageBack = new ImageView(getContext());
		frameTheme.addView(imageBack,
				LPR.create(Dimen.d40, Dimen.d40).centerV()
						.marginLeft(Dimen.d10).get());
		imageBack.setImageResource(Res.IC_BACK);
		imageBack.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				ActivityMain.ToDialogs(getContext());
			}
		});

		View line = new View(getContext());
		line.setBackgroundColor(0xffdddddd);
		frameTheme.addView(line, LPR.create(Dimen.d1).bottom().get());

		line = new View(getContext());
		line.setBackgroundColor(0xffdddddd);
		frameTheme.addView(line, LPR.create(Dimen.d1).get());

		
		background = new View(getContext());
		D.Set(background, Res.BACKGROUND_BLUE());
		addView(background, LPR.create().marginTop(Dimen.d60).get());
		
		scroll = new NestedScrollView(getContext());
		addView(scroll, LPR.create().marginTop(Dimen.d60).get());
		
		linear = new LinearLayout( getContext() );
		linear.setOrientation( 1 );
		scroll.addView( linear );

		edit = new EditText( getContext() );
		addView(edit, LPR.create(Dimen.d55).bottom().get() );
		Paints.SetText(edit, Type.Futura, 15, 0xff000000);
		edit.setText("");
		edit.setBackgroundColor(0xffffffff);
		edit.setHint("Введите текст сообщения...");
		edit.setHintTextColor(0x44000000);
		edit.setImeOptions(EditorInfo.IME_ACTION_SEND);
		edit.setSingleLine();
		edit.setOnEditorActionListener(new OnEditorActionListener()
		{

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event)
			{
				if(actionId == EditorInfo.IME_ACTION_SEND || event.getAction() == KeyEvent.ACTION_DOWN &&
		                event.getKeyCode() == KeyEvent.KEYCODE_ENTER)
				{
					
					if(edit.getText().length() != 0) {
						
						String comment = edit.getText().toString();
						
						DataComment data = new DataComment();
						data.createdAt = System.currentTimeMillis() / 1000L;
						data.id = 0;
						data.text = comment;
						data.userId = Api.Init.getCacheData().id;
						dialog.Comments.add(data);
						Set(dialog);
						
					
						postScrollDown();
						
						Api.Dialog.sendComment(dialog.id, comment);
						edit.setText("");
						K.Close(edit);
					}

				}
				return true;
			}
		});

	}
	
	void postScrollDown() {
		
		//scroll.dispatchNestedPreScroll(0, linear.getMeasuredHeight() + Dimen.d100, null, null);
		
		//scroll.smoothScrollTo(0,linear.getMeasuredHeight() + Dimen.d100);
		//scroll.fullScroll(ScrollView.FOCUS_DOWN);
		

		post(new Runnable()
		{
			public void run()
			{
				scroll.scrollTo(0, linear.getMeasuredHeight() + Dimen.d100);

				//scroll.smoothScrollTo(0,linear.getMeasuredHeight() + Dimen.d100);
				//scroll.fullScroll(ScrollView.FOCUS_DOWN);
			}
		});
	}

	public void Set(DataDialog dialog)
	{

		this.dialog = dialog;

		Set();
		
		postScrollDown();

	}
	
	@Override
	protected void onAttachedToWindow()
	{
		super.onAttachedToWindow();
		A.fadein(scroll, 150);
		VovaActivity.AddHandler(getContext(), this);
	}
	
	@Override
	protected void onDetachedFromWindow()
	{
		
		super.onDetachedFromWindow();
		
		VovaActivity.RemoveHandler(getContext(), this);
		linear.removeAllViews();
		
	}

	private void Set()
	{
		
		if(dialog.Comments != null && dialog.Comments.size() > 0)
		{
			textTheme.setText(dialog.Comments.get(0).text);
		} else
		{
			if(dialog.Comments == null)
			{
				dialog.Comments = new ArrayList<>();
			}
			textTheme.setText("");
		}
		
		
		linear.removeAllViews();

		linear.addView(new View(getContext()),
				LPL.create(Dimen.d5).get());

		for (DataComment c : dialog.Comments)
		{

			UIMessage item = new UIMessage(getContext());
			item.Set(c);

			linear.addView(item, LPL.create(item.Height()).get());

		}

		linear.addView(new View(getContext()),
				LPL.create(Dimen.d70).get());

	}

	@Override
	public void handle(Integer event, Object param)
	{
		if(Api.Dialog.SETTING_DIALOGS.equals(event)) {
			
			
			for(DataDialog d:Api.Dialog.getData().dialog) {
				if(d.id.equals(dialog.id)) {
					
					dialog = d;
					Set();
					return;
				}
			}
			
		}
		
	}

	@Override
	public void onDestroy()
	{
		
	}

	@Override
	public void onStart()
	{
		
	}

	@Override
	public void onStop()
	{
		
	}

	@Override
	public void onResume()
	{
		
	}

	@Override
	public void onPause()
	{
		
	}

	@Override
	public boolean isBlockBack()
	{
		
		ActivityMain.ToCalendar(getContext());
		
		return true;
	}

}
