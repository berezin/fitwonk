package ui.main.screens.targets;

import java.util.ArrayList;
import java.util.Calendar;

import com.utils.Dimen;
import com.utils.shorts.Q;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.TextPaint;
import android.view.MotionEvent;
import common.DateFormats;
import common.Palette;
import ui.common.UISectionList;
import ui.common.UISectionList.UISubItem;

public class UISubTrainings extends UISubItem 
{

	float offset = 0f;
	
	

	public UISubTrainings(Context context)
	{
		super(context);
		
		setBackgroundColor(0xffffffff);
		
	}
	
	Paint paintGrayLine = new Paint()
	{
		{
			setAntiAlias(true);
			setColor(0x22000000);
			setStyle(Style.STROKE);
			setStrokeWidth(Dimen.d1);
		}
	};

	Paint paintBlackLine = new Paint()
	{
		{
			setAntiAlias(true);
			setColor(0xff000000);
			setStyle(Style.STROKE);
			setStrokeWidth(Dimen.d2);
		}
	};
	
	Paint paintRed = new Paint()
	{
		{
			setAntiAlias(true);
			setColor(0xffaa0022);
		}
	};
	
	Paint paintGray = new Paint()
	{
		{
			
			setColor(0xffeeeeee);
		}
	};
	
	Paint paintGreen = new Paint()
	{
		{
			setAntiAlias(true);
			setColor(0xff00aa22);
		}
	};
	
	TextPaint tpDate = new TextPaint() {{

		setColor(Palette.BLUE);
		setAntiAlias(true);
		setTextSize(Dimen.d10);
	}};
	
	TextPaint tpDateGray = new TextPaint() {{

		setColor(Palette.GRAY);
		setAntiAlias(true);
		setTextSize(Dimen.d10);
	}};
	
	public static class DataLine {
		public String date;
		public boolean isTraining;
	}
	
	ArrayList<DataLine> lines;
	RectF rectRed;
	RectF rectGreen;
	Rect recrBoundText = null;
	
	@Override
	protected void dispatchDraw(Canvas canvas)
	{
		
		super.dispatchDraw(canvas);
		
		int w = getMeasuredWidth();
		int h = getMeasuredHeight();
		Rect rect = new Rect(0, 0, w, h);
		Rect rectContent = new Rect(Dimen.d30, Dimen.d15,
				rect.right - Dimen.d20, rect.bottom - Dimen.d20);

		canvas.drawRect(rect, paintGrayLine);
		canvas.drawRect(rectContent, paintGray);
		
		canvas.drawLine(rectContent.left, rectContent.top, rectContent.left,
				rectContent.bottom, paintBlackLine);
		canvas.drawLine(rectContent.left, rectContent.bottom, rectContent.right,
				rectContent.bottom, paintBlackLine);
		
		if(lines == null) {
			lines = new ArrayList<>();
			
			int n = 10;
			Long step = 1000L*60L*60L*24L;
			Long time = System.currentTimeMillis() - step * n;
			Calendar calendar = Calendar.getInstance();
			String[] days = {"ПН","ВТ","СР","ЧТ","ПТ","СБ","ВС"};
			
			for(int i = 0; i < n; i++) {
				
				calendar.setTimeInMillis(time);
				int day = calendar.get(Calendar.DAY_OF_WEEK); 
				
				DataLine d = new DataLine();
				d.date = DateFormats.SimpleDate(time) +" "+days[day - 1];
				time += step;
				d.isTraining = ((int)(Math.random() * 2) == 1);
				
				lines.add(d);
			}
			
			rectGreen = new RectF(0, rectContent.bottom - rectContent.height() * 0.5f, 0, rectContent.bottom);
			rectRed = new RectF(0, rectContent.bottom - rectContent.height() *0.1f, 0, rectContent.bottom);
		}

		float between = rectContent.width() / 5;
		float between2 = between / 2;
		
		int i = 0;
		
		float width = lines.size() * between;
		float left = rectContent.right - width;
	
		
		
		offset = Math.max(0f, offset);
		offset = Math.min(width - rectContent.width(), offset);
		float p = Dimen.d3;
		
		left += offset;
		float right = left + between;
		
		canvas.save();
		canvas.clipRect(new Rect(rectContent.left, rectContent.top, rectContent.right, rect.bottom));
		
		for(DataLine d:lines) {
			
			if(right > rectContent.left && left < rectContent.right) {
				
				if(d.isTraining) {
					rectGreen.left = left + p;
					rectGreen.right = right - p;
					canvas.drawRect(rectGreen, paintGreen);
					
					
				}
				else {
					rectRed.left = left + p;
					rectRed.right = right - p;
					canvas.drawRect(rectRed, paintRed);
				}
				
				String str = d.date;
				
				if(recrBoundText == null) {
					recrBoundText = new Rect();
					tpDate.getTextBounds(str, 0, str.length(), recrBoundText);
				}
				
				canvas.drawText(str, left + between2 - recrBoundText.width()*0.5f, rectContent.bottom + recrBoundText.height() + Dimen.d5, d.isTraining?tpDate:tpDateGray);
			}
			
			left += between;
			right += between;
		}
		
		canvas.restore();
		
	}

	static Integer _h;
	@Override
	public Integer Height()
	{
		
		if(_h == null) {
			_h = (int) (Q.getW() * 0.75f);
		}
		
		return _h;
	}
	
	@Override
	public boolean onInterceptTouchEvent(android.view.MotionEvent ev)
	{

		UISectionList.IS_LOCKED_SWITCH = true;
		return true;
	};
	
	float lastX = -1, lastY, cur_x;
	int last_offset;
	boolean is_setted = false;
	boolean is_setted_locked = false;
	
	@Override
	public boolean onTouchEvent(MotionEvent arg0)
	{

		if(arg0.getAction() == MotionEvent.ACTION_UP || arg0.getAction() == MotionEvent.ACTION_CANCEL)
		{

			is_setted = false;
			lastX = -1;
			UISectionList.IS_LOCKED_SWITCH = false;

			if(is_setted_locked)
			{
				
				is_setted_locked = false;
				
				postInvalidate();
				
				return true;
			}

		} else if(arg0.getAction() == MotionEvent.ACTION_DOWN)
		{
			
			
			is_setted = false;
			is_setted_locked = false;
			lastX = arg0.getX();
			lastY = arg0.getY();
			
		}

		else if(arg0.getAction() == MotionEvent.ACTION_MOVE && lastX != -1)
		{
			
			if(!is_setted)
			{

				double r = Math.sqrt(Math.pow(lastY - arg0.getY(), 2)
						+ Math.pow(lastX - arg0.getX(), 2));
				
				if(r > 8)
				{
					if(Math.abs(lastX - arg0.getX()) < Math.abs(lastY
							- arg0.getY()))
					{
						lastX = -1;
						UISectionList.IS_LOCKED_SWITCH = false;
						is_setted_locked = false;
					}
					else
					{
						is_setted_locked = true;
						lastX = arg0.getX();
						lastY = arg0.getY();
						cur_x = lastX;
					}

					is_setted = true;
				}

			} else
			{

				if(is_setted_locked) {
					cur_x = arg0.getX();
					offset+=(cur_x - lastX);
					lastX = cur_x;
					invalidate();
				}
				
				


			}

		}

		if(!is_setted_locked)
		{
			super.onTouchEvent(arg0);
		}

		return true;

	}

}
