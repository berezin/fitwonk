package ui.main.screens.targets;

import java.util.ArrayList;

import com.utils.EventsUtils.IBinder;
import com.utils.shorts.E;

import android.content.Context;
import common.Res;
import data.DataAnthropometrys;
import data.DataAnthropometrys.DataAnthropometry;
import fitwonk.Api;
import ui.common.UISectionList;
import ui.main.screens.dialogs.UISectionIconText;

public class UITargets extends UISectionList implements IBinder
{

	
	boolean isNoAnth = false;
	DataAnthropometrys anths;
	
	public UITargets(Context context)
	{
		super(context);
		
	}

	
	private void Set(boolean isNoAnth) {
		
		ArrayList<UIItem> views = new ArrayList<>();
		
		UISubItem item;
		UISectionIconText uiSection;
		DataAnthropometrys anths = Api.Anthropometry.getData();
		
		if(!isNoAnth) {
			
			uiSection = new UISectionIconText(getContext());
			uiSection.Set(Res.IC_WEIGHT, "ВЕС");
			views.add(uiSection);
			
			views.add(new UISubTop(getContext()));
			
			UISubGraph uiGraph = new UISubGraph(getContext(), null/*anths.getWeight().name*/, anths.getWeight());
			views.add(uiGraph);
			
			uiSection = new UISectionIconText(getContext());
			uiSection.Set(Res.IC_SIZE, "РАЗМЕРЫ");
			views.add(uiSection);
			
			views.add(new UISubTop(getContext()));
			
			for(DataAnthropometry a: anths.getSizes()) {
				uiGraph = new UISubGraph(getContext(), a.name, a);
				views.add(uiGraph);
			}
		}
		
		uiSection = new UISectionIconText(getContext());
		uiSection.Set(Res.IC_TRAININGS, "ТРЕНИРОВКИ");
		views.add(uiSection);
		
		views.add(new UISubTop(getContext()));
		views.add( new UISubTrainings(getContext()));
		views.add(new UISubTop(getContext()));
		
		uiSection = new UISectionIconText(getContext());
		uiSection.Set(Res.IC_BAGES, "БЕЙДЖИ");
		views.add(uiSection);
		
		item = new UISubBages(getContext());
		views.add(item);
		
		Adapter adapter = new Adapter(views, null);

		Set(adapter);
	}

	
	@Override
	protected void onAttachedToWindow()
	{
		super.onAttachedToWindow();
		E.Bind(this);
		
		DataAnthropometrys a = Api.Anthropometry.getData();
		
		if(anths!=null && a == anths) {
			return;
		}
		
		anths = a;
		
		isNoAnth = anths == null;
		if(isNoAnth) {
			Api.Anthropometry.requestData();
		}
		
		Set(isNoAnth);
		
	}
	
	@Override
	protected void onDetachedFromWindow()
	{
		super.onDetachedFromWindow();
		E.Unbind(this);
	}

	@Override
	public void handle(Integer event, Object param)
	{
		if(Api.Anthropometry.hasChangedEvent(event)) {
			if(anths == null) {
				DataAnthropometrys a = Api.Anthropometry.getData();
				
				anths = a;
				
				isNoAnth = anths == null;
				
				Set(isNoAnth);
			}
		}
	}

}
