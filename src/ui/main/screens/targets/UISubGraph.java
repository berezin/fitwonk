package ui.main.screens.targets;

import com.utils.Dimen;
import com.utils.shorts.D;
import com.utils.shorts.LPR;

import android.content.Context;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import common.Paints;
import common.Palette;
import common.Type;
import data.DataAnthropometrys.DataAnthropometry;
import ui.common.UISectionList.UISubItem;

public class UISubGraph extends UISubItem 
{

	UITargetGraph uiGraph;
	TextView textAdd;
	TextView textTargetChange;
	DataAnthropometry data;
	
	public UISubGraph(Context context, String title, DataAnthropometry d)
	{
		super(context);
		data = d;
		setBackgroundColor(Palette.GRAY_BACKGROUND);
		uiGraph = new UITargetGraph(getContext());
		uiGraph.Set(title, data);
		addView(uiGraph, LPR.create(uiGraph.Height()).get());
		
		textAdd = new TextView(getContext());
		Paints.SetText(textAdd, 20, 0xffffffff);
		textAdd.setText("+");
		textAdd.setGravity(Gravity.CENTER);
		D.Set(textAdd, D.Fill(Palette.BLUE));
		addView(textAdd, LPR.create(Dimen.d40, Dimen.d40).right().margins(0, uiGraph.HeightText() + Dimen.d15, Dimen.d20).get());
		
		
		textTargetChange = new TextView(getContext());
		Paints.SetText(textTargetChange, Type.Futura, 12, Palette.BLUE);
		textTargetChange.setText(Html.fromHtml("<u>ИЗМЕНИТЬ ЦЕЛЬ<u/>"));
		textTargetChange.setGravity(Gravity.CENTER_VERTICAL);
		
		addView(textTargetChange, LPR.create(LPR.WRAP,Dimen.d30).bottom().right().margins(0, 0, Dimen.d20, uiGraph.HeightBottom()).get());
		
		textTargetChange.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				UIDialogSendValue ui = new UIDialogSendValue(getContext());
				ui.setTitle(data.name);
				ui.setAnthropometry(data.id, data.unit, false);
				ui.show();
			}
		});
	
		textAdd.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				UIDialogSendValue ui = new UIDialogSendValue(getContext());
				ui.setTitle(data.name);
				ui.setAnthropometry(data.id, data.unit, true);
				ui.show();
			}
		});
		
	}

	Integer _h;
	@Override
	public Integer Height() {
		if(_h == null) {
			_h = uiGraph.Height();
		}
		
		return _h;
	}

}
