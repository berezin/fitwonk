package ui.main.screens.targets;

import com.utils.Dimen;
import com.utils.EventsUtils.IBinder;
import com.utils.shorts.E;
import com.utils.shorts.LPR;
import com.utils.shorts.T;

import android.content.Context;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import common.AbstractTitleDialogView;
import common.Paints;
import common.Palette;
import common.Type;
import common.updater.Request;
import data.DataAnthropometrys.DataAnthropometry;
import data.DataAnthropometrys.DataDateValue;
import drawable.DrawableProgress;
import fitwonk.Api;

@SuppressWarnings("rawtypes")
public class UIDialogSendValue extends AbstractTitleDialogView implements IBinder
{

	//static Long lastSaveValues = 0L;
	//static HashMap<Integer, String> values = new HashMap<>();
	

	RelativeLayout frame;
	RelativeLayout center;
	EditText edit;
	TextView textUnit;
	TextView textType;
	
	View progress;
	TextView textSend;
	TextView textCancel;
	//
	Integer id;
	Double sendValue;
	Integer token;
	String unit;
	Boolean isValue;
	Request sender;
	
	public UIDialogSendValue(Context context)
	{
		super(context);
		
		
		textSend = addButton("ОТПРАВИТЬ");
		textSend.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				if(token==null) {
					
					try
					{
						String str = edit.getText().toString();
						sendValue = Double.parseDouble(str);
						setSending();
						token = sender.Send(id, sendValue);
					}
					catch (Exception e)
					{
						
					}
					
				}
			}
		});
		
		textCancel = addButton("ОТМЕНА");
		textCancel.setTextColor(Palette.GRAY);
		textCancel.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if(token==null) {
					hide();
				}
			}
		});
		
	}
	
	@Override
	public void show()
	{
		if(id!=null)
			super.show();
	}
	
	public void setAnthropometry(Integer id, String unit, boolean isValue) {
		this.id = id;
		this.unit = unit;
		this.isValue = isValue;
		
		if(id == null) {
			hide();
		}
		else {
			
			
			
			if(unit==null)
				unit = "";
		
			textUnit.setText(unit.toUpperCase());
			
			if(isValue) {
				
				DataAnthropometry da =  Api.Anthropometry.search(id);

				if(da!=null) {
					
					DataDateValue ddv =  da.getCurrentDayLog();
					
					String d = ddv!=null?ddv.getValueText():null;
					
					if(d!=null) {
						edit.setText(d);
					}
					
					textType.setText("ЗАМЕР:");
					
					sender = Api.Anthropometry.Saving;
				}
				else {
					hide();
				}
				
				
			}
			else {
				
				DataAnthropometry da =  Api.Anthropometry.search(id);

				if(da!=null) {
					if(da.target!=null) {
						edit.setText(da.getTargetText());
					}
					
					textType.setText("ЦЕЛЬ:");
					
					sender = Api.Anthropometry.SavingTarget;
				}
				else {
					hide();
				}
				
			}
			
		}	

	}
	
	@Override
	protected void onAttachedToWindow()
	{
		
		super.onAttachedToWindow();
		E.Bind(this);
	}
	
	@Override
	protected void onDetachedFromWindow()
	{
		
		super.onDetachedFromWindow();
		E.Unbind(this);
	}
	
	@Override
	public boolean isCancelable()
	{
		return false;
	}
	

	@Override
	public void createDialogBundle(Bundle b)
	{
		b.putInt("id", id);
		b.putString("unit", unit);
		b.putBoolean("isValue", isValue);
	}

	@Override
	public void setDialogBundle(Bundle b)
	{
		setAnthropometry(b.getInt("id"), b.getString("unit"), b.getBoolean("isValue"));
	}

	@Override
	public View getContentView()
	{
		frame = new RelativeLayout(getContext());
		center = new RelativeLayout(getContext());
		
		View empty = new View(getContext());
		frame.addView(empty, LPR.create(Dimen.d80).get());
		
		
		textType = new TextView(getContext());
		textType.setId(1);
		Paints.SetText(textType, Type.Futura, 12, 0xff000000);
		textType.setText("");
		center.addView(textType, LPR.createWrap().centerV().get());
		
		
		edit = new EditText(getContext());
		edit.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
		Paints.SetText(edit, Type.Futura, 14, 0xff000000);
		edit.setText("");
		edit.setId(2);
		edit.setHint("...");
		edit.setHintTextColor(0x44000000);
		edit.setBackgroundColor(0x11000000);
		edit.setPadding(Dimen.d30, Dimen.d10, Dimen.d30, Dimen.d10);
		
		InputFilter[] fArray = new InputFilter[1];
		fArray[0] = new InputFilter.LengthFilter(6);
		edit.setFilters(fArray);
		center.addView(edit, LPR.createWrap().margins(Dimen.d10).rightOf(1).centerV().get());
		
		textUnit = new TextView(getContext());
		Paints.SetText(textUnit, Type.FuturaLight, 14, 0xff000000);
		textUnit.setText("");
		center.addView(textUnit, LPR.createWrap().margins(Dimen.d10).rightOf(2).centerV().get());
		

		frame.addView(center, LPR.createWrap().center().get());
		
		progress = new View(getContext());
		DrawableProgress.Set(progress, Palette.BLUE);
		frame.addView(progress, LPR.create(Dimen.d40, Dimen.d40).center().get());
		
		setInput();
		
		return frame;
	}
	
	void setSending() {
		progress.setVisibility(VISIBLE);
		center.setVisibility(INVISIBLE);
	
	}
	
	void setInput() {
		token = null;
		progress.setVisibility(INVISIBLE);
		center.setVisibility(VISIBLE);
	
	}
	
	@Override
	public void handle(Integer event, Object param)
	{
		
		if(token != null) {
			
			if(sender.EVENT_OK.equals(event)) {

				token = null;
				hide();
			}
			else if(sender.EVENT_ERROR.equals(event)){
				setInput();
				T.show("Не удалось отправить данный");
			}
			
		}
	}

}
