package ui.main.screens.targets;

import com.utils.Dimen;
import android.content.Context;
import ui.common.UISectionList.UISubItem;

public class UISubTop extends UISubItem
{

	public UISubTop(Context context)
	{
		super(context);
		setBackgroundColor(0x00000000);
	}

	@Override
	public Integer Height()
	{

		return Dimen.d10;
	}

}
