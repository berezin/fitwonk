package ui.main.screens.targets;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

import com.utils.Dimen;
import com.utils.EventsUtils.BindedRelativeLayout;
import com.utils.shorts.LPR;
import com.utils.shorts.Q;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.TextPaint;
import android.view.Gravity;
import android.view.MotionEvent;
import android.widget.TextView;
import common.DateFormats;
import common.Paints;
import common.Palette;
import common.Test;
import common.Type;
import data.DataAnthropometrys.DataAnthropometry;
import data.DataAnthropometrys.DataDateValue;
import fitwonk.Api;
import ui.common.UISectionList;

@SuppressLint("ClickableViewAccessibility")
public class UITargetGraph extends BindedRelativeLayout
{

	TextView title;
	
	int hText = Dimen.d30;
	boolean hasTextView = true;
	DataAnthropometry data;
	float offset = 0f;
	float offsetFixed = 0f;
	ArrayList<DataDateValue> logs;
	String strtitle;

	public UITargetGraph(Context context)
	{
		super(context);

		title = new TextView(getContext());
		Paints.SetText(title, Type.FuturaLight, 14, 0xff000000);
		title.setText("");
		title.setGravity(Gravity.CENTER_VERTICAL);
		addView(title, LPR.create(hText).marginLeft(Dimen.d15).get());
	}
	
	@Override
	public boolean onInterceptTouchEvent(android.view.MotionEvent ev)
	{

		UISectionList.IS_LOCKED_SWITCH = true;
		return true;
	};
	
	float lastX = -1, lastY, cur_x;
	int last_offset;
	boolean is_setted = false;
	boolean is_setted_locked = false;
	@Override
	public boolean onTouchEvent(MotionEvent arg0)
	{

		if(arg0.getAction() == MotionEvent.ACTION_UP || arg0.getAction() == MotionEvent.ACTION_CANCEL)
		{

			is_setted = false;
			lastX = -1;
			UISectionList.IS_LOCKED_SWITCH = false;

			if(is_setted_locked)
			{
				
				is_setted_locked = false;
				
				postInvalidate();
				
				return true;
			}

		} else if(arg0.getAction() == MotionEvent.ACTION_DOWN)
		{
			
			
			is_setted = false;
			is_setted_locked = false;
			lastX = arg0.getX();
			lastY = arg0.getY();
			
		}

		else if(arg0.getAction() == MotionEvent.ACTION_MOVE && lastX != -1)
		{
			
			if(!is_setted)
			{

				double r = Math.sqrt(Math.pow(lastY - arg0.getY(), 2)
						+ Math.pow(lastX - arg0.getX(), 2));
				
				if(r > 8)
				{
					if(Math.abs(lastX - arg0.getX()) < Math.abs(lastY
							- arg0.getY()))
					{
						lastX = -1;
						UISectionList.IS_LOCKED_SWITCH = false;
						is_setted_locked = false;
					}
					else
					{
						is_setted_locked = true;
						offsetFixed = offset;
						lastX = arg0.getX();
						lastY = arg0.getY();
						cur_x = lastX;
					}

					is_setted = true;
				}

			} else
			{

				if(is_setted_locked) {
					cur_x = arg0.getX();
					offset+=(cur_x - lastX);
					lastX = cur_x;
					invalidate();
				}
				
				


			}

		}

		if(!is_setted_locked)
		{
			super.onTouchEvent(arg0);
		}

		return true;

	}

	public void Set(String strtitle, DataAnthropometry data)
	{
		this.strtitle = strtitle;
		this.data = data;
		logs = null;
		needSetPoints = true;
		points = null;
		offset = 0f;

		if(strtitle == null)
		{
			title.setVisibility(INVISIBLE);
			hasTextView = false;
		} else
		{
			title.setVisibility(VISIBLE);
			title.setText(strtitle.toUpperCase());
			hasTextView = true;
		}

		invalidate();
	}

	public int HeightImage()
	{
		return (int) (Q.getW() * 0.75f) + Dimen.d30;
	}
	
	public int HeightBottom()
	{
		return Dimen.d10;
	}

	public int HeightText()
	{
		return hasTextView ? hText : 0;
	}

	public int Height()
	{
		return HeightImage() + HeightText() + HeightBottom();
	}

	Paint paintWhite = new Paint()
	{
		{
			setColor(0xffffffff);
		}
	};

	Paint paintGrayLine = new Paint()
	{
		{
			setAntiAlias(true);
			setColor(0x22000000);
			setStyle(Style.STROKE);
			setStrokeWidth(Dimen.d1);
		}
	};

	Paint paintBlackLine = new Paint()
	{
		{
			setAntiAlias(true);
			setColor(0xff000000);
			setStyle(Style.STROKE);
			setStrokeWidth(Dimen.d2);
		}
	};

	Paint paintBlueLine = new Paint()
	{
		{
			setAntiAlias(true);
			setColor(Palette.BLUE);
			setStyle(Style.STROKE);
			setStrokeWidth(Dimen.d3);
		}
	};
	
	Paint paintBlueLineThin = new Paint()
	{
		{
			setAntiAlias(true);
			setColor(Palette.BLUE);
			setStyle(Style.STROKE);
			setStrokeWidth(Dimen.d1);
		}
	};
	
	Paint paintBlue = new Paint()
	{
		{
			setColor(Palette.BLUE);
			setAntiAlias(true);
			setStrokeWidth(Dimen.d3);
		}
	};
	
	Paint paintGray = new Paint()
	{
		{
			
			setStyle(Style.FILL);
			setColor(0xffeeeeee);
		}
	};

	boolean needSetPoints = false;
	float[] points;
	float[] pointsTarget;
	float[] pointsLinesV;
	float[] pointsLinesH;
	String[] textsH;
	float fx, fy;
	
	
	TextPaint tp = new TextPaint() {{
		setColor(Palette.GRAY);
		setAntiAlias(true);
		setTextSize(Dimen.d10);
		//setTypeface(Type.FuturaLight());
	}};
	
	TextPaint tpNoData = new TextPaint() {{

		setColor(0xff000000);
		setAntiAlias(true);
		setTextSize(Dimen.d14);
		setTypeface(Type.Futura());
	}};
	
	
	TextPaint tpDate = new TextPaint() {{
		setColor(Palette.BLUE);
		setAntiAlias(true);
		setTextSize(Dimen.d10);
		//setTypeface(Type.FuturaLight());
	}};

	@Override
	protected void dispatchDraw(Canvas canvas)
	{

		super.dispatchDraw(canvas);

		canvas.save();
		canvas.translate(0, HeightText());

		int w = getMeasuredWidth();
		int h = HeightImage();
		Rect rect = new Rect(0, 0, w, h);
		Rect rectContent = new Rect(Dimen.d40, Dimen.d15,
				rect.right - Dimen.d20, rect.bottom - Dimen.d50);

		canvas.drawRect(rect, paintWhite);
		canvas.drawRect(rect, paintGrayLine);
		
		canvas.drawRect(rectContent, paintGray);

		canvas.drawLine(rectContent.left, rectContent.top, rectContent.left,
				rectContent.bottom, paintBlackLine);
		canvas.drawLine(rectContent.left, rectContent.bottom, rectContent.right,
				rectContent.bottom, paintBlackLine);

		int maxPoints = 6;

		if(logs == null) {
			
			logs = data.logs;
			
			if(Test.isFakeAnthGraphs) {
				int nGen = (int) (4 + Math.random() * 18);
				logs = new ArrayList<>();
				
				Long step = 1000L*60L*60L*24L*3L;
				Long time = System.currentTimeMillis() - step * (nGen - 1);
				
				for(int i = 0 ; i < nGen; i++) {
					DataDateValue l = new DataDateValue();
					l.value =  (Double) (34. + Math.random() * 20.);
					l.date = DateFormats.SimpleDate.format(new Date(time));
					time += step;
					
					logs.add(l);
				}
			}
			else {
				
				if(logs!=null)
				for(DataDateValue d: logs) {
					
					if(d.date == null || d.date.length()>5) {
						try
						{
							Long time = DateFormats.OnlyDate2.parse(d.date).getTime();
							d.date = DateFormats.SimpleDate(time);
						} catch (Exception e)
						{
						
							e.printStackTrace();
						}
						
					}
					
				}
				
			}
			
			
			
		}
		

		// drawLines
		if(logs != null && logs.size() > 0)
		{

			int offsetPoints = 0;
			int countPoints = 0;
			
			int offsetPointsV = 0;
			int countPointsV = 0;
			
			int realPoints = Math.min(maxPoints, logs.size() - 1);
			
			//if(realPoints == 0) 
			//	realPoints = 1;
			
			float between = realPoints != 0?rectContent.width() / realPoints:rectContent.width();
			
			if(realPoints < maxPoints) {
				
				if(realPoints != 0) {
					
					between = (rectContent.width()) / (realPoints + 2);
					
					offset = -between;
				}
				else {
					offset = -rectContent.width() * 0.5f;
				}
				
			}
			
			if(needSetPoints)
			{
				needSetPoints = false;

				Double max = Double.MIN_VALUE;
				Double min = Double.MAX_VALUE;

				for (DataDateValue l : logs)
				{
					if(max < l.value)
					{
						max = l.value;
					}
					if(min > l.value)
					{
						min = l.value;
					}
				}
				
				if(data.target!=null) {
					
					if(max < data.target)
					{
						max = data.target;
					}
					if(min > data.target)
					{
						min = data.target;
					}
				}
			
				if(max - min < 5)
				{
					max = min + 5;
				}

				float minReal = rectContent.bottom
						- rectContent.height() * 0.2f;
				float realX = rectContent.right;
				float hReal = rectContent.height() * 0.6f;
				
				
				int index = 0;
				pointsLinesH = new float[40];
				textsH = new String[10];
				Double step = (max - min) / 6.;
				Double start = min - step;
				DecimalFormat df = new DecimalFormat("###.#");
				int index2 = 0;
				for(float k = 0.1f; k < 1.1f ; k+=0.1f) {
					
					pointsLinesH[index++] = rectContent.left;
					pointsLinesH[index++] = rectContent.bottom - rectContent.height() * k;
					pointsLinesH[index++] = rectContent.right;
					pointsLinesH[index] = pointsLinesH[index - 2];
					index++;
					
					textsH[index2++] = df.format(start);
					start+=step;
					
				}
				
				int n = (logs.size() - 1);
				pointsLinesV = new float[(logs.size()) * 4];
				
				pointsLinesV[0] = realX - between * (n);
				pointsLinesV[1] = rectContent.top;
				pointsLinesV[2] = pointsLinesV[0];
				pointsLinesV[3] = rectContent.bottom;

				if(logs.size() != 1) {
					
					points = new float[(n) * 4];
					
					index = 0;
					for (int i = 0; i < n; i++)
					{

						if(i == 0)
						{
							DataDateValue l1 = logs.get(i);
							points[index] = realX - between * (n - i);
							
							index++;
							points[index] = (float) (minReal
									- hReal * (l1.value - min) / (max - min));
							index++;
						} else
						{
							points[index] = points[index - 2];
							index++;
							points[index] = points[index - 2];
							index++;
						}

						DataDateValue l2 = logs.get(i + 1);
						points[index] = points[index - 2] + between;
						
						pointsLinesV[4 + index - 2] = points[index];
						pointsLinesV[4 + index - 1] = rectContent.top;
						pointsLinesV[4 + index] = points[index];
						pointsLinesV[4 + index + 1] = rectContent.bottom;
						
						index++;
						points[index] = (float) (minReal
								- hReal * (l2.value - min) / (max - min));
						
						index++;
					}
				}
				
				if(data.target!=null) {
					
					float v = (float) (minReal
							- hReal * (data.target - min) / (max - min));
					
					pointsTarget = new float[]{ rectContent.left, v, rectContent.right, v };
				}
				
				
				fx = realX;
				fy = (float) (minReal
						- hReal * (logs.get(logs.size() - 1).value - min) / (max - min));

			}
			
			if(realPoints < maxPoints) {
				
				if(points!=null) {
					offsetPoints = 0;
					countPoints = points.length;
				}
				
				offsetPointsV = 0;
				countPointsV = pointsLinesV.length;
				
			}
			else {
				float maxOffset = between * (logs.size() - 1)
						- rectContent.width();
				offset = Math.min(offset, maxOffset);
				offset = Math.max(offset, 0f);
				
				offsetPoints = (logs.size() - 2 - maxPoints - (int)(offset / between)) * 4;
				
				if(offsetPoints < 0) {
					offsetPoints = 0;
				}
				
				countPoints = (maxPoints + 1) * 4;
				
				if(offsetPoints + countPoints >= points.length) {
					countPoints = points.length - offsetPoints;
				}
				
				offsetPointsV = offsetPoints;
				countPointsV = countPoints + 4;

			}
			
			if(pointsLinesH!=null)
			{
				canvas.drawLines(pointsLinesH, 0, pointsLinesH.length, paintGrayLine);
				int index = 0;
				Rect bounds = new Rect();
				
				for(int i = 0; i < pointsLinesH.length; i+=4) {
					
					String s = textsH[index];
					tp.getTextBounds(s, 0, s.length(), bounds);
					
					canvas.drawText(s, pointsLinesH[i] - bounds.width() - Dimen.d5, pointsLinesH[i+1] - bounds.centerY(), tp);
					index++;
				}
				
			}
			
			if(pointsTarget!=null)
			{
				canvas.drawLines(pointsTarget, 0, 4, paintBlueLineThin);
				
			}
			
			canvas.save();
			canvas.translate(offset, 0);
			
			rectContent.offset((int) -offset, 0);
			rectContent.right += Dimen.d5;
			
			canvas.save();
			canvas.clipRect(rectContent);
			
			if(pointsLinesV!=null) {
				canvas.drawLines(pointsLinesV, offsetPointsV, countPointsV, paintGrayLine);
			}
			
			
			
			if(points!=null)
			{
				canvas.drawLines(points, offsetPoints, countPoints, paintBlueLine);
				
			}
			
			canvas.drawCircle(fx, fy, Dimen.d5, paintBlue);
			
			
			canvas.restore();
			
			
			
				int i = offsetPointsV / 4;
				int n = i + countPointsV / 4;
				
				

				Float lastX = null;
				Rect bound = new Rect();
				Float xCurrent = null;
				
				for(int j = i; j < n ; j++) {
					
					String str = logs.get(j).date;
					
					if(lastX == null) {
						tpDate.getTextBounds(str, 0, str.length(), bound);
					}
					
					if(xCurrent == null) {
						xCurrent = pointsLinesV[j * 4] - bound.width() * 0.5f;
					}

					float x = xCurrent;
					
					xCurrent += between;
					
					if(lastX == null || x - lastX > bound.width() + Dimen.d10) {
						canvas.drawText(str, x, rectContent.bottom + bound.height() + Dimen.d5, tpDate);
						lastX = x;
					}
					
				}
			

			
			canvas.restore();

		}
		else {
			String str = "Нет ни одного замера";
			Rect bound = new Rect();
			tpNoData.getTextBounds(str, 0, str.length(), bound);
			canvas.drawText(str, rectContent.centerX() - bound.centerX(), rectContent.centerY() - bound.centerY(), tpNoData);
		}


		canvas.restore();

	}

	@Override
	public void handle(Integer event, Object param)
	{
		if(Api.Anthropometry.hasChangedEvent(event)) {
			
			DataAnthropometry d = Api.Anthropometry.search(data.id);
			
			if(d!=null) {
				Set(strtitle, d);
			}
			
		}
	}

}
