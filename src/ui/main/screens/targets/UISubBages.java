package ui.main.screens.targets;

import com.utils.Dimen;
import com.utils.shorts.LPR;
import com.utils.shorts.Q;

import android.content.Context;
import common.Res;
import ui.common.UISectionList.UISubItem;

public class UISubBages extends UISubItem
{

	@SuppressWarnings("deprecation")
	public UISubBages(Context context)
	{
		super(context);
		
		setBackgroundDrawable(Res.BACKGROUND_BLUE());
		
		//int n = Bages.Count();
		
		int marginH = Dimen.d20;
		int marginV = Dimen.d15;
		int padH = 0;
		int padV = Dimen.d5;
		
		int wi = (int) ((Q.getW() - padH * 2 - marginH * 2) / 3);
		int hi = (int) ((Height() - padV * 2 - marginV * 2) / 3);
		
		for (int i = 0; i < 9; i++) {
			
			int x = i % 3;
			int y = i / 3;
			
			UITargetsBage ui = new UITargetsBage(getContext());
			ui.Set(i);
			
			addView(ui, LPR.create(wi, hi).margins(marginH + x * ( wi + padH), marginV + y * ( hi + padV)).get());
			
		}
		
	}
	
	
	
	static Integer _h;
	@Override
	public Integer Height()
	{
		
		if(_h == null) {
			_h = (int) (Q.getW() * 1.1f);
		}
		
		return _h;
	}

}
