package ui.main.screens.targets;

import com.utils.shorts.LPR;
import com.utils.ui.ImageLayout;

import android.content.Context;
import android.view.Gravity;
import android.widget.TextView;
import common.Paints;
import common.Type;
import fitwonk.Bages;
import fitwonk.Bages.DataBage;
import ui.common.SmartRelativeLayout;

public class UITargetsBage extends SmartRelativeLayout
{

	ImageLayout image;
	TextView text;
	
	public UITargetsBage(Context context)
	{
		super(context);
		
		image = new ImageLayout(getContext());
		image.setId(1);
		image.setDefaultImageView();
		addView(image);
		
		text = new TextView(getContext());
		text.setGravity(Gravity.CENTER);
		Paints.SetText(text, Type.FuturaLight, 14, 0xffffffff);
		addView(text, LPR.create().below(1).get());
		
	}
	
	public void Set(int index) {
		
		DataBage bage = Bages.Get(index);
		
		boolean isCurrent = index == Bages.CurrentIndex();
		
		text.setTextColor(isCurrent? 0xffffffff: 0x55ffffff);
		text.setText(bage.getInterval());
		image.Set(isCurrent?bage.getHighlightBitmap():bage.getNormalBitmap(), true);
		image.setAlpha(index<=Bages.CurrentIndex()?1f:0.3f);
	}

	@Override
	public void handle(Integer event, Object param)
	{
		
	}

	@Override
	protected void onSetSize(int width, int height)
	{
		image.setLayoutParams(LPR.create(width, width).get());
	}

}
