package ui.main.screens;

import com.utils.shorts.LPR;
import android.content.Context;
import fitwonk.Api;
import ui.common.SmartRelativeLayout;
import ui.common.UITitle;
import ui.main.screens.calendar.UICalendar;
import ui.main.screens.settings.UISettings;
import ui.main.screens.targets.UITargets;

public class UIScreenSettings extends UIAbstractScreen
{

	UITitle uiTitle;
	UISettings uiSettings;
	
	public UIScreenSettings(Context context)
	{
		super(context);
		
		uiTitle = new UITitle(getContext());
		uiTitle.Set("НАСТРОЙКИ");
		addView(uiTitle, LPR.create(UITitle.Height).get());
		
		uiSettings = new UISettings(getContext());
		addView(uiSettings, LPR.create().marginTop(UITitle.Height).get());
	}

	
	
	@Override
	public void handle(Integer event, Object param)
	{
		
	}

	@Override
	protected void onSetSize(int width, int height)
	{
		
	}

}
