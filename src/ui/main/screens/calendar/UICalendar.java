package ui.main.screens.calendar;

import java.util.ArrayList;

import data.DataExercise;
import data.DataTraining;

import fitwonk.Api;
import ui.common.UISectionList;
import android.content.Context;

public class UICalendar extends UISectionList
{

	public UICalendar(Context context)
	{
		super(context);
		
		
		ArrayList<UIItem> views = new ArrayList<>();
		ArrayList<Integer> opened = new ArrayList<Integer>();
		
		ArrayList<DataTraining> trainings = Api.Init.getCacheData().trainings;
		
		int day = 1;
		int currentDay = Api.Init.getCacheData().currentDay();
		
		for(DataTraining t: trainings) {
			
			UISectionCalendar uiSection = new UISectionCalendar(getContext());
			uiSection.Set(day, t);
			views.add(uiSection);
			
			ArrayList<DataExercise> exs = Api.Init.getCacheData().getExercises(t);
			

			
			int index = 1;
			int n = exs.size();
			for(DataExercise ex: exs) {
				
				
				UISubExercise uiSub = new UISubExercise(getContext());
				uiSub.Set(index, day, ex, n == index);
				
				views.add(uiSub);
				index ++;
			}
			
			if(day == currentDay) {
				views.add(new UISubBeginWord(getContext()));
			}
			
			
			day ++ ;
			
		}
		
		Integer index = Api.Init.getCacheData().currentDay() - 1;
		opened.add(index);
		
		Adapter adapter = new Adapter(
				views, opened);
		
		Set(adapter);
		
		
	}

}
