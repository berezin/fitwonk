package ui.main.screens.calendar;

import common.Res;
import ui.common.UISectionList.UISubItem;

import com.utils.shorts.LPR;
import com.utils.shorts.Q;
import com.utils.Dimen;
import activity.ActivityMain;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;
import android.widget.ImageView;

public class UISubBegin extends UISubItem
{

	ImageView image;
	
	
	static Paint paintBottom = new Paint(){{
		setColor(0xfff6f6f8);
	}};
	
	static Paint paintLine = new Paint(){{
		setColor(0xffdddddd);
		setAntiAlias(true);
	}};
	
	
	
	
	public UISubBegin(Context context)
	{
		super(context);
		
		setBackgroundColor(0xffffffff);
		
		image = new ImageView(getContext());
		int wImage =  (int) (Q.getW() * 0.35f);
		addView(image, LPR.create(wImage, wImage).center().get());
		image.setImageResource(Res.IMG_PLAYER_BEGIN);
		
		image.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				ActivityMain.ToTraining(getContext());
			}
		});
		
	}
	
	
	Integer _height;
	@Override
	public Integer Height()
	{
		if(_height == null) {
			_height = (int) (Q.getW() * 0.45f);
		}
		return _height;
	}
	
	@Override
	protected void dispatchDraw(Canvas canvas)
	{
		
		canvas.drawRect(new Rect(0, Height() / 2, getMeasuredWidth(), getMeasuredHeight()), paintBottom);
		
		canvas.drawRect(new Rect(0, Height() / 2, getMeasuredWidth(), Height() / 2 + Dimen.d1), paintLine);
		
		super.dispatchDraw(canvas);
	}

}
