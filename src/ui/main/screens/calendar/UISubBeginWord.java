package ui.main.screens.calendar;

import common.Paints;
import common.Palette;
import common.Type;
import common.painter.TextDrawer;
import common.painter.ViewDrawer;
import ui.common.UISectionList.UISubItem;
import ui.common.UISectionList.UISubItemView;

import com.utils.shorts.D;
import com.utils.shorts.LPR;
import com.utils.Dimen;
import activity.ActivityMain;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

public class UISubBeginWord extends UISubItemView
{

	TextDrawer text;
	

	
	public UISubBeginWord(Context context)
	{
		super(context);
		
		setBackgroundColor(0xffffffff);
		
		text = new TextDrawer(this);
		
		

		text.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				ActivityMain.ToTraining(getContext());
			}
		});
		
		ViewDrawer lineBottom = new ViewDrawer(this);
		lineBottom.setBackgroundColor(0x22000000);
		lineBottom.layout().calcLineH(Dimen.d1).bottom();
		addDrawer(lineBottom);
		
		runAsyncSet();
	}
	
	
	@Override
	protected void asyncSet()
	{
		text.setPadding(Dimen.d15, Dimen.d10, Dimen.d15, Dimen.d10);
		text.set("НАЧАТЬ", Paints.GetPaint(Type.Futura, 14, Palette.BLUE));
		text.setBackgroundColor(0x11000000, 0x0a000000);
		text.layout().calcCenterH(FixWidth()).calcCenterV(FixHeight());
		addDrawer(text);
		//D.Set(text, D.Highlight(, 0x05000000));
	}
	
	
	@Override
	public Integer Height()
	{
		return Dimen.d50;
	}
	

}
