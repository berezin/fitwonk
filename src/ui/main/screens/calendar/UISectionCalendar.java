package ui.main.screens.calendar;

import common.Paints;
import common.Palette;
import common.Type;
import common.painter.TextDrawer;
import common.painter.ViewDrawer;

import com.utils.Dimen;
import data.DataTraining;
import fitwonk.Api;
import ui.common.UISectionList.UISectionItemView;
import android.content.Context;

public class UISectionCalendar extends UISectionItemView
{

	ViewDrawer background;
	TextDrawer textDayIndex;
	TextDrawer textDayWord;
	
	public UISectionCalendar(Context context)
	{
		super(context);
		
		
		background = new ViewDrawer(this);
		background.setOnClickListener(this);
		setClickable(false);
		background.layout(FixWidth(), HeightContent()).marginTop(Dimen.d4);
		addDrawer(background);
		

		textDayIndex = new TextDrawer(this);
		textDayWord = new TextDrawer(this);

		
		
		ViewDrawer line = new ViewDrawer(this);
		line.setBackgroundColor(0x22000000);
		line.layout().calcLineH(Dimen.d1).bottom();
		addDrawer(line);
		
		line = new ViewDrawer(this);
		line.setBackgroundColor(0x22000000);
		line.layout().calcLineH(Dimen.d1).marginTop(Dimen.d4);
		addDrawer(line);

		
	}
	
	boolean isCurrent;
	boolean isInFuture;
	int day;
	
	
	
	
	@Override
	protected void asyncSet()
	{
		textDayIndex.set(""+day + " ", Paints.GetPaint(Type.Futura, 25, isCurrent? 0xffffffff: isInFuture?Palette.GRAY:Palette.BLUE));
		textDayIndex.layout().marginLeft(Dimen.d20).marginTop(Dimen.d4).calcCenterV(HeightContent());
		
		textDayWord.set("день", Paints.GetPaint(Type.FuturaLight, 18, isCurrent? 0xffffffff: isInFuture?Palette.GRAY:Palette.BLUE));
		textDayWord.layout().rightOf(textDayIndex.layout()).marginTop(Dimen.d4).calcCenterV(HeightContent());

		addDrawer(textDayIndex);
		addDrawer(textDayWord);
		
	}
	
	
	@Override
	public Integer Height()
	{
		return Dimen.d64;
	}
	
	public Integer HeightContent()
	{
		return Dimen.d60;
	}
	
	public void Set(int day, DataTraining training) {
		
		this.day = day;
		int current = Api.Init.getCacheData().currentDay();
		isCurrent = day == current;
		isInFuture = day > current;
		
		
		
		if(isCurrent) {
			background.setBackgroundColor(Palette.BLUE);
			background.getRipple().setColor(Palette.WHITE_RIPPLE);
		}
		else {
			background.setBackgroundColor(0xffffffff);
			background.getRipple().setColor(Palette.BLUE_RIPPLE);
		}
		
		runAsyncSet();

		
	}

}
