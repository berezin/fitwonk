package ui.main.screens.calendar;

import common.Paints;
import common.Palette;
import common.Type;
import common.painter.ImageDrawer;
import common.painter.TextDrawer;
import common.painter.ViewDrawer;

import com.utils.shorts.Q;
import com.utils.Dimen;
import data.DataExercise;
import fitwonk.Api;
import fitwonk.Icons;
import activity.ActivityMain;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.StaticLayout;
import android.view.View;
import ui.common.UISectionList.UISubItemWithDescView;
import ui.main.screens.myday.InfoConst;

public class UISubExercise extends UISubItemWithDescView
{

	ViewDrawer background;
	
	TextDrawer txtIndex;
	TextDrawer txtName;
	TextDrawer txtViewFullDesc;
	
	ViewDrawer lineTop;
	ViewDrawer lineBottom;
	
	ImageDrawer imageIc;
	StaticLayout slDesc;

	DataExercise exersice;
	Integer colorIc;
	boolean isFirstOpen = true;

	static int heightButton = Dimen.d35;
	static int marginButton = Dimen.d10;
	static int marginBottom = Dimen.d4;

	public static Integer WIDTH_BLUE = Dimen.d80;

	Paint paintSolid;
	Paint paintLine = new Paint()
	{
		{
			setColor(0x22000000);
		}
	};

	static Paint paintRectInFuture = new Paint()
	{
		{
			setColor(0x11000000);
		}
	};

	static Paint paintRectInPast = new Paint()
	{
		{
			setColor(Palette.DARK_GRAY);
		}
	};

	public void onOpen()
	{
		setBackgroundColor(0xffeeeeee);
		slDesc = exersice.createDescriptionLayout(DescW());
		if(txtViewFullDesc == null) {
			
			txtViewFullDesc = new TextDrawer(this);
			txtViewFullDesc.setPadding(Dimen.d10, Dimen.d10, Dimen.d10, Dimen.d10);
			txtViewFullDesc.set("Посмотреть инструкцию", Paints.GetPaint(Type.FuturaLight, 14, 0xff000000));
			txtViewFullDesc.setBackgroundColor(0x11000000, 0x0a000000);
			txtViewFullDesc.layout().marginTop(Height() + slDesc.getHeight() + marginButton).marginLeft(WIDTH_BLUE + Dimen.d10);
			txtViewFullDesc.getRipple().setClickDelayed(true);
			addDrawer(txtViewFullDesc);
			
			heightButton = txtViewFullDesc.Height();

			
			txtViewFullDesc.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					ActivityMain.ToDescription(v.getContext(), exersice);
				}
			});
		}
		else {
			txtViewFullDesc.show();
		}
	
	}

	@Override
	public void onShow()
	{
		if(isFirstOpen || !imageIc.hasBitmap())
		{
			Icons.Get(exersice.icon, colorIc, new Icons.ICallback()
			{

				@Override
				public void post(Bitmap bitmap)
				{
					imageIc.Set(bitmap, true);
				}
			});

			isFirstOpen = false;
		}
	}

	@Override
	public void onClose()
	{
		setBackgroundColor(0xfff8f8f8);
		
		if(txtViewFullDesc != null) {
			txtViewFullDesc.hide();
		}
	}

	
	public UISubExercise(Context context)
	{
		super(context);

		onClose();
		
		
		background = new ViewDrawer(this);
		background.getRipple().setActive(true, false);
		addDrawer(background);
		
		txtIndex = new TextDrawer(this);
		
		
		lineBottom = new ViewDrawer(this);
		lineBottom.setBackgroundColor(0x11000000);
		addDrawer(lineBottom);
		
		lineTop = new ViewDrawer(this);
		lineTop.setBackgroundColor(0x11000000);
		lineTop.layout().calcLineH(Dimen.d1);
		addDrawer(lineTop);
		
		txtName = new TextDrawer(this);
		
		
		
		
		
		imageIc = new ImageDrawer(this);
		int pad = Dimen.d4;
		imageIc.setPadding(pad);
		imageIc.layout(Height(), Height()).marginLeft(WIDTH_BLUE - Height());
		
		addDrawer(imageIc);

	}

	
	public Integer HeightContentFrame()
	{
		return Dimen.d55;
	}
	
	@Override
	public Integer Height()
	{
		return HeightContentFrame() + marginBottom;
	}

	@Override
	public Integer HeightDesc()
	{
		return Height() + slDesc.getHeight() + marginButton + heightButton + marginButton + marginBottom;
	}

	public Integer DescX()
	{
		return (int) (WIDTH_BLUE + Dimen.d10);
	}

	public Integer DescW()
	{
		return (int) (Q.getW() - Dimen.d10 - DescX());
	}

	
	
	
	String strIndex;
	int colorIndex;
	@Override
	protected void asyncSet()
	{
		int left = WIDTH_BLUE + Dimen.d10;
		txtName.set(exersice.name, (int)(Q.getW() - left - Dimen.d10), Paints.GetPaint(Type.FuturaLight, 15, Palette.BLUE));
		txtName.layout().marginLeft(left).calcCenterV(HeightContentFrame());
		
		txtIndex.set(strIndex, Paints.GetPaint(Type.Futura, 38, colorIndex));
		txtIndex.layout().marginLeft(Dimen.d5);

		addDrawer(txtName);
		addDrawer(txtIndex);
		
	}

	public void Set(int index, int day, DataExercise ex, boolean isLast)
	{

		exersice = ex;

		strIndex = "" + index;
		
		int current = Api.Init.getCacheData().currentDay();
		boolean isCurrent = day == current;
		boolean isInFuture = day > current;
		
		colorIndex = 0x33ffffff;
		//int alphaIndex = 50;
		
		if(isInFuture)
		{
			colorIndex = 0x22000000;
			//alphaIndex = 35;
			paintSolid = paintRectInFuture;

			paintLine.setColor(0x00ffffff);
			
		}
		else
		{
			if(isCurrent)
			{
				paintSolid = InfoConst.PAINT_BLUE;
			}
			else
			{
				paintSolid = paintRectInPast;
			}
		}

		

		colorIc = isInFuture ? 0xffaaaaaa : 0xffffffff;

		if(isLast) {
			lineBottom.layout().calcLineH(Dimen.d1).bottom();
			background.layout().marginBottom(0);
		}
		else {
			lineBottom.layout().calcLineH(Dimen.d1).bottom().marginBottom(marginBottom);
			background.layout().marginBottom(marginBottom);
		}
		
		runAsyncSet();

	}
	
	
	@Override
	protected void dispatchDrawBackground(Canvas canvas)
	{
		super.dispatchDrawBackground(canvas);
		
		canvas.drawRect(new Rect(0, 0, WIDTH_BLUE, getMeasuredHeight()),
				paintSolid);
		canvas.drawRect(new Rect(WIDTH_BLUE - Dimen.d4, 0, WIDTH_BLUE,
				getMeasuredHeight()), paintLine);

		if(!isMin)
		{
			canvas.save();
			canvas.translate(DescX(), Height());
			slDesc.draw(canvas);
			canvas.restore();
		}
	}

}
