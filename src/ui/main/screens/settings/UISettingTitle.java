package ui.main.screens.settings;

import com.utils.Dimen;
import com.utils.shorts.Q;

import android.content.Context;
import common.Paints;
import common.Type;
import common.painter.TextDrawer;
import ui.common.UISectionList.UISubItemView;

public class UISettingTitle extends UISubItemView
{

	TextDrawer textName;
	
	public UISettingTitle(Context context, String name)
	{
		super(context);

		textName = new TextDrawer(this, name,
				Paints.GetPaint(Type.FuturaLight, 14, 0xff000000));
		textName.layout().calcCenterV(FixHeight()).marginLeft(Dimen.d15);
		addDrawer(textName);
	}

	@Override
	public Integer Height()
	{
		return Dimen.d30;
	}

}
