package ui.main.screens.settings;

import com.utils.Dimen;
import com.utils.shorts.I;
import com.utils.shorts.LPR;
import com.utils.shorts.Q;

import activity.ActivityMain;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import common.Paints;
import common.Palette;
import common.Res;
import common.Type;
import common.painter.TextDrawer;
import common.painter.ViewDrawer;
import fitwonk.Api;
import ui.common.UICircleImage;
import ui.common.UISectionList.UISubItem;

public class UISettingAvatar extends UISubItem
{

	UICircleImage image;
	TextDrawer text;
	
	
	Integer ImageSize = Height() - Dimen.d30;
	
	public UISettingAvatar(Context context)
	{
		super(context);
		
		setBackgroundColor(0xffffffff);

		image = new UICircleImage(getContext());
		image.setBorderColor(0xffffffff);
		image.setBorderWidth(Dimen.d2);
		image.setBorderColor2(Palette.BLUE);
		image.setBorderWidth2(Dimen.d3);
		
		addView(image, LPR.create(ImageSize, ImageSize).centerV().marginLeft(Dimen.d15).get());
		I.Load(image, Api.Init.getCacheData().icon);
		
		if(!image.hasImage())
			image.setImageResource(Res.IMG_PLACEHOLDER_CIRCLE_GRAY);
		
		
		text = new TextDrawer(this);
		text.setPadding(Dimen.d15, Dimen.d10, Dimen.d15, Dimen.d10);
		text.set("ИЗМЕНИТЬ", Paints.GetPaint(Type.Futura, 14, Palette.BLUE));
		text.setBackgroundColor(0x11000000, 0x0a000000);
		text.layout().calcCenterV(Height()).marginLeft(Height());
		addDrawer(text);
		text.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				Api.Settings.takePictureFromGallery(v.getContext());
			}
		});

		ViewDrawer line = new ViewDrawer(this);
		line.setBackgroundColor(0x22000000);
		line.layout().calcLineH(Dimen.d1).bottom();
		addDrawer(line);
		
		line = new ViewDrawer(this);
		line.setBackgroundColor(0x22000000);
		line.layout().calcLineH(Dimen.d1);
		addDrawer(line);
	}


	public void SetValue(String url)
	{
		I.Load(image, url);
	}

	
	@Override
	public Integer Height()
	{
		return (int) (Q.getW() / 2.3);
	}


}
