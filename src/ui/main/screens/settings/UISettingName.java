package ui.main.screens.settings;

import com.utils.Dimen;
import com.utils.shorts.I;
import com.utils.shorts.K;
import com.utils.shorts.LPR;
import com.utils.shorts.Q;

import activity.ActivityMain;
import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import common.Paints;
import common.Palette;
import common.Res;
import common.Type;
import common.painter.TextDrawer;
import common.painter.ViewDrawer;
import data.DataDialogs.DataComment;
import fitwonk.Api;
import ui.common.UICircleImage;
import ui.common.UIEntry;
import ui.common.UISectionList.UISubItem;

public class UISettingName extends UISubItem
{

	
	UIEntry editName;
	public UIEntry EditName() {
		return editName;
	}
	
	public static interface ICallback {
		void setName(String name);
	}
	
	ICallback callback;
	
	public void setCallback(ICallback callback) {
		this.callback = callback;
	}

	static Integer Height = Dimen.d60;
	
	public UISettingName(Context context)
	{
		super(context);

		editName = new UIEntry(getContext());
		addView(editName, LPR.create(Height).get());
		editName.setBackgroundColor(0xffffffff);
		
		editName.setText(Api.Init.getCacheData().name);
		
		editName.setHint("Введите имя...");
		editName.setHintTextColor(0x44000000);
		editName.setImeOptions(EditorInfo.IME_ACTION_SEND);
		editName.setSingleLine();
		editName.setOnEditorActionListener(new OnEditorActionListener()
		{

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event)
			{
				if(actionId == EditorInfo.IME_ACTION_SEND || event.getAction() == KeyEvent.ACTION_DOWN &&
		                event.getKeyCode() == KeyEvent.KEYCODE_ENTER)
				{
					
					if(editName.getText().length() != 0) {
						
						String name = editName.getText().toString();
						
						if(callback!=null) {
							callback.setName(name);
						}
						
						K.Close(editName);
					}

				}
				return true;
			}
		});
		
		ViewDrawer line = new ViewDrawer(this);
		line.setBackgroundColor(0x22000000);
		line.layout().calcLineH(Dimen.d1).bottom();
		addDrawer(line);
		
		line = new ViewDrawer(this);
		line.setBackgroundColor(0x22000000);
		line.layout().calcLineH(Dimen.d1);
		addDrawer(line);
	}

	@Override
	public Integer Height()
	{
		return Height;
	}

}
