package ui.main.screens.settings;

import com.utils.Dimen;

import android.content.Context;
import fitwonk.Api;
import ui.common.UISectionList.UISubItem;
import ui.splash.form.UIFormDate;

public class UISubBirthDate extends UISubItem
{

	UIFormDate uiDate;
	String lastValue;
	public static interface ICallback {
		void setDate(String date);
	}
	
	ICallback callback;
	
	public void setCallback(ICallback callback) {
		this.callback = callback;
	}
	
	public UISubBirthDate(Context context)
	{
		super(context);
		uiDate = new UIFormDate(getContext());
		lastValue = Api.Init.getCacheData().birthday;
		uiDate.setValue(lastValue);

		uiDate.setCallback(new UIFormDate.ICallback()
		{
			
			@Override
			public void setDate(String date)
			{
				if(callback!=null && !lastValue.equals(date)) {
					callback.setDate(date);
				}
				
				lastValue = date;
			}
		});
		
		
		addView(uiDate);
		
	}
	
	@Override
	public Integer Height()
	{
		
		return Dimen.d60;
	}

	
	public String getValue()
	{
		return uiDate.getValue();
	}

}
