package ui.main.screens.settings;

import java.util.ArrayList;

import com.utils.EventsUtils.IBinder;
import com.utils.InternalService;
import com.utils.shorts.E;
import com.utils.shorts.T;

import android.content.Context;
import android.view.View;
import common.Res;
import data.DataOption;
import data.DataRegisterDict;
import data.DataUploadImageResult;
import data.DataUploadUserResult;
import fitwonk.Api;
import ui.common.UISectionList;
import ui.main.screens.dialogs.UISectionIconText;
import ui.main.screens.settings.UISettingName.ICallback;
import ui.main.screens.targets.UISubBages;
import ui.main.screens.targets.UISubTop;

public class UISettings extends UISectionList implements IBinder
{

	UISectionIconText targets;

	// selection
	DataOption sendTargetOption;
	String sendDate;
	String sendName;
	//
	UISubBirthDate uiDate;
	UISettingAvatar uiAvatar;

	public UISettings(Context context)
	{
		super(context);
		Set();
	}

	private void Set()
	{

		ArrayList<UIItem> views = new ArrayList<>();

		UISubItem item;
		UISectionIconText uiSection;

		uiSection = new UISectionIconText(getContext());
		uiSection.Set(Res.IC_PROFILE, "ПРОФИЛЬ");
		views.add(uiSection);

		SetProfile(views);

		uiSection = new UISectionIconText(getContext());
		uiSection.Set(Res.IC_ANTH, "АНТРОПОМЕТРИЯ");
		views.add(uiSection);

		SetAnth(views);

		targets = new UISectionIconText(getContext());
		targets.Set(Res.IC_TARGETS, "ЦЕЛИ");
		views.add(targets);

		SetTargets(views);

		uiSection = new UISectionIconText(getContext());
		uiSection.Set(Res.IC_NOTIFICATION, "НАПОМИНАНИЯ");
		views.add(uiSection);

		item = new UISubBages(getContext());
		views.add(item);

		Adapter adapter = new Adapter(views, null);

		Set(adapter);
	}

	private void SetProfile(ArrayList<UIItem> views)
	{
		views.add(new UISettingTitle(getContext(), "Фотография профиля"));
		views.add(uiAvatar = new UISettingAvatar(getContext()));
		views.add(new UISettingTitle(getContext(), "Имя"));

		UISettingName uiName = new UISettingName(getContext());
		sendName = Api.Init.getCacheData().name;
		views.add(uiName);
		uiName.setCallback(new ICallback()
		{

			@Override
			public void setName(String name)
			{
				sendName = name;
				Send();
			}
		});

	}

	private void SetAnth(ArrayList<UIItem> views)
	{
		views.add(new UISubTop(getContext()));
		views.add(new UISettingTitle(getContext(), "Дата рождения"));
		uiDate = new UISubBirthDate(getContext());
		sendDate = uiDate.getValue();
		views.add(uiDate);
		uiDate.setCallback(new UISubBirthDate.ICallback()
		{

			@Override
			public void setDate(String date)
			{
				sendDate = date;
				Send();
			}
		});

	}

	private void SetTargets(ArrayList<UIItem> views)
	{

		views.add(new UISubTop(getContext()));

		DataRegisterDict dict = Api.Settings.Dictionary.getCacheData();
		final ArrayList<UISetting> uis = new ArrayList<>();
		DataOption purpose = Api.Init.getCacheData().purpose;
		int startSelection = purpose != null ? purpose.id : 0;

		if(dict != null)
		{

			if(dict.purpose != null)
			{

				for (final DataOption option : dict.purpose)
				{

					final UISetting item = new UISetting(getContext(),
							option.name);
					uis.add(item);
					views.add(item);

					if(startSelection == option.id)
					{
						item.SetValue(true);
						sendTargetOption = option;
					}

					item.setOnClickListener(new OnClickListener()
					{

						@Override
						public void onClick(View v)
						{

							if(!item.GetValue())
							{

								for (UISetting ui : uis)
								{

									ui.SetValue(false);

								}

								item.SetValue(true);

								sendTargetOption = option;

								Send();

							}

						}
					});

				}

				if(sendTargetOption == null)
				{
					sendTargetOption = dict.purpose.get(0);
					Send();
				}

			}

		} else
		{
			Api.Settings.Dictionary.Send();
		}

	}
	
	
	InternalService servise = new InternalService()
	{
		
		@Override
		public void process()
		{
			if(sendName!=null && sendDate!=null && sendTargetOption!=null)
			{
				Api.Settings.UploadUser.Send(sendName, sendDate, sendTargetOption.id);
				stop();
			}
		}
		
		@Override
		public int getTimeout()
		{
			return 30000;
		}
	};

	private void Send()
	{
		servise.start(5000);
	}

	@Override
	protected void onAttachedToWindow()
	{
		super.onAttachedToWindow();
		E.Bind(this);
	}

	@Override
	protected void onDetachedFromWindow()
	{
		super.onDetachedFromWindow();
		E.Unbind(this);
	}

	@Override
	public void handle(Integer event, Object param)
	{
		if(Api.Settings.Dictionary.hasOKEvent(event))
		{
			Set();
		} else if(Api.Settings.UploadImage.hasOKEvent(event))
		{

			DataUploadImageResult data = Api.Settings.UploadImage
					.getData(param);

			uiAvatar.SetValue(data.icon);

		} else if(Api.Settings.UploadImage.hasErrorEvent(event))
		{

			DataUploadImageResult data = Api.Settings.UploadImage
					.getData(param);

			if(data!=null && data.error!=null) {
				T.show(data.error);
			}
			else {
				T.show(Res.STR_ERROR_CONNECTION);
			}

		}
		else if(Api.Settings.UploadUser.hasOKEvent(event))
		{
			DataUploadUserResult data = Api.Settings.UploadUser
					.getData(param);

		}
		else if(Api.Settings.UploadUser.hasErrorEvent(event))
		{

			DataUploadUserResult data = Api.Settings.UploadUser
					.getData(param);

			if(data!=null && data.error!=null) {
				T.show(data.error);
			}
			else {
				servise.start(servise.getTimeout());
			}

		}
	}

}
