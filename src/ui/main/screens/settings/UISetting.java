package ui.main.screens.settings;

import com.utils.Dimen;
import com.utils.shorts.Q;

import android.content.Context;
import common.Paints;
import common.Palette;
import common.Type;
import common.painter.ImageDrawer;
import common.painter.ParentViewLayout;
import common.painter.TextDrawer;
import ui.common.UISectionList.UISubItemView;

public class UISetting extends UISubItemView
{

	TextDrawer textName;
	ImageDrawer imageCheck;
	Boolean value = false;
	
	public UISetting(Context context, String name)
	{
		super(context);

		textName = new TextDrawer(this, name,
				Paints.GetPaint(Type.FuturaLight, 14, 0xff000000));
		textName.layout().calcCenterV(FixHeight()).marginLeft(Dimen.d15);
		addDrawer(textName);

		imageCheck = new ImageDrawer(this);
		imageCheck.layout(Dimen.d30, Dimen.d30).calcCenterV(FixHeight()).right()
				.marginRight(Dimen.d15);
		addDrawer(imageCheck);
	}

	public boolean GetValue()
	{
		return value;
	}
	
	public void SetValue(Boolean value)
	{
		this.value = value;
		imageCheck.setBackgroundColor(value ? Palette.BLUE : 0x00000000);
	}

	@Override
	public int FixWidth()
	{
		return (int) Q.getW();
	}

	@Override
	public int FixHeight()
	{
		return Dimen.d50;
	}

}
