package ui.main.screens;

import common.Paints;
import common.Res;
import common.Type;
import fitwonk.Api;
import fitwonk.Training;
import fitwonk.Training.Player;
import com.utils.shorts.LPR;
import com.utils.shorts.D;
import com.utils.shorts.Q;
import com.utils.Dimen;

import ui.common.UITitle;
import ui.main.screens.training.view.UITrainingDayAndDate;
import ui.main.screens.training.view.UITrainingTitleExercise;
import ui.main.screens.training.view.UITrainingTitleNextExercise;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class UIScreenTraining extends UIAbstractScreen
{

	UITitle uiTitle;
	UITrainingDayAndDate uiDayAndDate;
	UITrainingTitleExercise uiTitleAndDesc;
	UITrainingTitleNextExercise uiTitleAndDescNext;
	RelativeLayout viewBottom;
	View viewBottomLine;
	ImageView imagePlay;
	RelativeLayout frameContent;
	ImageView imageMute;
	ImageView imageHelp;
	TextView textRestart;
	TextView textExit;
	TextView textTimeCurrent;
	TextView textTimeAll;
	
	
	
	
	static Integer _HeightBottomBar;
	public static Integer HeightBottomBar() {
		
		if(_HeightBottomBar == null) {
			_HeightBottomBar = (int) (Q.getW() * 0.25f);
		}
		
		return _HeightBottomBar;
	}
	
	static Integer _HeightImage;
	public static Integer HeightImage() {
		
		if(_HeightImage == null) {
			_HeightImage = (int) (Q.getW() * 0.35f);
		}
		
		return _HeightImage;
	}
	
	
	
	public ImageView getPlayButton() {
		return imagePlay;
	}
	
	public ImageView getMuteButton() {
		return imageMute;
	}
	
	public ImageView getHelpButton() {
		return imageHelp;
	}
	
	public TextView getRestartButton() {
		return textRestart;
	}
	
	public TextView getExitButton() {
		return textExit;
	}
	
	public TextView getTextCurrentPosition() {
		return textTimeCurrent;
	}
	
	public TextView getTextAllDuration() {
		return textTimeAll;
	}
	
	public RelativeLayout getContentFrame() {
		return frameContent;
	}
	
	public RelativeLayout getBottomFrame() {
		return viewBottom;
	}
	
	public UITrainingTitleExercise getTitleExercise() {
		return uiTitleAndDesc;
	}
	
	public UITrainingTitleNextExercise getTitleNextExercise() {
		return uiTitleAndDescNext;
	}
	
	@SuppressWarnings("deprecation")
	public UIScreenTraining(Context context)
	{
		super(context);
		
		uiTitle = new UITitle(getContext());
		uiTitle.Set("ТРЕНИРОВКА");
		
		addView(uiTitle, LPR.create(UITitle.Height).get());
		
		
		uiDayAndDate = new UITrainingDayAndDate(getContext());
		addView(uiDayAndDate, LPR.create(UITrainingDayAndDate.Height).marginTop(UITitle.Height).get());
		
		frameContent = new RelativeLayout(getContext());
		frameContent.setBackgroundDrawable(Res.BACKGROUND_BLUE());
		addView(frameContent, LPR.create().margins(0,UITitle.Height + UITrainingDayAndDate.Height,0, HeightBottomBar()).get());
		
		
		viewBottom = new RelativeLayout(getContext());
		viewBottom.setBackgroundColor(0xfff6f6f8);
		addView(viewBottom, LPR.create(HeightBottomBar()).bottom().get());
		
		
		viewBottomLine = new View(getContext());
		viewBottomLine.setBackgroundColor(0xffdddddd);
		addView(viewBottomLine, LPR.create(Dimen.d1).bottom().marginBottom(HeightBottomBar()).get());
		
		imagePlay = new ImageView(getContext());
		imagePlay.setId(1023);
		addView(imagePlay, LPR.create(HeightImage(), HeightImage()).centerH().bottom().marginBottom(HeightBottomBar() - HeightImage() / 2).get());
		imagePlay.setImageResource(Res.IMG_PLAYER_START);
		
		uiTitleAndDesc = new UITrainingTitleExercise(getContext());
		addView(uiTitleAndDesc, LPR.create(UITrainingTitleExercise.HEIGHT).margins(0,UITitle.Height + UITrainingDayAndDate.Height,0).get());
		
		
		int marginRL = Dimen.d20;
		int marginTop = UITitle.Height + UITrainingDayAndDate.Height + Dimen.d10;
		int sizeImg = Dimen.d45;
		
		imageMute = new ImageView(getContext());
		imageMute.setImageResource(Res.IMG_PLAYER_MUTE);
		addView(imageMute, LPR.create(sizeImg, sizeImg).margins(marginRL, marginTop).get());
		
		imageHelp = new ImageView(getContext());
		imageHelp.setImageResource(Res.IMG_PLAYER_HELP);
		addView(imageHelp, LPR.create(sizeImg, sizeImg).margins(0, marginTop, marginRL).right().get());
		
		uiTitleAndDescNext = new UITrainingTitleNextExercise(getContext());
		addView(uiTitleAndDescNext, LPR.create(HeightBottomBar() - HeightImage() / 2 + Dimen.d5).bottom().get());
		
		
		int widthButton = Dimen.d80;
		int heightButton = Dimen.d35;
		int marginButton = Dimen.d5;
		
		textRestart = new TextView(getContext());
		Paints.SetText(textRestart, Type.FuturaLight, 14, 0xff000000);
		textRestart.setText("Сначала");
		textRestart.setGravity(Gravity.CENTER);
		textRestart.setBackgroundDrawable(D.Highlight(0xffe6e6e6, 0xffffffff));
		addView(textRestart, LPR.create(widthButton, heightButton).
				marginBottom(HeightBottomBar() - marginButton - heightButton).
				marginRight(marginButton).leftOf(1023).bottom().get());
		
		textExit = new TextView(getContext());
		Paints.SetText(textExit, Type.FuturaLight, 14, 0xff000000);
		textExit.setText("Выход");
		textExit.setGravity(Gravity.CENTER);
		textExit.setBackgroundDrawable(D.Highlight(0xffe6e6e6, 0xffffffff));
		addView(textExit, LPR.create(widthButton, heightButton).
				marginBottom(HeightBottomBar() - marginButton - heightButton).
				marginLeft(marginButton).rightOf(1023).bottom().get());
		
		 
		textTimeCurrent = new TextView(getContext());
		Paints.SetText(textTimeCurrent, Type.FuturaLight, 13, 0xffffffff);
		textTimeCurrent.setText("");
		addView(textTimeCurrent, LPR.createWrap().
				marginBottom(HeightBottomBar() + marginButton).
				marginRight(marginButton).leftOf(1023).bottom().get());
		
		textTimeAll = new TextView(getContext());
		Paints.SetText(textTimeAll, Type.FuturaLight, 13, 0xffffffff);
		textTimeAll.setText("");
		addView(textTimeAll, LPR.createWrap().
				marginBottom(HeightBottomBar() + marginButton).
				marginLeft(marginButton).rightOf(1023).bottom().get());
		
		hideControls();
		
	}

	@Override
	public void handle(Integer event, Object param)
	{
		
	}
	
	
	
	Player player;
	
	@Override
	protected void onAttachedToWindow()
	{
		
		super.onAttachedToWindow();
		
		player = Training.Activate(this, Api.Init.getCacheData().currentTraining());
		
	}
	
	@Override
	protected void onDetachedFromWindow()
	{
		
		super.onDetachedFromWindow();
		
		Training.Deactivate(player);
	}

	@Override
	protected void onSetSize(int width, int height)
	{
		
	}

	
	public void setContent(View ui)
	{
		frameContent.removeAllViews();
		frameContent.addView(ui, LPR.create().get());
	}
	
	public void setContentBottom(View ui)
	{
		viewBottom.removeAllViews();
		viewBottom.addView(ui, LPR.create().get());
	}

	
	public void hideControls()
	{
		imageMute.setVisibility(INVISIBLE);
		imageMute.setClickable(false);
		
		imageHelp.setVisibility(INVISIBLE);
		imageHelp.setClickable(false);
		
		textExit.setVisibility(INVISIBLE);
		textExit.setClickable(false);
		
		textRestart.setVisibility(INVISIBLE);
		textRestart.setClickable(false);
		
		textTimeCurrent.setVisibility(INVISIBLE);
		textTimeCurrent.setClickable(false);
		
		textTimeAll.setVisibility(INVISIBLE);
		textTimeAll.setClickable(false);
	}

	@Override
	public boolean isBlockBack()
	{
		
		return true;
	}
	
}
