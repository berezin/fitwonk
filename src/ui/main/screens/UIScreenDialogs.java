package ui.main.screens;

import com.utils.shorts.LPR;
import android.content.Context;
import ui.common.UITitle;
import ui.main.screens.calendar.UICalendar;
import ui.main.screens.dialogs.UIDialogs;

public class UIScreenDialogs extends UIAbstractScreen
{

	UITitle uiTitle;
	UIDialogs uiDialogs;
	
	public UIScreenDialogs(Context context)
	{
		super(context);
		
		uiTitle = new UITitle(getContext());
		uiTitle.Set("МОЙ ТРЕНЕР");
		addView(uiTitle, LPR.create(UITitle.Height).get());
		
		uiDialogs = new UIDialogs(getContext());
		addView(uiDialogs, LPR.create().marginTop(UITitle.Height).get());
		
		
	}

	
	
	@Override
	public void handle(Integer event, Object param)
	{
		
	}

	@Override
	protected void onSetSize(int width, int height)
	{
		
	}

}
