package ui.main.screens;

import com.utils.shorts.LPR;
import android.content.Context;
import android.os.Debug;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import fitwonk.Api;
import ui.common.UITitle;
import ui.main.screens.calendar.UICalendar;

public class UIScreenCalendar extends UIAbstractScreen
{

	UITitle uiTitle;
	UICalendar uiCalendar;
	
	public UIScreenCalendar(Context context)
	{
		super(context);
		
	
		//Debug.startMethodTracing("UIScreenCalendar");
		
		uiTitle = new UITitle(getContext());
		uiTitle.Set("ТРЕНИРОВКИ");
		addView(uiTitle, LPR.create(UITitle.Height).get());
		
		uiCalendar = new UICalendar(getContext());
		addView(uiCalendar, LPR.create().marginTop(UITitle.Height).get());
		
		//Debug.stopMethodTracing();
		
	}

	
	
	@Override
	public void handle(Integer event, Object param)
	{
		if(Api.Init.hasOKEvent(event)) {
			
			removeView(uiCalendar);
			uiCalendar = new UICalendar(getContext());
			addView(uiCalendar, LPR.create().marginTop(UITitle.Height).get());
			
		}
	}

	@Override
	protected void onSetSize(int width, int height)
	{
		
	}

}
