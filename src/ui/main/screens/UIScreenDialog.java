package ui.main.screens;

import com.utils.shorts.LPR;
import com.utils.VovaActivity;
import com.utils.VovaActivity.IActivityHandler;
import activity.ActivityMain;
import android.content.Context;
import data.DataDialogs.DataDialog;
import ui.common.UITitle;
import ui.main.screens.dialog.UIDialog;

public class UIScreenDialog extends UIAbstractScreen implements IActivityHandler
{

	UITitle uiTitle;
	UIDialog uiDialog;
	
	public UIScreenDialog(Context context)
	{
		super(context);
		
		uiTitle = new UITitle(getContext());
		uiTitle.Set("МОЙ ТРЕНЕР");
		addView(uiTitle, LPR.create(UITitle.Height).get());
		
		uiDialog = new UIDialog(getContext());
		addView(uiDialog, LPR.create().marginTop(UITitle.Height).get());
	}
	
	
	@Override
	protected void onAttachedToWindow()
	{
		
		super.onAttachedToWindow();
		
		VovaActivity.AddHandler(getContext(), this);
	}
	
	@Override
	protected void onDetachedFromWindow()
	{
		
		super.onDetachedFromWindow();
		
		VovaActivity.RemoveHandler(getContext(), this);
	}

	@Override
	public void Set(Object data)
	{
		try
		{
			DataDialog dialog = (DataDialog) data;
			uiDialog.Set(dialog);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void handle(Integer event, Object param)
	{
		
	}

	@Override
	protected void onSetSize(int width, int height)
	{
		
	}

	
	@Override
	public void onDestroy()
	{
		
	}

	@Override
	public void onStart()
	{
		
	}

	@Override
	public void onStop()
	{
		
	}

	@Override
	public void onResume()
	{
		
	}

	@Override
	public void onPause()
	{
		
	}

	@Override
	public boolean isBlockBack()
	{
		
		ActivityMain.ToDialogs(getContext());
		
		return true;
	}

}
