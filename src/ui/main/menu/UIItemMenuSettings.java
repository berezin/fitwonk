package ui.main.menu;

import common.Res;
import fitwonk.Api;
import fitwonk.Training;
import ui.main.UIMain;

import com.utils.shorts.E;

import activity.ActivityMain;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;

public class UIItemMenuSettings extends UIItemMenu
{

	public UIItemMenuSettings(Context context)
	{
		super(context);
		
		Set(Res.IC_ITEM_MENU_SETTINGS, Res.STR_ITEM_MENU_SETTINGS, getDesc());
		
		setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				if(!Training.IN_PROCESS) {
					E.Event(UIMain.REQUEST_MENU);
					ActivityMain.ToSettings(getContext());
				}
				
			}
		});
	}
	
	String getDesc() {
		
		return null;
	}

	@Override
	public void handle(Integer event, Object param)
	{
		
	}

}
