package ui.main.menu;

import com.utils.shorts.Q;
import com.utils.shorts.E;
import ui.main.UIMain;
import ui.main.UIMainContent;
import ui.main.screens.UIScreenCalendar;
import common.Res;
import fitwonk.Api;
import fitwonk.Training;
import activity.ActivityMain;
import android.content.Context;
import android.view.View;

public class UIItemMenuCalendar extends UIItemMenu
{

	public UIItemMenuCalendar(Context context)
	{
		super(context);
		
		Set();
		
		setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				if(!Training.IN_PROCESS) {
					E.Event(UIMain.REQUEST_MENU);
					ActivityMain.ToCalendar(getContext());
				}
				
			}
		});
	}
	
	String getDesc() {
		
		return String.format(Q.getStr(Res.STR_ITEM_MENU_CALENDAR_DESC), Api.Init.getCacheData().currentDay());
	}
	
	void Set() {
		Set(Res.iC_ITEM_MENU_CALENDAR, Res.STR_ITEM_MENU_CALENDAR, getDesc());
	}
	
	@Override
	public void handle(Integer event, Object param)
	{
		if(Api.Init.hasOKEvent(event)) {
			
			Set();
			
		}
	}


}
