package ui.main.menu;

import com.utils.shorts.E;
import ui.main.UIMain;
import common.Res;
import fitwonk.Api;
import fitwonk.Training;
import activity.ActivityMain;
import android.content.Context;
import android.view.View;

public class UIItemMenuMain extends UIItemMenu
{

	public UIItemMenuMain(Context context)
	{
		super(context);
		
		Set(Res.IC_ITEM_MENU_MAIN, Res.STR_ITEM_MENU_MAIN, getDesc());
		
		setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				if(!Training.IN_PROCESS) {
					E.Event(UIMain.REQUEST_MENU);
					ActivityMain.ToMain(getContext());
				}
				
			}
		});
	}
	
	String getDesc() {
		
		return null;
	}
	
	void Set() {
		Set(Res.IC_ITEM_MENU_GOAL, Res.STR_ITEM_MENU_GOAL, getDesc());
	}
	
	@Override
	public void handle(Integer event, Object param)
	{

	}

}
