package ui.main.menu;

import common.Res;
import fitwonk.Api;
import fitwonk.Training;
import com.utils.shorts.E;
import ui.main.UIMain;
import activity.ActivityMain;
import android.content.Context;
import android.view.View;

public class UIItemMenuTrainer extends UIItemMenu
{

	public UIItemMenuTrainer(Context context)
	{
		super(context);
		
		Set();
		
		setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				if(!Training.IN_PROCESS) {
					E.Event(UIMain.REQUEST_MENU);
					ActivityMain.ToDialogs(getContext());
				}
				
			}
		});
	}
	
	String getDesc() {
		
		return Api.Init.getCacheData().getTrainerName();
	}

	void Set() {
		Set(Res.IC_ITEM_MENU_TRAINER, Res.STR_ITEM_MENU_TRAINER, getDesc());
	}
	
	@Override
	public void handle(Integer event, Object param)
	{
		if(Api.Init.hasOKEvent(event)) {
			
			Set();
			
		}
	}
}
