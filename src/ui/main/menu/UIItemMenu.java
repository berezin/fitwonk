package ui.main.menu;

import common.Paints;
import common.Type;
import com.utils.shorts.LPR;
import com.utils.shorts.Q;
import com.utils.EventsUtils.BindedRelativeLayout;
import com.utils.Dimen;
import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public abstract class UIItemMenu extends BindedRelativeLayout
{

	TextView text;
	TextView textDesc;
	ImageView image;
	
	RelativeLayout frameTexts;
	
	static public Integer MARGIN_LEFT = Dimen.d60;
	static public Integer WIDTH_IMAGE = Dimen.d36;
	static public Integer MARGIN_TOP = Dimen.d25;
	static public Integer COLOR_TEXT1 = 0xffffffff;
	static public Integer COLOR_TEXT2 = 0xff999999;
	
	
	
	public UIItemMenu(Context context)
	{
		super(context);
		
		frameTexts = new RelativeLayout(getContext());
		addView(frameTexts, LPR.createWrap().marginLeft(MARGIN_LEFT).centerV().get());
		
		text = new TextView(getContext());
		text.setId(1);
		Paints.SetText(text, Type.Futura , 15, COLOR_TEXT1);
		frameTexts.addView(text);
		
		textDesc = new TextView(getContext());
		Paints.SetText(textDesc, Type.FuturaLight, 14, COLOR_TEXT2);
		frameTexts.addView(textDesc, LPR.createWrap().marginTop(0).below(1).get());
		
		image = new ImageView(getContext());
		addView(image, LPR.create(WIDTH_IMAGE, WIDTH_IMAGE).margins((MARGIN_LEFT - WIDTH_IMAGE)/2).centerV().get());
		
		
	}
	
	public void Set(int icon, int t, String optDesc) {
		
		text.setText(Q.getStr(t).toUpperCase());
		
		if(optDesc == null) {
			textDesc.setVisibility(GONE);
		}
		else {
			textDesc.setText(optDesc);
		}
		
		image.setImageResource(icon);

	}
	

}
