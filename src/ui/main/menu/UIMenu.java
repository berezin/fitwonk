package ui.main.menu;

import common.Paints;
import common.Palette;
import common.Res;
import common.Starter;
import common.Test;
import common.Type;
import data.DataUploadImageResult;
import data.DataUploadUserResult;
import fitwonk.Api;
import fitwonk.Bages;
import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.utils.shorts.LPR;
import com.utils.shorts.Q;
import com.utils.ui.ImageLayout;

import activity.ActivityMain;

import com.utils.shorts.I;
import com.utils.Dimen;

import ui.common.SmartRelativeLayout;
import ui.common.UICircleImage;
import ui.main.screens.myday.InfoConst;

public class UIMenu extends SmartRelativeLayout
{
	
	
	RelativeLayout containerAvatar;
	UICircleImage image;
	TextView textName;
	ImageLayout imageBage;
	View line;
	
	UIItemMenu itemMain;
	UIItemMenu itemCalendar;
	UIItemMenu itemTrainer;
	UIItemMenu itemGoal;
	UIItemMenu itemSettings;
	OnClickListener listener = new OnClickListener()
	{
		
		@Override
		public void onClick(View v)
		{
			if(Test.isLogoutByAvatarClick) {
				Api.Auth.clearCacheData();
				Api.ClearUserData();
				Starter.Splash(getContext());
			}
			else {
				
			}
		}
	};

	@SuppressWarnings("deprecation")
	public UIMenu(Context context)
	{
		super(context);
		
		setBackgroundColor(0xff2d2d2d);
		
		containerAvatar = new RelativeLayout(getContext());
		containerAvatar.setBackgroundDrawable(Res.BACKGROUND_BLUE());
		addView(containerAvatar, LPR.createMW().get());
		
		image = new UICircleImage(getContext());
		image.setBorderColor(0xffffffff);
		image.setBorderWidth(Dimen.d2);
		image.setBorderColor2(Palette.BLUE);
		image.setBorderWidth2(Dimen.d3);
		
		containerAvatar.addView(image);
		I.Load(image, Api.Init.getCacheData().icon, listener, true);
		
		if(!image.hasImage())
			image.setImageResource(Res.IMG_PLACEHOLDER_CIRCLE_WHITE);
		
		textName = new TextView(getContext());
		textName.setGravity(Gravity.CENTER);
		containerAvatar.addView(textName);
		
		imageBage = new ImageLayout(getContext());
		imageBage.is_xfer = false;
		imageBage.setBackgroundColor(0x00000000);
		containerAvatar.addView(imageBage);
		imageBage.Set(Bages.CurrentNormalBitmap(), false);
		
		line = new View(getContext());
		line.setBackgroundColor(InfoConst.PAINT_BLUE_DARK.getColor());
		addView(line);
		
		itemMain = new UIItemMenuMain(getContext());
		addView(itemMain);
		
		itemCalendar = new UIItemMenuCalendar(getContext());
		addView(itemCalendar);
		
		itemTrainer = new UIItemMenuTrainer(getContext());
		addView(itemTrainer);
		
		itemGoal = new UIItemMenuGoal(getContext());
		addView(itemGoal);
		
		itemSettings = new UIItemMenuSettings(getContext());
		addView(itemSettings);
		
		setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				
			}
		});
	}

	@Override
	protected void onSetSize(int w, int h)
	{

		int hContainer = (int) (w * 0.7);
		
		int tH = hContainer / 6;
		int mT = Dimen.d15;
		int wImg = hContainer - tH - mT;
		int wImg2 = (int) (wImg * 0.5f);
		
		containerAvatar.setLayoutParams(LPR.create(w, hContainer).get());
		image.setLayoutParams(LPR.create(wImg, wImg).marginTop(mT).centerH().get());
		textName.setLayoutParams(LPR.create(tH).bottom().get());
		Paints.SetText(textName, Type.Futura, (int) Q.getTextSize(tH / 2), 0xffffffff);
		textName.setText(Api.Init.getCacheData().name.toUpperCase());
		imageBage.setLayoutParams(LPR.create(wImg2, wImg2).marginTop(mT ).marginLeft((int) ((w - wImg)/2 - wImg2*0.4f)).get());
		
		line.setLayoutParams(LPR.create(Dimen.d5).marginTop(hContainer).get());
		
		
		int top = hContainer + Dimen.d15;
		int heightItem = (h - hContainer) / 6;// Dimen.d60;
		
		itemMain.setLayoutParams(LPR.create(heightItem).marginTop(top).get());
		top += heightItem;
		itemCalendar.setLayoutParams(LPR.create(heightItem).marginTop(top).get());
		top += heightItem;
		itemTrainer.setLayoutParams(LPR.create(heightItem).marginTop(top).get());
		top += heightItem;
		itemGoal.setLayoutParams(LPR.create(heightItem).marginTop(top).get());
		top += heightItem;
		itemSettings.setLayoutParams(LPR.create(heightItem).marginTop(top).get());
		top += heightItem;
		
	}
	

	@Override
	public void handle(Integer event, Object param)
	{
		if(Api.Init.hasOKEvent(event)) {
			
			imageBage.Set(Bages.CurrentNormalBitmap(), false);
			textName.setText(Api.Init.getCacheData().name.toUpperCase());
			
		}
		else if(Api.Settings.UploadImage.hasOKEvent(event)) {
			
			DataUploadImageResult data = Api.Settings.UploadImage.getData(param);
			
			I.Load(image, data.icon, listener, true);
			
			if(!image.hasImage())
				image.setImageResource(Res.IMG_PLACEHOLDER_CIRCLE_WHITE);
			
			
		}
		else if(Api.Settings.UploadUser.hasOKEvent(event)) {
			
			DataUploadUserResult data = Api.Settings.UploadUser.getData(param);
			
			textName.setText(data.name.toUpperCase());
			
			
		}
	}

}
