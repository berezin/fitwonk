package ui.main;

import common.Palette;
import common.Res;

import com.utils.shorts.LPR;
import com.utils.shorts.Q;
import com.utils.shorts.A;
import com.utils.shorts.E;
import com.utils.AnimationUtils.IEvent;
import com.utils.Dimen;

import ui.common.SmartRelativeLayout;
import ui.main.screens.UIScreenMyDay;
import activity.ActivityMain;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;

public class UIMainContent extends SmartRelativeLayout
{
	
	
	static public Integer REQUEST_SETUP = E.Id("UIMainContent.REQUEST_SETUP");
	
	ImageView imageTitle;
	ImageView imageMenu;
	View child;
	
	static public Integer HeightTitle = Dimen.d60;
	
	static Integer WidthTitle() {
		
		return Math.round( Q.getW() / 2.1f );
	}
	
	public UIMainContent(Context context)
	{
		super(context);
		
		setBackgroundColor(Palette.GRAY_BACKGROUND);
		
		imageTitle = new ImageView(getContext());
		addView( imageTitle, LPR.create(WidthTitle(), HeightTitle).marginTop( Dimen.d10).centerH().get() );
		imageTitle.setImageResource(Res.TITLE);
		
		imageMenu = new ImageView(getContext());
		int pad = Dimen.d18;
		imageMenu.setPadding(pad, pad, pad, pad);
		addView( imageMenu, LPR.create(HeightTitle, HeightTitle).marginLeft(Dimen.d5).marginTop( Dimen.d10).get() );
		imageMenu.setImageResource(Res.MENU_BUTTON);
		
		imageMenu.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				E.Event(UIMain.REQUEST_MENU);
			}
		});
		
		
		//setup(new DataSetup(new UIScreenMain(getContext())));
	}
	
	@Override
	protected void onAttachedToWindow()
	{
		super.onAttachedToWindow();
		
		ActivityMain.ToMain(getContext());
	}

	
	boolean inTransition = false;
	
	public void setup(final DataSetup setupData) {
		
		if(child != null && child == setupData.view) {
			return;
		}
		
		if(!inTransition) {
			inTransition = true;
			
			
			addView(setupData.view, LPR.create().marginTop(HeightTitle).get());
			A.fadein(setupData.view, 300, new IEvent()
			{
				
				@Override
				public void onEnd()
				{
					if(child!=null)
						removeView(child);
					
					child = setupData.view;
					inTransition = false;
				}
			});
		}
		else {
			
			postDelayed(new Runnable()
			{
				
				@Override
				public void run()
				{
					setup(setupData);
				}
			}, 400);
		}
		
	
		
	}
	
	
	@Override
	public void handle(Integer event, Object param) {
		if(event.equals(REQUEST_SETUP)) {
			
			DataSetup setupData = (DataSetup) param;
			
			setup(setupData);
		}
	}


	@Override
	protected void onSetSize(int width, int height) {
		
		//BAssert.log("UIShellMain " + height);
	}
	
	
	public static class DataSetup {
		
		public View view;

		public DataSetup(View view) {
			
			this.view = view;
		}
		
		@Override
		public String toString()
		{
			return view.toString();
		}
	}
	
	
	
	

}
