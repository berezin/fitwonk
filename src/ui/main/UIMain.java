package ui.main;

import com.utils.shorts.LPR;
import com.utils.shorts.E;
import com.utils.EventsUtils.IBinder;
import com.utils.VovaActivity;
import com.utils.VovaActivity.IActivityHandler;
import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import ui.main.menu.UIMenu;

public class UIMain extends DrawerLayout implements IBinder, IActivityHandler
{

	
	public static Integer REQUEST_MENU = E.Id("UIMain.REQUEST_MENU");
	
	UIMenu uiMenu;
	UIMainContent uiMain;
	
	public UIMain(Context context)
	{
		super(context);
		
		uiMain = new UIMainContent(getContext());
		uiMenu = new UIMenu(getContext());

		
		addView(uiMain, new DrawerLayout.LayoutParams(LPR.MATCH, LPR.MATCH));
		addView(uiMenu, new DrawerLayout.LayoutParams(LPR.MATCH, LPR.MATCH, Gravity.START));
		
		
		
	}
	
	@Override
	protected void onAttachedToWindow()
	{
		
		super.onAttachedToWindow();
		E.Bind(this);
		VovaActivity.AddHandler(getContext(), this);
	}
	
	@Override
	protected void onDetachedFromWindow()
	{
		
		super.onDetachedFromWindow();
		E.Unbind(this);
		VovaActivity.RemoveHandler(getContext(), this);
	}

	@Override
	public void handle(Integer event, Object param)
	{
		
		if(event.equals(REQUEST_MENU)) {
			
			if(isDrawerOpen(uiMenu)) {
				closeDrawers();
			}
			else {
				openDrawer(uiMenu);
			}
			
		}
		
	}

	@Override
	public void onDestroy() {
		
	}

	@Override
	public void onStart()
	{
		
	}

	@Override
	public void onStop()
	{
		
	}

	@Override
	public boolean isBlockBack()
	{
		return false;
	}

	@Override
	public void onPause()
	{
		E.Unbind(this);
	}

	@Override
	public void onResume()
	{
		E.Bind(this);
	}

}
