package ui.splash.trainer;

import com.utils.Dimen;
import com.utils.shorts.I;
import com.utils.shorts.LPR;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;
import common.Paints;
import common.Palette;
import common.Res;
import common.Type;
import data.DataRegisterFormResult.DataTrainer;
import ui.common.UICircleImage;

public class UITrainer extends RelativeLayout
{

	
	UICircleImage image;
	TextView textName;
	
	DataTrainer trainer;
	
	public Integer Height() {
		return Dimen.d90;
	}
	
	public Integer MarginLeft() {
		return Dimen.d15;
	}
	
	public Integer HeightImage() {
		return Dimen.d70;
	}
	
	public UITrainer(Context context)
	{
		super(context);
		
		image = new UICircleImage(getContext());
		image.setBorderColor2(0xffffffff);
		image.setBorderWidth(Dimen.d2);
		image.setBorderColor(Palette.BLUE);
		image.setBorderWidth2(Dimen.d2);
		
		
		addView(image, LPR.create(HeightImage(), HeightImage()).centerV().marginLeft(MarginLeft()).get());
		
		textName = new TextView(getContext());
		Paints.SetText(textName, Type.Futura, 14, 0xff000000);
		addView(textName, LPR.createWrap().centerV().marginLeft(HeightImage()+ MarginLeft()+ MarginLeft()).get());
		
	}
	
	public void Set(DataTrainer trainer) {
		this.trainer = trainer;
		I.Load(image, trainer.icon);
		
		if(!image.hasImage())
			image.setImageResource(Res.IMG_PLACEHOLDER_CIRCLE_GRAY);
		
		textName.setText(trainer.name);

	}
	
	public Integer getID() {
		
		return trainer.id;
	}
	
	public void setSelection(boolean selected) {
		
		setBackgroundColor(selected?0x22000000:0x00000000);
		
	}

}
