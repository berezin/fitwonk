package ui.splash.trainer;

import java.util.ArrayList;

import com.utils.shorts.LPL;
import com.utils.shorts.LPR;
import com.utils.shorts.Q;
import com.utils.shorts.T;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import common.AbstractTitleDialogView;
import common.Palette;
import common.Res;
import common.updater.Request.Responce;
import data.DataRegisterFormResult;
import data.DataRegisterFormResult.DataTrainer;
import data.DataRegisterTrainerResult;
import fitwonk.Api;
import ui.common.UIProgressAndTryAgain;
import ui.splash.form.UIDialogForm;
import ui.splash.pay.UIDialogPayment;

public class UIDialogTrainer extends AbstractTitleDialogView
{

	RelativeLayout frameContent;
	TextView textSend;
	TextView textBack;
	
	ArrayList<UITrainer> uiTrainers;
	Integer currentTrainer = null;
	
	UIProgressAndTryAgain frameProgress;
	
	
	public UIDialogTrainer(Context context)
	{
		super(context);
		setTitle("Тренер");
		
		textSend = addButton("ОТПРАВИТЬ");
		textSend.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				if(currentTrainer!=null) {
					
					setProgress();
					Api.Registration.Trainer.Send(currentTrainer);
				}
			}
		});
		
		textBack = addButton("ОТМЕНА");
		textBack.setTextColor(Palette.GRAY);
		textBack.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				Api.Registration.Form.clearCacheData();
				hide();
				
				UIDialogForm ui = new UIDialogForm(getContext());
				ui.show();
			}
		});
		
	}
	
	@Override
	public void show()
	{
		setInput();
		super.show();
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public void handle(Integer event, Object param)
	{
		if(Api.Registration.Trainer.hasOKEvent(event)) {
			
			hide();
			
			UIDialogPayment ui = new UIDialogPayment(getContext());
			ui.show();
			
		}
		else if(Api.Registration.Trainer.hasErrorEvent(event)) {
			
			Responce<DataRegisterTrainerResult> r = (Responce<DataRegisterTrainerResult>) param;
			
			if(r.data!=null && r.data.error!=null) {
				
				setInput();
				T.show(r.data.error);
			}
			else {
				
				T.show(Res.STR_ERROR_CONNECTION);
				
				frameProgress.setTryAgain(new OnClickListener() {
					
					@Override
					public void onClick(View v)
					{
						frameProgress.setProgress();
						Api.Registration.Trainer.TryLast();
					}
				});
			}
			
		}
	}

	@Override
	public void createDialogBundle(Bundle b)
	{
		
	}

	@Override
	public void setDialogBundle(Bundle b)
	{
		
	}
	
	@Override
	public boolean isCancelable()
	{
		return false;
	}
	
	void setProgress() {
		frameContent.setVisibility(INVISIBLE);
		frameProgress.setVisibility(VISIBLE);
		frameProgress.setProgress();
		textSend.setVisibility(INVISIBLE);
		if(textBack!=null) {
			textBack.setVisibility(INVISIBLE);
		}
	}
	
	void setInput() {
		frameContent.setVisibility(VISIBLE);
		frameProgress.setVisibility(INVISIBLE);
		textSend.setVisibility(VISIBLE);
		if(textBack!=null) {
			textBack.setVisibility(VISIBLE);
		}
	}

	@Override
	public View getContentView()
	{
		
		uiTrainers = new ArrayList<>();
		
		RelativeLayout frame = new RelativeLayout(getContext());
		
		int h = (int) (Q.getH() * 0.6);
		
		frameContent = new RelativeLayout(getContext());
		frame.addView(frameContent, LPR.create(h).get());
		
		DataRegisterFormResult data = Api.Registration.Form.getCacheData();
		
		if(data.trainers!=null) {
			
			ScrollView scroll = new ScrollView(getContext());
			frameContent.addView(scroll, LPR.create().get());
			LinearLayout linear = new LinearLayout(getContext());
			linear.setOrientation(1);
			scroll.addView(linear);
			
			for(DataTrainer t:data.trainers) {
				
				UITrainer ui = new UITrainer(getContext());
				uiTrainers.add(ui);
				
				ui.Set(t);
				linear.addView(ui, LPL.create(LPL.MATCH, ui.Height()).get());
				
				ui.setOnClickListener(new OnClickListener()
				{
					
					@Override
					public void onClick(View v)
					{
						for(UITrainer ui:uiTrainers) {
							if(ui == v) {
								ui.setSelection(true);
								currentTrainer = ui.getID();
							}
							else {
								ui.setSelection(false);
							}
						}
					}
				});

			}
		}
		
		frameProgress = new UIProgressAndTryAgain(getContext());
		frame.addView(frameProgress, LPR.create(h).get());
		
		return frame;
	}

}
