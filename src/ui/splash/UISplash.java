package ui.splash;

import common.Res;
import common.Starter;
import data.DataRegisterFormResult;
import data.DataRegisterPlanResult;
import data.DataRegisterTrainerResult;
import data.DataRegisterMailResult;

import com.utils.EventsUtils.BindedRelativeLayout;
import com.utils.EventsUtils.GODRelativeLayout;
import com.utils.shorts.LPR;
import com.utils.ui.ImageLayout;

import fitwonk.Api;
import ui.splash.auth.UIDialogAuth;
import ui.splash.form.UIDialogForm;
import ui.splash.pay.UIDialogPayment;
import ui.splash.trainer.UIDialogTrainer;
import android.app.Activity;
import android.content.Context;

public class UISplash extends BindedRelativeLayout implements Runnable
{

	boolean isNeedHandle = false;
	
	ImageLayout image;

	public UISplash(Context context)
	{
		super(context);
		
		image = new ImageLayout(getContext());
		image.setDefaultImageView();
		addView(image, LPR.create().get());
		image.Set(Res.IMG_SPLASH, false);

		isNeedHandle = true;

	}
	
	

	void ToMain()
	{
		Starter.Main(getContext());
		((Activity) getContext()).finish();
		
		
	}

	@Override
	public void handle(Integer event, Object param)
	{

		if(isNeedHandle && Api.Init.hasOKEvent(event)) {
			
			isNeedHandle = false;
			ToMain();
		}
		

	}

	boolean isFirst = true;
	
	
	
	
	@Override
	protected void onAttachedToWindow()
	{
		
		super.onAttachedToWindow();

		if(isFirst) {
			
			isFirst = false;
			
			postDelayed(this, 300);
		}
		
	}



	@Override
	public void run()
	{
		if(!Api.hasToken()) {
			
			DataRegisterTrainerResult dataTrainerResult = Api.Registration.Trainer.getCacheData();
			DataRegisterFormResult dataFormResult = Api.Registration.Form.getCacheData();
			DataRegisterMailResult dataEmailResult = Api.Registration.Mail.getCacheData();
			DataRegisterPlanResult dataPaymentResult = Api.Registration.Payment.getCacheData();
			
			
			if(dataEmailResult!=null) {
				
				
				if(dataFormResult!=null) {
					
					if(dataTrainerResult!=null) {
						
						if(dataPaymentResult!=null) {
							UIDialogAuth ui = new UIDialogAuth(getContext());
							ui.show();
						}
						else {
							UIDialogPayment ui = new UIDialogPayment(getContext());
							ui.show();
						}
						
					}
					else {
						UIDialogTrainer ui = new UIDialogTrainer(getContext());
						ui.show();
					}
					
				}
				else {
					UIDialogForm ui = new UIDialogForm(getContext());
					ui.show();
				}

			} else {
				UIDialogAuth ui = new UIDialogAuth(getContext());
				ui.show();
			}
			

		}
		else {
			
			Api.Init.Send();

			postDelayed(new Runnable()
			{
				@Override
				public void run()
				{
					if(isNeedHandle && !Api.Init.IsEmpty()) {
						isNeedHandle = false;
						ToMain();
					}
				}
			}, 2000);

		}
	}
	




}
