package ui.splash.auth;

import com.utils.Dimen;
import com.utils.shorts.LPR;
import com.utils.shorts.T;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import common.AbstractTitleDialogView;
import common.Palette;
import common.Res;
import data.DataInit;
import data.DataToken;
import fitwonk.Api;
import ui.common.UIEntry;
import ui.common.UIProgressAndTryAgain;
import ui.splash.form.UIDialogMail;

public class UIDialogAuth extends AbstractTitleDialogView
{
	RelativeLayout frameContent;
	EditText editLogin;
	EditText editPass;
	TextView textOk;
	TextView textReg;
	
	UIProgressAndTryAgain frameProgress;
	
	static Integer HeightEdit = Dimen.d50;
	static Integer WidthEdit = Dimen.d200;
	static Integer Margin = Dimen.d15;
	
	public UIDialogAuth(Context context)
	{
		super(context);
		setTitle("Вход");
		
		textOk = addButton("ВОЙТИ");
		textOk.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setProgress();
				Api.Auth.Send(editLogin.getText().toString(), editPass.getText().toString());
			}
		});
		
		textReg = addButton("РЕГИСТРАЦИЯ");
		textReg.setTextColor(Palette.GRAY);
		textReg.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
			
				UIDialogMail ui = new UIDialogMail(getContext());
				ui.show();
			
				hide();

			}
		});
		
	}

	@Override
	public void handle(Integer event, Object param)
	{
		if(Api.Auth.hasOKEvent(event)) {
			
			Api.Init.Send();
		}
		else if(Api.Auth.hasErrorEvent(event)) {
			
			DataToken data = Api.Auth.getData(param);
			
			if(data!=null && data.error!=null) {
				
				T.show(data.error);
				setInput();
			}
			else {
				
				T.show(Res.STR_ERROR_CONNECTION);
				frameProgress.setTryAgain(new OnClickListener()
				{
					
					@Override
					public void onClick(View v)
					{
						frameProgress.setProgress();
						Api.Auth.TryLast();
					}
				});
			}
		}
		else if(Api.Init.hasErrorEvent(event)) {
			DataInit data = Api.Init.getData(param);
			
			if(data!=null && data.error!=null) {
				
				T.show(data.error);
				setInput();
			}
			else {
				
				T.show(Res.STR_ERROR_CONNECTION);
				
				frameProgress.setTryAgain(new OnClickListener()
				{
					
					@Override
					public void onClick(View v)
					{
						frameProgress.setProgress();
						Api.Init.TryLast();
					}
				});
				
			}
		}
		else if(Api.Init.hasOKEvent(event)) {
			Api.ClearUserData();
		}
	}

	@Override
	public void createDialogBundle(Bundle b)
	{
		
	}

	@Override
	public void setDialogBundle(Bundle b)
	{
		
	}

	@Override
	public void show()
	{
		setInput();
		super.show();
	}
	
	@Override
	public View getContentView()
	{
	
		RelativeLayout frame = new RelativeLayout(getContext());
		
		frameContent = new RelativeLayout(getContext());
		editLogin = new UIEntry(getContext());
		editLogin.setHint("Введите ваш email");
		editLogin.setText("trainee@b-co.org");
		frameContent.addView(editLogin, LPR.create(HeightEdit).margins(Margin, Margin, Margin).get());
		
		editPass = new UIEntry(getContext());
		editPass.setHint("Введите пароль");
		editPass.setText("0u9i8o");
		frameContent.addView(editPass, LPR.create(HeightEdit).margins(Margin, Margin + HeightEdit + Margin, Margin).get());
		
		frame.addView(frameContent, LPR.createMW().get());
		
		frameProgress = new UIProgressAndTryAgain(getContext());
		frame.addView(frameProgress, LPR.create(Margin + HeightEdit + Margin + HeightEdit).get());
		

		return frame;
	}
	
	void setProgress() {
		
		textOk.setVisibility(INVISIBLE);
		textReg.setVisibility(INVISIBLE);
		
		frameProgress.setVisibility(VISIBLE);
		frameProgress.setProgress();
		frameContent.setVisibility(INVISIBLE);
	}
	
	void setInput() {
		textOk.setVisibility(VISIBLE);
		textReg.setVisibility(VISIBLE);
		
		frameProgress.setVisibility(INVISIBLE);
		frameContent.setVisibility(VISIBLE);
	}

	@Override
	public boolean isCancelable()
	{
		return false;
	}
	
}
