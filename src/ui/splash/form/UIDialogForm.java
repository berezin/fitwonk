package ui.splash.form;

import java.util.ArrayList;

import com.utils.Dimen;
import com.utils.parser.ParserUtils.JSONParser.Post;
import com.utils.shorts.A;
import com.utils.shorts.LPR;
import com.utils.shorts.Q;
import com.utils.shorts.T;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import common.AbstractTitleDialogView;
import common.Palette;
import common.Res;
import common.updater.Request.Responce;
import data.DataOption;
import data.DataRegisterDict;
import data.DataRegisterFormResult;
import data.DataRegisterTrainerResult;
import drawable.DrawableProgress;
import fitwonk.Api;
import ui.common.UIEntry;
import ui.common.UIProgressAndTryAgain;
import ui.splash.trainer.UIDialogTrainer;

public class UIDialogForm extends AbstractTitleDialogView
{

	ScrollView scroll;
	LinearLayout linear;
	UIProgressAndTryAgain frameProgress;
	TextView textSend;
	TextView textBack;
	
	//username	String	Системное имя пользователя
	UIFormEntry editUsername;
	//password	String	Пароль
	UIFormEntry editPassword;
	//name	String	Публичное имя пользователя
	UIFormEntry editName;
	//birthday	Date	Дата рождения
	UIFormDate uiBirthday;
	//genderId	Integer	Пол – идентификатор [id] значения из справочника «Пол»
	UIFormSpinner spinnerGender;
	//cityId	Integer	Город – идентификатор [id] значения из справочника «Город»
	UIFormSpinner spinnerCity;
	//lifestyleId	Integer	Образ жизни – идентификатор [id] значения из справочника «Образ жизни»
	UIFormSpinner spinnerLifestyle;
	//levelId	Integer	Уровень подготовки – идентификатор [id] значения из справочника «Уровень подготовки»
	UIFormSpinner spinnerLevel;
	//height	Float	Рост пользователя, см
	UIFormEntry editHeight;
	//weight	Float	Вес пользователя, кг
	UIFormEntry editWeight;
	//placeId	Integer	Место проведения тренировок – идентификатор [id] значения из справочника «Место тренировок»
	UIFormSpinner spinnerPlace;
	//purposeId	Integer	Цель – идентификатор [id] значения из справочника «Цель»
	UIFormSpinner spinnerPurpose;
	//note	Text	Примечания (например, ограничения по медицинским показаниям или какие-то персональные особенности)
	UIFormEntry spinnerNote;
	//trainerGenderId	Integer	Препочитаемый пол тренера – идентификатор [id] значения из справочника «Пол»
	UIFormSpinner spinnerTrainerGender;

	
	ArrayList<IFormProperty> properties = new ArrayList<>();
	
	public static interface IFormProperty {
		boolean isValid();
		void putValue(Post p);
	}
	
	private void subtitle(String title) {
		new UIFormSubTitle(linear, title);
	}
	
	@Override
	public boolean isCancelable()
	{
		return false;
	}
	
	private UIFormEntry entry(String property, String defaultValue, boolean canBeEmpty) {
		UIFormEntry ui = new UIFormEntry(linear, property, defaultValue, canBeEmpty);
		properties.add(ui);
		return ui;
	}
	
	private UIFormEntry entryNum(String property) {
		UIFormEntry ui = new UIFormEntry(linear, property, "0", true);
		ui.setNumberEntry();
		
		properties.add(ui);
		return ui;
	}
	
	private UIFormSpinner spinner(String property, ArrayList<DataOption> options) {
		UIFormSpinner ui = new UIFormSpinner(linear, property, options);
		properties.add(ui);
		return ui;
	}
	
	private UIFormDate date(String property) {
		UIFormDate ui = new UIFormDate(linear, property);
		properties.add(ui);
		return ui;
	}
	
	private void putPost(Post post) {
		
		for(IFormProperty p: properties) {
			p.putValue(post);
		}
	}
	
	private boolean isValid() {
		int index = 0;
		for(IFormProperty p: properties) {
			if(!p.isValid()) {
				
				View v = (View) p;
				
				scroll.smoothScrollTo(0, v.getTop() - Dimen.d40);
				
				return false;
			}
			
			index+=2;
		}
		
		return true;
	}
	
	private void createForm() {
		
		DataRegisterDict dict = Api.Registration.Dict.getCacheData();

		//name	String	Публичное имя пользователя
		subtitle("ФАМИЛИЯ, ИМЯ:");
		editName = entry("name", dict.name, false);
		//username	String	Системное имя пользователя
		subtitle("USERNAME:");
		editUsername = entry("username", dict.name, false);
		//password	String	Пароль
		subtitle("ПАРОЛЬ:");
		editPassword = entry("password", "", false);
		
		//birthday	Date	Дата рождения
		subtitle("ДАТА РОЖДЕНИЯ:");
		uiBirthday = date("birthday");
		//genderId	Integer	Пол – идентификатор [id] значения из справочника «Пол»
		subtitle("ПОЛ:");
		spinnerGender = spinner("genderId", dict.gender);
		//cityId	Integer	Город – идентификатор [id] значения из справочника «Город»
		subtitle("ГОРОД:");
		spinnerCity = spinner("cityId", dict.city);
		//lifestyleId	Integer	Образ жизни – идентификатор [id] значения из справочника «Образ жизни»
		subtitle("ОБРАЗ ЖИЗНИ:");
		spinnerLifestyle = spinner("lifestyleId", dict.lifestyle);
		//levelId	Integer	Уровень подготовки – идентификатор [id] значения из справочника «Уровень подготовки»
		subtitle("ОБРАЗ ЖИЗНИ:");
		spinnerLevel = spinner("levelId", dict.level);
		//height	Float	Рост пользователя, см
		subtitle("РОСТ:");
		editHeight = entryNum("height");
		//weight	Float	Вес пользователя, кг
		subtitle("ВЕС:");
		editWeight = entryNum("weight");
		//placeId	Integer	Место проведения тренировок – идентификатор [id] значения из справочника «Место тренировок»
		subtitle("МЕСТО ТРЕНИРОВКИ:");
		spinnerPlace = spinner("placeId", dict.place);
		//purposeId	Integer	Цель – идентификатор [id] значения из справочника «Цель»
		subtitle("ЦЕЛЬ:");
		spinnerPurpose = spinner("purposeId", dict.purpose);
		//note	Text	Примечания (например, ограничения по медицинским показаниям или какие-то персональные особенности)
		subtitle("МЕДИЦИНСКИЕ ОГРАНИЧЕНИЯ:");
		spinnerNote = entry("note", "", true);
		//trainerGenderId	Integer	Препочитаемый пол тренера – идентификатор [id] значения из справочника «Пол»
		subtitle("ПОЛ ТРЕНЕРА:");
		spinnerTrainerGender = spinner("purposeId", dict.gender);
		

		textSend.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				if(isValid()) {
					
					Post p = new Post();
					putPost(p);
					
					setProgress();
					Api.Registration.Form.Send(p);
				}
				
			}
		});
		
	}
	
	public UIDialogForm(Context context)
	{
		super(context);
		setTitle("Личные данные");
		textSend = addButton("ОТПРАВИТЬ");
		
		textBack = addButton("НАЗАД");
		textBack.setTextColor(Palette.GRAY);
		textBack.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				Api.Registration.Mail.clearCacheData();
				hide();
				
				UIDialogMail ui = new UIDialogMail(getContext());
				ui.show();
			}
		});
	}
	
	@Override
	public void show()
	{
		setProgress();
		Api.Registration.Dict.Send();
		super.show();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void handle(Integer event, Object param)
	{
		if(Api.Registration.Dict.hasOKEvent(event)) {
			setInput();
			createForm();
		}
		else if(Api.Registration.Form.hasOKEvent(event)) {
			hide();
			UIDialogTrainer ui = new UIDialogTrainer(getContext());
			ui.show();
		}
		else if(Api.Registration.Form.hasErrorEvent(event)) {
			
			
			Responce<DataRegisterTrainerResult> r = (Responce<DataRegisterTrainerResult>) param;
			
			if(r.data!=null && r.data.error!=null) {
				setInput();
				T.show(r.data.error);
			}
			else {
				
				T.show(Res.STR_ERROR_CONNECTION);
				
				frameProgress.setTryAgain(new OnClickListener()
				{
					
					@Override
					public void onClick(View v)
					{
						frameProgress.setProgress();
						Api.Registration.Form.TryLast();
					}
				});
			}
		}
		else if(Api.Registration.Dict.hasErrorEvent(event)) {
			
			Responce<DataRegisterDict> r = (Responce<DataRegisterDict>) param;
			
			if(r.data!=null && r.data.error!=null) {
				T.show(r.data.error);
			}
			else {
				T.show(Res.STR_ERROR_CONNECTION);
			}
			
			frameProgress.setTryAgain(new OnClickListener()
			{
				
				@Override
				public void onClick(View v)
				{
					frameProgress.setProgress();
					Api.Registration.Dict.TryLast();
				}
			});
			
		}
	}

	@Override
	public void createDialogBundle(Bundle b)
	{
		
	}

	@Override
	public void setDialogBundle(Bundle b)
	{
		
	}

	@Override
	public View getContentView()
	{
		RelativeLayout frame = new RelativeLayout(getContext());
		
		int h = (int) (Q.getH() * 0.6);
		
		scroll = new ScrollView(getContext());
		frame.addView(scroll, LPR.create(h).get());
		
		linear = new LinearLayout(getContext());
		linear.setOrientation(1);
		scroll.addView(linear);
		
		frameProgress = new UIProgressAndTryAgain(getContext());
		frame.addView(frameProgress, LPR.create(h).get());
		
		return frame;
	}
	
	void setInput() {
		
		frameProgress.setVisibility(INVISIBLE);
		scroll.setVisibility(VISIBLE);
		textSend.setVisibility(VISIBLE);
		textBack.setVisibility(VISIBLE);
	}
	void setProgress() {
		
		frameProgress.setVisibility(VISIBLE);
		frameProgress.setProgress();
		
		scroll.setVisibility(INVISIBLE);
		textSend.setVisibility(INVISIBLE);
		textBack.setVisibility(INVISIBLE);
	}

}
