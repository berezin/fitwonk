package ui.splash.form;

import java.util.ArrayList;

import com.utils.Dimen;
import com.utils.parser.ParserUtils.JSONParser.Post;
import com.utils.shorts.LPL;
import com.utils.shorts.LPR;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import common.Paints;
import common.Type;
import ui.splash.form.UIDialogForm.IFormProperty;

public class UIFormDate extends RelativeLayout implements IFormProperty
{

	UIFormSpinnerDate spinnerDay;
	UIFormSpinnerDate spinnerMounth;
	UIFormSpinnerDate spinnerYear;
	String property;
	AdapterDays adapterDays;
	
	public UIFormDate(Context context)
	{
		super(context);
		init(null, "");
	}
	
	public static interface ICallback {
		void setDate(String date);
	}
	
	ICallback callback;
	
	public void setCallback(ICallback callback) {
		this.callback = callback;
	}
	
	void init(LinearLayout parent, String property) {
		this.property = property;

		LinearLayout linear = new LinearLayout(getContext());
		spinnerDay = new UIFormSpinnerDate(getContext());
		adapterDays = new AdapterDays(spinnerDay, getDays());
		spinnerDay.setAdapter(adapterDays);
		linear.addView(spinnerDay, LPL.create(LPL.WRAP).weight(1.1f).get());
		
		spinnerMounth = new UIFormSpinnerDate(getContext());
		spinnerMounth.setAdapter(new AdapterDays(spinnerMounth, getMounths()));
		linear.addView(spinnerMounth, LPL.create(LPL.WRAP).marginLeft(Dimen.d10).weight(0.7f).get());
		
		spinnerYear = new UIFormSpinnerDate(getContext());
		spinnerYear.setAdapter(new AdapterDays(spinnerYear, getYears()));
		linear.addView(spinnerYear, LPL.create(LPL.WRAP).marginLeft(Dimen.d10).weight(1f).get());
		
		if(parent!=null)
		{
			parent.addView(linear, LPL.create(LPL.WRAP).margins(Dimen.d15,0,Dimen.d15).get());
		}
		else {
			addView(linear, LPR.create(LPL.WRAP).margins(Dimen.d15,0,Dimen.d10).get());
		}
		
		
		spinnerDay.setOnItemSelectedListener(listener);
		spinnerMounth.setOnItemSelectedListener(listener);
		spinnerYear.setOnItemSelectedListener(listener);
	}
	
	public UIFormDate(LinearLayout parent, String property)
	{
		super(parent.getContext());
		
		init(parent, property);
		
	}
	
	OnItemSelectedListener listener = new OnItemSelectedListener()

	{
		int mountsFeb = 1;
		
		ArrayList<Integer> mounts30 = new ArrayList<>();
		ArrayList<Integer> leapYear = new ArrayList<>();
		
		{
			mounts30.add(3);
			mounts30.add(5);
			mounts30.add(8);
			mounts30.add(10);
			
			for(int i = 2; i < 1000; i+=4) {
				leapYear.add(i);
			}
			
		}
		
		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id)
		{
			
			position = spinnerMounth.getSelectedItemPosition();
			
			if(mounts30.contains(position)) {
				changeDayIfNeed(29);
			}
			else if(mountsFeb == position) {
				
				int max = 27;
				
				int yearPosition = spinnerYear.getSelectedItemPosition();
				
				if(leapYear.contains(yearPosition)) {
					max = 28;
				}
				
				changeDayIfNeed(max);
				
			}
			else {
				changeDayIfNeed(30);
			}
			
			if(callback!=null) {
				callback.setDate(getValue());
			}
			
			
		}
		
		private void changeDayIfNeed(int max) {
			
			int dayPosition = spinnerDay.getSelectedItemPosition();
			
			if(dayPosition > max) {
				spinnerDay.setSelection(max);
				
			}
			
			adapterDays.setDaysLimit(max + 1);
			
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent)
		{
			
		}
	};

	@Override
	public boolean isValid()
	{
		return true;
	}
	
	public void setValue(String date)
	{
		try
		{
			String[] strs = date.split("-");
			
			int selectionYear = Integer.parseInt(strs[0]) - 1970;
			int selectionMounth = Integer.parseInt(strs[1]) - 1;
			int selectionDay = Integer.parseInt(strs[2]) - 1;
			
			spinnerYear.setSelection(selectionYear);
			spinnerMounth.setSelection(selectionMounth);
			spinnerDay.setSelection(selectionDay);
		}
		catch (Exception e)
		{
			
			e.printStackTrace();
		}
		
	}

	public String getValue()
	{
		String year = (String) spinnerYear.getSelectedItem();
		String mounth = ""+(spinnerMounth.getSelectedItemPosition() + 1);
		if(mounth.length() == 1) {
			mounth = "0"+mounth;
		}
		
		String day = ""+(spinnerDay.getSelectedItemPosition() + 1);
		if(day.length() == 1) {
			day = "0"+day;
		}
		
		return year+"-"+mounth+"-"+day;
	}
	
	@Override
	public void putValue(Post p)
	{
		p.add(property, getValue());
	}
	
	ArrayList<String> getDays() {
		ArrayList<String> texts = new ArrayList<String>();
		
		for(int i = 1; i < 32; i++) {
			texts.add(String.valueOf(i));
		}
		
		return texts;
	}
	
	ArrayList<String> getMounths() {
		ArrayList<String> texts = new ArrayList<String>();
		
		texts.add("Январь");
		texts.add("Февраль");
		texts.add("Март");
		texts.add("Апрель");
		texts.add("Май");
		texts.add("Июнь");
		texts.add("Июль");
		texts.add("Август");
		texts.add("Сентябрь");
		texts.add("Октябрь");
		texts.add("Ноябрь");
		texts.add("Декабрь");
		
		return texts;
	}
	
	ArrayList<String> getYears() {
		ArrayList<String> texts = new ArrayList<String>();
		
		for(int i = 1970; i < 2000; i++) {
			texts.add(String.valueOf(i));
		}
		return texts;
	}
	
	public static class AdapterDays extends BaseAdapter {


		ArrayList<String> texts;
		UIFormSpinnerDate spinner;
		
		public AdapterDays(UIFormSpinnerDate spinner, ArrayList<String> texts) {
			this.spinner = spinner;
			this.texts = texts;
		}
		
		@Override
		public int getCount()
		{
			return Math.min(texts.size(), limit);
		}
		
		int limit = 100000;
		public void setDaysLimit(int limit)
		{
			this.limit = limit;
			notifyDataSetChanged();
		}

		@Override
		public Object getItem(int position)
		{
			return texts.get(position);
		}

		@Override
		public long getItemId(int position)
		{
			return texts.get(position).hashCode();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			
			TextView txt = null;
			
			if(convertView!=null) {
				txt = (TextView) convertView;
			}
			else {
				txt = new TextView(parent.getContext());
				Paints.SetText(txt, Type.Futura, 14, 0xff000000);
				txt.setPadding(Dimen.d2, Dimen.d12, Dimen.d2, Dimen.d12);
			}
			
			txt.setGravity(Gravity.CENTER);
			txt.setBackgroundColor(position==spinner.getSelectedItemPosition()?0xffdddddd:0xffffffff);
			txt.setText(texts.get(position));
			return txt;
		}
		
	}


}
