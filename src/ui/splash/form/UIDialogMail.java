package ui.splash.form;

import com.utils.Dimen;
import com.utils.shorts.LPR;
import com.utils.shorts.T;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import common.AbstractTitleDialogView;
import common.Paints;
import common.Palette;
import common.Res;
import common.Type;
import common.updater.Request.Responce;
import data.DataRegisterMailResult;
import drawable.DrawableProgress;
import fitwonk.Api;
import ui.common.UIEntry;
import ui.common.UIProgressAndTryAgain;
import ui.splash.auth.UIDialogAuth;

public class UIDialogMail extends AbstractTitleDialogView
{

	RelativeLayout frameContent;
	
	RelativeLayout frameEmail;
	EditText editEmail;
	
	
	
	UIProgressAndTryAgain frameProgress;

	private boolean isInput;
	
	TextView textSend;
	TextView textBack;
	
	public UIDialogMail(Context context)
	{
		super(context);
		setTitle("Регистрация");
		
		textSend = addButton("ОТПРАВИТЬ");
		textSend.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				if(isInput) {
					
					setSending();
					Api.Registration.Mail.Send(editEmail.getText().toString());
				}
				
			}
		});
		
		textBack = addButton("НАЗАД");
		textBack.setTextColor(Palette.GRAY);
		textBack.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				hide();
				
				UIDialogAuth ui = new UIDialogAuth(getContext());
				ui.show();
			}
		});
		
		setInput();

	}
	

	@Override
	public void createDialogBundle(Bundle b)
	{

	}

	@Override
	public void setDialogBundle(Bundle b)
	{
	
	}
	
	
	@Override
	public void show()
	{
		
		super.show();
	}
	
	@Override
	public void hide()
	{
		
		//Api.Registration.User.getCacheStorage().Clear();
		
		super.hide();
	}
	
	void setSending() {
		isInput = false;
		frameProgress.setVisibility(VISIBLE);
		frameProgress.setProgress();
		textBack.setVisibility(INVISIBLE);
		textSend.setVisibility(INVISIBLE);
		frameEmail.setVisibility(INVISIBLE);
	}
	
	void setInput() {
		isInput = true;
		frameProgress.setVisibility(INVISIBLE);
		textSend.setVisibility(VISIBLE);
		textBack.setVisibility(VISIBLE);
		frameEmail.setVisibility(VISIBLE);
	}
	
	@Override
	public boolean isCancelable()
	{
		return false;
	}

	@Override
	public View getContentView()
	{
		
		int h = Dimen.d80;
		
		frameContent = new RelativeLayout(getContext());
		//
		frameEmail = new RelativeLayout(getContext());
		editEmail = new UIEntry(getContext());
		Paints.SetText(editEmail, Type.Futura, 14, 0xff000000);
		editEmail.setHint("Введите ваш email");
		frameEmail.addView(editEmail, LPR.createMW().margins(Dimen.d15, 0, Dimen.d15).center().get());
		
		frameContent.addView(frameEmail, LPR.create(h).get());
		//
		frameProgress = new UIProgressAndTryAgain(getContext());
		frameContent.addView(frameProgress, LPR.create(h).get());
		

		return frameContent;
	}


	@SuppressWarnings("unchecked")
	@Override
	public void handle(Integer event, Object param)
	{
		if(Api.Registration.Mail.hasOKEvent(event)) {
			
			UIDialogForm ui = new UIDialogForm(getContext());
			ui.show();
		}
		else if(Api.Registration.Mail.hasErrorEvent(event)) {
			Responce<DataRegisterMailResult> result = (Responce<DataRegisterMailResult>) param;
			
			if(result.data!=null) {
				setInput();
				T.show(result.data.error);
			}
			else {
				
				T.show(Res.STR_ERROR_CONNECTION);
				
				frameProgress.setTryAgain(new OnClickListener()
				{
					
					@Override
					public void onClick(View v)
					{
						frameProgress.setProgress();
						Api.Registration.Mail.TryLast();
					}
				});
			}
			
			
		}
	}

}
