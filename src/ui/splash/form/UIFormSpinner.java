package ui.splash.form;

import java.util.ArrayList;

import com.utils.Dimen;
import com.utils.parser.ParserUtils.JSONParser.Post;
import com.utils.shorts.LPL;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import common.Paints;
import common.Type;
import data.DataOption;
import ui.splash.form.UIDialogForm.IFormProperty;

public class UIFormSpinner extends Spinner implements IFormProperty
{
	
	String property;
	ArrayList<DataOption> options;
	
	public UIFormSpinner(LinearLayout parent, String property, ArrayList<DataOption> options)
	{
		super(parent.getContext());
		this.property = property;
		this.options = options;
		
		setAdapter(new Adapter(this, options));
		setBackgroundColor(0x11000000);
		setPadding(0, 0, 0, 0);
		
		parent.addView(this, LPL.create(LPL.WRAP).margins(Dimen.d15,0,Dimen.d15).get());
		
	}
	
	@Override
	public boolean isValid()
	{
		return true;
	}


	@Override
	public void putValue(Post p)
	{
		p.add(property, options.get(getSelectedItemPosition()).id);
	}
	
	
	public static class Adapter extends BaseAdapter {

		UIFormSpinner spinner;
		ArrayList<DataOption> options;
		
		public Adapter(UIFormSpinner spinner, ArrayList<DataOption> options) {
			this.spinner = spinner;
			this.options = options;
		}
		
		@Override
		public int getCount()
		{
			return options.size();
		}

		@Override
		public Object getItem(int position)
		{
			return options.get(position);
		}

		@Override
		public long getItemId(int position)
		{
			return options.get(position).id;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			
			TextView txt = new TextView(parent.getContext());
			
			Paints.SetText(txt, Type.Futura, 14, 0xff000000);
			
			txt.setBackgroundColor(position==spinner.getSelectedItemPosition()?0xffdddddd:0xffffffff);
			txt.setPadding(Dimen.d20, Dimen.d12, Dimen.d20, Dimen.d12);
			txt.setText(options.get(position).name);
			return txt;
		}
		
	}






}
