package ui.splash.form;

import com.utils.Dimen;
import com.utils.shorts.LPL;

import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;
import common.Paints;
import common.Type;

public class UIFormSubTitle extends TextView
{

	public UIFormSubTitle(LinearLayout parent, String title)
	{
		super(parent.getContext());
		
		setPadding(0, 0, Dimen.d5, 0);
		setGravity(Gravity.BOTTOM);
		Paints.SetText(this, Type.Futura, 12, 0xff666666);
		setText(title);
		
		parent.addView(this, LPL.create(Dimen.d40).margins(Dimen.d15,0,Dimen.d15).get());
	}

}
