package ui.splash.form;

import com.utils.Dimen;
import com.utils.parser.ParserUtils.JSONParser.Post;
import com.utils.shorts.LPL;

import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import ui.common.UIEntry;
import ui.splash.form.UIDialogForm.IFormProperty;

public class UIFormEntry extends UIEntry implements IFormProperty
{
	boolean canBeEmpty;
	String property;
	
	public UIFormEntry(LinearLayout parent, String property, String defaultValue, boolean canBeEmpty)
	{
		super(parent.getContext());
		this.canBeEmpty = canBeEmpty;
		this.property = property;
		setText(defaultValue!=null?defaultValue:"");
		
		parent.addView(this, LPL.create(LPL.WRAP).margins(Dimen.d15,0,Dimen.d15).get());
	}

	@Override
	public boolean isValid()
	{
		return canBeEmpty || getText().length()>0;
	}

	@Override
	public void putValue(Post p)
	{
		String str = getText().toString();
		
		
		if(isNumber) {
			p.add(property, str.isEmpty()?0.0:Double.parseDouble(str));
		}
		else {
			p.add(property, str);
		}
		
		
	}

	boolean isNumber = false;
	public void setNumberEntry()
	{
		isNumber = true;
		setInputType(EditorInfo.TYPE_CLASS_NUMBER);
	}
	
	

}
