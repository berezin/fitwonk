package ui.splash.form;

import java.util.ArrayList;

import com.utils.Dimen;
import com.utils.parser.ParserUtils.JSONParser.Post;
import com.utils.shorts.LPL;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import common.Paints;
import common.Type;
import data.DataOption;
import ui.splash.form.UIDialogForm.IFormProperty;

public class UIFormSpinnerDate extends Spinner
{

	public UIFormSpinnerDate(Context context)
	{
		super(context);

		setBackgroundColor(0x11000000);
		setPadding(0, 0, 0, 0);

	}
	
	
	public static class Adapter extends BaseAdapter {

		UIFormSpinnerDate spinner;
		ArrayList<DataOption> options;
		
		public Adapter(UIFormSpinnerDate spinner, ArrayList<DataOption> options) {
			this.spinner = spinner;
			this.options = options;
		}
		
		@Override
		public int getCount()
		{
			return options.size();
		}

		@Override
		public Object getItem(int position)
		{
			return options.get(position);
		}

		@Override
		public long getItemId(int position)
		{
			return options.get(position).id;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			
			TextView txt = new TextView(parent.getContext());
			
			Paints.SetText(txt, Type.Futura, 14, 0xff000000);
			txt.setGravity(Gravity.CENTER);
			txt.setBackgroundColor(position==spinner.getSelectedItemPosition()?0xffdddddd:0xffffffff);
			txt.setPadding(Dimen.d10, Dimen.d10, Dimen.d10, Dimen.d10);
			txt.setText(options.get(position).name);
			return txt;
		}
		
	}






}
