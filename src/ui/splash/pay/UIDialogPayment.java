package ui.splash.pay;

import com.utils.Dimen;
import com.utils.shorts.LPR;
import com.utils.shorts.T;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import common.AbstractTitleDialogView;
import common.Palette;
import common.Res;
import common.updater.Request;
import data.DataInit;
import data.DataRegisterDict;
import data.DataRegisterFormResult;
import data.DataRegisterPlanResult;
import data.DataRegisterTrainerResult;
import data.DataToken;
import fitwonk.Api;
import ui.common.UIEntry;
import ui.common.UIProgressAndTryAgain;
import ui.splash.auth.UIDialogAuth;
import ui.splash.trainer.UIDialogTrainer;

public class UIDialogPayment extends AbstractTitleDialogView
{

	RelativeLayout frameContent;
	TextView textSend;
	TextView textBack;
	UIEntry entryCode;
	
	UIProgressAndTryAgain frameProgress;
	DataRegisterTrainerResult dataPlans;
	
	Request<DataRegisterTrainerResult> Plans;
	Request<DataRegisterPlanResult> Payment;
	Request<DataToken> Auth;
	
	
	public UIDialogPayment(Context context)
	{
		super(context);
		
		Plans = Api.Registration.Plans;
		Payment = Api.Registration.Payment;
		Auth = Api.Auth;
		
		setTitle("Оплата");
		
		textSend = addButton("СОХРАНИТЬ");
		textSend.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				setProgress();
				Payment.Send(null, entryCode.getText().toString());
			}
		});
		
		textBack = addButton("НАЗАД");
		textBack.setTextColor(Palette.GRAY);
		textBack.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				Api.Registration.Trainer.clearCacheData();
				hide();
				
				UIDialogTrainer ui = new UIDialogTrainer(getContext());
				ui.show();
			}
		});

	}
	
	@Override
	public void show()
	{
		setProgress();
		Plans.Send();
		super.show();
	}
	
	@Override
	public boolean isCancelable()
	{
		return false;
	}
	
	void setProgress() {
		frameContent.setVisibility(INVISIBLE);
		frameProgress.setVisibility(VISIBLE);
		frameProgress.setProgress();
		textSend.setVisibility(INVISIBLE);
		if(textBack!=null) {
			textBack.setVisibility(INVISIBLE);
		}
	}
	
	void setInput() {
		frameContent.setVisibility(VISIBLE);
		frameProgress.setVisibility(INVISIBLE);
		textSend.setVisibility(VISIBLE);
		if(textBack!=null) {
			textBack.setVisibility(VISIBLE);
		}
	}

	@Override
	public void handle(Integer event, Object param)
	{
		if(Plans.hasOKEvent(event)) {
			
			dataPlans = Plans.getData(param);
			fillForm();
			setInput();
			
		}
		else if(Plans.hasErrorEvent(event)) {
			
			DataRegisterTrainerResult data = Plans.getData(param);
			
			if(data!=null && data.error!=null) {
				T.show(data.error);
			}
			else {
				T.show(Res.STR_ERROR_CONNECTION);
			}
			
			frameProgress.setTryAgain(new OnClickListener()
			{
				
				@Override
				public void onClick(View v)
				{
					frameProgress.setProgress();
					Plans.TryLast();
				}
			});
			
		}
		else if(Payment.hasOKEvent(event)) {

			DataRegisterFormResult form = Api.Registration.Form.getCacheData();
			DataRegisterDict dict = Api.Registration.Dict.getCacheData();
			Auth.Send(dict.email, form.password);
		}
		else if(Payment.hasErrorEvent(event)) {
			
			DataRegisterPlanResult data = Payment.getData(param);
			
			if(data!=null && data.error!=null) {
				setInput();
				T.show(data.error);
			}
			else {
				T.show(Res.STR_ERROR_CONNECTION);
				
				frameProgress.setTryAgain(new OnClickListener()
				{
					
					@Override
					public void onClick(View v)
					{
						frameProgress.setProgress();
						Payment.TryLast();
					}
				});
			}

		}
		else if(Api.Auth.hasOKEvent(event)) {
			
			Api.Init.Send();
		}
		else if(Api.Auth.hasErrorEvent(event)) {
			
			DataToken data = Api.Auth.getData(param);
			
			if(data!=null && data.error!=null) {
				T.show(data.error);
				hide();
				UIDialogAuth ui = new UIDialogAuth(getContext());
				ui.show();
			}
			else {
				
				T.show(Res.STR_ERROR_CONNECTION);
				
				frameProgress.setTryAgain(new OnClickListener()
				{
					
					@Override
					public void onClick(View v)
					{
						frameProgress.setProgress();
						Api.Auth.TryLast();
					}
				});
			}
			
			
		}
		else if(Api.Init.hasErrorEvent(event)) {
			DataInit data = Api.Init.getData(param);
			
			if(data!=null && data.error!=null) {
				
				T.show(data.error);
				hide();
				UIDialogAuth ui = new UIDialogAuth(getContext());
				ui.show();
			}
			else {
				
				T.show(Res.STR_ERROR_CONNECTION);
				
				frameProgress.setTryAgain(new OnClickListener()
				{
					
					@Override
					public void onClick(View v)
					{
						frameProgress.setProgress();
						Api.Init.TryLast();
					}
				});
			}
		}
		else if(Api.Init.hasOKEvent(event)) {
			Api.ClearUserData();
		}
		
		
	}

	private void fillForm()
	{
		
	}

	@Override
	public void createDialogBundle(Bundle b)
	{
		
	}

	@Override
	public void setDialogBundle(Bundle b)
	{
		
	}

	
	
	@Override
	public View getContentView()
	{
		
		RelativeLayout frame = new RelativeLayout(getContext());
		

		int h = Dimen.d80;
		
		frameContent = new RelativeLayout(getContext());
		frame.addView(frameContent, LPR.create(h).get());
		
		entryCode = new UIEntry(getContext());
		entryCode.setText("eB8D2qXjb774R2EVRa9");
		entryCode.setHint("Введите сертификат");
		frameContent.addView(entryCode, LPR.createMW().margins(Dimen.d15,0,Dimen.d15).center().get());
		
		
		frameProgress = new UIProgressAndTryAgain(getContext());
		frame.addView(frameProgress, LPR.create(h).get());
		
		return frame;
	}

}
