package drawable;

import com.utils.Dimen;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;

public class DrawableProgress extends Drawable
{
	
	int progress = 0;
	Paint paint;
	int strokeWidth = Dimen.d8;
	Interpolator interpol;
	
	
	
	public static void Set(View v) {
		new DrawableProgress(v, 0xffffffff);
	}
	
	public static void Set(View v, Integer color) {
		new DrawableProgress(v, color);
	}
	
	
	//float left, top, right, bottom;
	
	public DrawableProgress(View v, Integer color)
	{
		interpol = new AccelerateDecelerateInterpolator();
		
		paint = new Paint();
		paint.setColor(color);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(strokeWidth);
		paint.setAntiAlias(true);
        /*
		left = Q.getRealSize(10);
		top = left;
		right = Q.getRealSize(70) - left;
		bottom = right;
		
		rect = new RectF(left, top, right, bottom);
         */
		v.setBackgroundDrawable(this);
		
	}

	
	RectF rect = null;
	@Override
	public void draw(Canvas canvas)
	{
		
		if(rect == null)
		{
			rect = new RectF(getBounds());
			int pad = strokeWidth / 2 + 1;
			
			rect.top += pad;
			rect.bottom -= pad;
			rect.left += pad;
			rect.right -= pad;
			
		}
		
		
		
		
		progress+=4;
		
		if(progress>=360) {
			progress = 0;
		}
		
		float f = 0;
		
		if(progress<=180)
		{
			f = ((float)progress )/ 180;
		}
		else
		{
			f = (360f - progress)/180;
		}
		
		
		
		f = interpol.getInterpolation(f);
		
		canvas.drawArc(rect, progress, (int)(360 * f), false, paint);
		
		invalidateSelf();
	}

	@Override
	public void setAlpha(int alpha)
	{
		
	}

	@Override
	public void setColorFilter(ColorFilter cf)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public int getOpacity()
	{
		// TODO Auto-generated method stub
		return 0;
	}

}
