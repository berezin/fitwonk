package app;

import com.danikula.videocache.HttpProxyCacheServer;
import com.fitwonk.R;
import com.utils.ThreadTransanction;
import com.utils.ThreadTransanction.ThreadRunnable;
import com.utils.ThreadTransanction.ThreadStatus;
import com.utils.image.Const;
import com.utils.shorts.I;

import android.app.Application;
import android.graphics.BitmapFactory;
import common.Palette;
import common.painter.RippleAddon.RippleController;
import fitwonk.Bages;

public class App extends Application
{

	private HttpProxyCacheServer proxy;

	public static HttpProxyCacheServer getProxy()
	{
		App app = (App) com.utils.App.get();
		return app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;
	}

	private HttpProxyCacheServer newProxy()
	{
		return new HttpProxyCacheServer.Builder(this)
				.maxCacheSize(100 * 1024 * 1024) // 100 Mb for cache
				.build();
	}

	@Override
	public void onCreate()
	{

		super.onCreate();
		com.utils.App.set(this);

		I.initMalevichStd(this);

		Const.COLOR_PLACEHOLDER = 0xffdddddd;
		Const.PLACEHOLDER = BitmapFactory.decodeResource(getResources(),
				R.drawable.placeholder);

		// preload
		ThreadTransanction.execute("preload", new ThreadRunnable<Object>()
		{

			@Override
			public void result(ThreadStatus aStatus, Object aResult)
			{

			}

			@Override
			public Object run()
			{
				Bages.Get(0);

				return null;
			}
		});

		RippleController.DEFAULT_COLOR = Palette.BLUE_RIPPLE;

		
	}


}
