package data;

import java.io.Serializable;

import com.utils.parser.ParserUtils.Data;

public class DataMedia implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5414092151934615896L;
	
	@Data
	public Integer id;
	
	@Data
	public String name;
	
	@Data
	public Integer order;
	
	@Data
	public String description;
	
	@Data
	public String URL;
	
	@Data
	public String type;
	
	public boolean isImage() {
		return type!=null && type.equals("image");
	}
	
	public boolean isVideo() {
		return type!=null && type.equals("embedded");
	}
	
	public String getVideoURL() {
		return "http://d2lgycgn2xkfkn.cloudfront.net/102_mahi-nogoy-lega-na-boku-720.mp4";
	}

}
