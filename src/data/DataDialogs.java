package data;

import java.io.Serializable;
import java.util.ArrayList;

import com.utils.parser.ParserUtils.Data;

public class DataDialogs implements Serializable
{


	private static final long serialVersionUID = -9179632935826071923L;
	
	
	@Data
	public ArrayList<DataDialog> dialog;
	
	
	public static class DataDialog implements Serializable {


		private static final long serialVersionUID = -2563512044346183014L;
		
		@Data
		public Integer id;
		
		@Data
		public ArrayList<DataComment> Comments;
		
	}
	
	public static class DataComment implements Serializable {


		private static final long serialVersionUID = -2563512044346183015L;
		
		@Data
		public Integer id;
		
		@Data
		public Integer userId;
		
		@Data
		public String text;
		
		@Data
		public Long createdAt;
	}
	

}
