package data;

import java.io.Serializable;
import java.util.ArrayList;

import com.utils.parser.ParserUtils.Data;

public class DataRegisterTrainerResult implements Serializable
{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -775920335812444985L;
	//error	String	Описание ошибки. В случае отсутствия ошибки пустая строка “”
	@Data
	public String error;
	//plans	Plan[]	Список активных тарифных планов / абонементов
	@Data
	public ArrayList<DataPlan> plans;
	//payWays	PayWay[]	??? Список доступных способов оплаты
	@Data
	public ArrayList<DataPayWay> payWays;
	
	
	public static class DataPlan implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -812340186829089185L;
		
		//id	Integer	Идентификатор тарифного плана
		@Data
		public Integer id;
		//name	String	Название тарифного плана
		@Data
		public String name;
		//price	Float	Стоимость 
		@Data
		public Double price;
		//period	Integer	Количество дней подписки, входящих в план/абонемент
		@Data
		public String period;
		//description	Text	Описание [text/html: span, div, p, br, hr, h1, h2, h3, h4, b, i, strong, a, img]
		@Data
		public String description;
		
	}
	
	public static class DataPayWay implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -812340186829089125L;
		
		//type	String	Строковый идентификатор способа оплаты
		@Data
		public String type;
		//name	String	Название способа оплаты
		@Data
		public String name;
		//minAmount	Integer	Минимальная принимаемая сумма к оплате. NULL если отсутствует
		@Data
		public Integer minAmount;
		//maxAmount	Integer	Максимальная принимаемая сумма к оплате. NULL если отсутствует
		@Data
		public Integer maxAmount;
		//commissionRate	Float	Комиссия в % от суммы. NULL если отсутствует
		@Data
		public Double commissionRate;
		//commissionMin	Float	Минимальный размер комиссии. NULL если отсутствует
		@Data
		public Double commissionMin;
		//commissionMax	Float	Максимальный размер комиссии. NULL если отсутствует
		@Data
		public Double commissionMax;

		
	}

}
