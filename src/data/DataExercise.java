package data;

import java.io.Serializable;
import java.util.ArrayList;

import common.Paints;
import common.Type;
import common.UtilText;

import android.text.Html;
import android.text.Layout.Alignment;
import android.text.StaticLayout;

import com.utils.parser.ParserUtils.Data;

public class DataExercise implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1478420598694056745L;
	
	@Data
	public Integer id;
	
	@Data
	public String name;
	
	@Data
	public String icon;
	
	@Data
	public String instruction;
	
	@Data
	public String description;
	
	@Data
	public ArrayList<DataMedia> media;

	
	transient private StaticLayout _createDescriptionLayout;
	public StaticLayout createDescriptionLayout(Integer w)
	{
		if(_createDescriptionLayout == null)
		{
			_createDescriptionLayout = new StaticLayout(
					UtilText.trim(Html.fromHtml(description)),
					Paints.GetPaint(Type.FuturaLight, 14, 0xff000000),
					w,
					Alignment.ALIGN_NORMAL, 
					1f, 0f, false);
		}
		
		return _createDescriptionLayout;
	}
	
	
	
	public String getVideoURL()
	{
		
		for(DataMedia m: media) {
			if(m.isVideo()) {
				return m.getVideoURL();
			}
		}
		
		return null;
	}

}
