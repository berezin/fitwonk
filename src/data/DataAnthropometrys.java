package data;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;

import com.utils.parser.ParserUtils.Data;

import common.DateFormats;

public class DataAnthropometrys implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7575973667287606427L;
	
	@Data
	public ArrayList<DataAnthropometry> anthropometry;
	
	transient DataAnthropometry _weight;
	public DataAnthropometry getWeight() {
		
		if(_weight == null) {
			
			for(DataAnthropometry a : anthropometry) {
				if(a.id.equals(2)) {
					_weight = a;
					break;
				}
			}
		}
		
		return _weight;
	}
	
	transient ArrayList<DataAnthropometry> _sizes;
	public ArrayList<DataAnthropometry> getSizes() {
		
		if(_sizes == null) {
			
			_sizes = new ArrayList<>();
			
			for(DataAnthropometry a : anthropometry) {
				if(!a.id.equals(2)
						&& !a.id.equals(1)) {
					
					_sizes.add(a);
				}
			}
		}
		
		return _sizes;
	}
	
	
	
	public static class DataDateValue implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 789141830489092567L;
		
		//date	Date	Дата
		@Data
		public String date;

		public Long timestamp() {
			try
			{
				return DateFormats.OnlyDate2.parse(date).getTime();
			} catch (ParseException e)
			{
				e.printStackTrace();
			}
			
			return 0L;
		}
		
		public String text() {
			return date;
		}
		
		//value	Float	Значение
		@Data
		public Double value;
		
		
		static DecimalFormat df = new DecimalFormat("#.#");
		public String getValueText() {
			try
			{
				return df.format(value);
			} catch (Exception e)
			{
				e.printStackTrace();
			}
			return "";
		}
		
		
	}
	
	public static class DataAnthropometry implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -2738095541526775148L;
		
		//id	Integer	Идентификатор типа антропометрии
		@Data
		public Integer id;
		
		//name	String	Название типа антропометрии
		@Data
		public String name;
		
		//description	Text	Описание типа антропометрии, способа проведения замеров
		@Data
		public String description;
		
		//gender	Option	Ограничения по полу для типа антропометрии. NULL если не зависит от пола
		@Data
		public DataOption gender;
		
		//order	Integer	Порядок отображения
		@Data
		public Integer order;
		
		//unit	String	Единицы измерения данного типа антропометрии
		@Data
		public String unit;
		
		//permanent	Boolean	Тип антропометрии практически постоянен (например, рост)
		@Data
		public Boolean permanent;
		
		//image	String	URL изображения демонстрирующего правильный способ проведения замеров
		@Data
		public String image;
		
		//icon	String	URL иконки типа антропометрии
		@Data
		public String icon;
		
		//target	Float	Целевое значение пользователя для данного типа антропометрии. NULL если цель не задана пользователем
		@Data
		public Double target;
		
		//logs	DateValue[]	Массив замеров для данного типа антропометрии, где date – дата проведения замера, а value – результат замера в единицах измерения данного типа
		@Data
		public ArrayList<DataDateValue> logs;
		
		//measuredAt	Integer	Время (timestamp) последнего измерения данного типа
		@Data
		public Long measuredAt;
		
		public DataDateValue getCurrentDayLog() {
			
			if(measuredAt!=null && logs!=null && logs.size() > 0) {
				
				
				Long day = 3600L * 24L;
				Long time = System.currentTimeMillis() / 1000L;
						
				if(time - measuredAt < day) {
					
					return logs.get(logs.size() - 1);
				}
				
			}
			
			return null;
		}
		
		
		static DecimalFormat df = new DecimalFormat("#.#");
		public String getTargetText() {
			try
			{
				if(target!=null)
					return df.format(target);
				
			} catch (Exception e)
			{
				e.printStackTrace();
			}
			
			return "";
		}
		
	}
	
	public static class DataAnthropometryResult implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -2738095541526775149L;
		
	
		//target	Float	Целевое значение пользователя для данного типа антропометрии. NULL если цель не задана пользователем
		@Data
		public Double target;
		
		//logs	DateValue[]	Массив замеров для данного типа антропометрии, где date – дата проведения замера, а value – результат замера в единицах измерения данного типа
		@Data
		public ArrayList<DataDateValue> logs;
		
	}
	

}
