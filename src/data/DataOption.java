package data;

import java.io.Serializable;

import com.utils.parser.ParserUtils.Data;

public class DataOption implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5414092151904619896L;
	
	@Data
	public Integer id;
	
	@Data
	public String name;
	
	@Override
	public String toString()
	{
		
		return "["+id+"-"+name+"]";
	}
	
}
