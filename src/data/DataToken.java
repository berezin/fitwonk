package data;

import java.io.Serializable;

import com.utils.parser.ParserUtils.Data;

public class DataToken implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 190817541985892324L;

	@Data
	public String error;
	
	@Data(name="access-token")
	public String accessToken;
	
}
