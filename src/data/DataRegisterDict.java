package data;

import java.io.Serializable;
import java.util.ArrayList;

import com.utils.parser.ParserUtils.Data;

public class DataRegisterDict implements Serializable
{


	private static final long serialVersionUID = 798078504101850126L;
	
	
	//error	String	Описание ошибки. В случае отсутствия ошибки пустая строка “”
	@Data
	public String error;
	
	//id	Integer	Идентификатор пользователя
	@Data
	public Integer id;
	
	//username	String	Системное имя пользователя
	@Data
	public String username;
	
	//name	String	Публичное имя пользователя
	@Data
	public String name;
	
	//email	String	E-mail пользователя
	@Data
	public String email;
	
	//birthday	Date	Дата рождения
	@Data
	public String birthday;
	
	//gender	Option[]	Справочник «Пол»
	@Data
	public ArrayList<DataOption> gender;
	
	//city	Option[]	Справочник «Город»
	@Data
	public ArrayList<DataOption> city;
	
	//lifestyle	Option[]	Справочник «Образ жизни»
	@Data
	public ArrayList<DataOption> lifestyle;
	
	//level	Option[]	Справочник «Уровень подготовки»
	@Data
	public ArrayList<DataOption> level;
	
	//place	Option[]	Справочник «Место тренировок»
	@Data
	public ArrayList<DataOption> place;
	
	//purpose	Option[]	Справочник «Цель»
	@Data
	public ArrayList<DataOption> purpose;
	

}
