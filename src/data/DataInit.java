package data;

import java.io.Serializable;
import java.util.ArrayList;
import data.DataTraining.DataExerciseSet;

import com.utils.parser.ParserUtils.Data;

import common.Test;

public class DataInit implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5977209852894405377L;

	@Data
	public String error;

	@Data
	public Integer id;

	@Data
	public String email;

	@Data
	public String name;

	@Data
	public String birthday;

	@Data
	public String icon;

	@Data
	public Integer training_status;

	@Data
	public Integer active;

	@Data
	public Long createdAt;

	@Data
	public Long loginAt;

	@Data
	public Integer trainerId;

	@Data // Цель (id – идентификатор, name - название) по итогам теста
			// («Похудеть»)
	public DataOption purpose;

	@Data // Цель в постановке пользователя («Похудеть на 13 кг.»)
	public String goal;

	@Data // Ближайшее событие пользователя
	public DataEvent event;

	@Data // Статус пользователя (название, пиктограмма и пр.)
	public DataStatus status;

	@Data // Массив-программа (календарь) тренировок
	public ArrayList<DataTraining> trainings;

	// @Data //Идентификато текущего тренировочного дня в массиве-программе
	// тренировок (Training[?].id)
	// public Integer activeTrainingId;

	@Data // Совокупное количество завершенных тренировок
	public Integer trainingFinished;

	@Data
	public Integer trainingLevelUp;

	@Data
	public Integer calorieIndex;

	@Data
	public String anthropometryAt;

	@Data // Массив информации обо всех упражнениях присутствующих в программе
			// тренировок
	public ArrayList<DataExercise> exercises;

	@Data // Массив информации обо всех пользователях присутствующих в данных
			// (тренер, авторы комментариев и т.д.)
	public ArrayList<DataUser> users;

	// private transient Integer _currentDay;
	public Integer currentDay()
	{
		if(Test.isCompleteAllTrainings)
		{
			return trainings.size() + 1;
		}

		return trainingFinished + 1;
		/*
		 * if(_currentDay == null) {
		 * 
		 * _currentDay = 1;
		 * 
		 * for(DataTraining t: trainings) { if(t.id.equals(activeTrainingId)) {
		 * break; }
		 * 
		 * _currentDay++; }
		 * 
		 * 
		 * }
		 * 
		 * return _currentDay;
		 */
	}

	public boolean isComplete()
	{
		return currentDay().equals(trainings.size() + 1);
	}

	public String getTrainerName()
	{
		for (DataUser u : users)
		{
			if(u.id.equals(this.trainerId))
			{
				return u.name;
			}
		}

		return "";
	}

	public DataTraining currentTraining()
	{

		if(Test.isCompleteAllTrainings)
		{
			trainingFinished = trainings.size();
		}

		DataTraining t = trainingFinished < trainings.size()
				? trainings.get(trainingFinished) : null;
		return t;
	}

	public DataExercise getExercise(Integer id)
	{
		for (DataExercise ex : exercises)
		{
			if(ex.id.equals(id))
			{
				return ex;
			}
		}

		return null;
	}

	public String getLastAnthropometry()
	{
		return anthropometryAt;// DateFormats.OnlyDate.format(new
								// Date(anthropometryAt * 1000L));
	}

	public ArrayList<DataExercise> getExercises(DataTraining training)
	{

		DataTraining t = training;

		ArrayList<DataExercise> list = new ArrayList<>();

		if(t != null && t.sets != null)
		{

			for (DataExerciseSet exSet : t.sets)
			{

				boolean hasId = false;

				for (DataExercise ex : list)
				{
					if(ex.id.equals(exSet.exerciseId))
					{
						hasId = true;
						break;
					}
				}

				if(!hasId)
				{
					list.add(getExercise(exSet.exerciseId));
				}

			}
		}

		return list;
	}

	public ArrayList<DataExercise> currentExercises()
	{
		return getExercises(currentTraining());
	}

}
