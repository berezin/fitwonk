package data;

import java.io.Serializable;
import java.util.ArrayList;

import com.utils.parser.ParserUtils.Data;

public class DataRegisterFormResult implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4868929349427961123L;
	
	//добавочные параметры
	
	@Data
	public String password;
	//
	//error	String	Описание ошибки. В случае отсутствия ошибки пустая строка “”
	@Data
	public String error;
	
	//purpose	Option	Цель – значение справочника «Цель»
	@Data
	public DataOption purpose;
	//weightNorm	Float	Идеальный (целевой) вес
	@Data
	public Double weightNorm;
	//weightLossSpeed	Float	Рекомендуемая норма сброса веса в неделю
	@Data
	public Double weightLossSpeed;
	//weightLossTime	Integer	Округленное время достижения (недель?) идеального веса при сбросе по рекомендуемой норме. Теоретически можно считать и на месте – в приложении.
	@Data
	public Double weightLossTime;
	//trainingTime	Integer	Рекомендуемое время физической активности для достижения цели
	@Data
	public Integer trainingTime;
	//hydrationNorm	Float	Норма потребления жидкости (литров) в день для достижения цели
	@Data
	public Double hydrationNorm;
	//foodEnergyNorm	Integer	Норма потребления пищи (калорий) в день для достижения цели
	@Data
	public Integer foodEnergyNorm;
	//trainers	Trainer[]	Список доступных тренеров для выбора
	@Data
	public ArrayList<DataTrainer> trainers;
	
	
	public static class DataTrainer implements Serializable {
		
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 223893171904482997L;
		
		//id	Integer	Идентификатор пользователя
		@Data
		public Integer id;
		//name	String	Отображаемое имя пользователя
		@Data
		public String name;
		//icon	String	URL картинки-аватарки пользователя
		@Data
		public String icon;
		//birthday	Date	Дата рождения
		@Data
		public String birthday;
		//gender	Option	Пол тренера в виде значения справочника «Пол»
		@Data
		public DataOption gender;
		//regalia	Text	Регалии и звания тренера в свободной форме
		@Data
		public String regalia;
		//specialization	Text	Информация о специализации тренера в свободной форме
		@Data
		public String specialization;
		//rating	Float	Рейтинг тренера в системе. Может быть NULL для новых тренеров.
		@Data
		public Double rating;

	}
	

}
