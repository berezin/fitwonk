package data;

import java.io.Serializable;

import com.utils.parser.ParserUtils.Data;

public class DataEvent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5415092151304619896L;
	
	@Data
	public Integer id;
	
	@Data
	public String name;
	
	@Data
	public String startedAt;
	
}
