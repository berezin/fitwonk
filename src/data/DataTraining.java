package data;

import java.io.Serializable;
import java.util.ArrayList;

import com.utils.parser.ParserUtils.Data;

public class DataTraining implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4049773941848578287L;
	
	
	public static class DataExerciseSet implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -3396044828680971148L;
		
		@Data
		public Integer id;
		
		@Data
		public Integer exerciseId;
		
		@Data
		public Integer order;
		
		@Data
		public DataExerciseMetric plan;
		
		@Data
		public DataExerciseMetric fact;
		
		@Data
		public Long startedAt;
		
		@Data
		public Long finishedAt;

		public DataExercise findExercise(DataInit data)
		{
			
			for(DataExercise d: data.exercises)
			{
				if(d.id.equals(exerciseId)) {
					return d;
				}
			}
			
			return null;
		}

		
	}
	
	public static class DataExerciseMetric implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 224433523917188338L;
		
		@Data
		public Integer repeats;
		
		@Data
		public Integer duration;
		
		@Data
		public Integer resttime;

	}
	
	@Data
	public Integer id;
	
	@Data
	public Integer day;
	
	@Data
	public String name;	
	
	@Data
	public ArrayList<DataExerciseSet> sets;
			
	@Data
	public Integer rate;

	transient public Integer pulse;
	
	@Data
	public Long startedAt;
		
	@Data
	public Long finishedAt;
	
	@Data
	public String comment;
	
	
	
	
		

}
