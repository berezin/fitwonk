package data;

import java.io.Serializable;
import java.util.ArrayList;

import data.DataDialogs.DataComment;
import com.utils.parser.ParserUtils.Data;

public class DataComments implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1190272298290695411L;
	
	@Data
	public ArrayList<DataComment> Comments;
	
	@Data
	public boolean isActive;
	
}
