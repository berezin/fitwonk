package data;

import java.io.Serializable;
import java.util.ArrayList;

import com.utils.parser.ParserUtils.Data;

public class DataRegisterPlanResult implements Serializable
{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -775920335812444300L;
	//error	String	Описание ошибки. В случае отсутствия ошибки пустая строка “”
	@Data
	public String error;

	@Data
	public Integer expiredAt;
	
}
