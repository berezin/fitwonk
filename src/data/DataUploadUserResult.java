package data;

import java.io.Serializable;

import com.utils.parser.ParserUtils.Data;

public class DataUploadUserResult implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2586271891487232417L;

	@Data
	public String error;
	
	@Data
	public String icon;
	
	@Data
	public String name;
	
	@Data
	public String birthdate;
	
	@Data
	public DataOption purpose;
	
	@Data
	public String email;
	
	@Data
	public Integer id;
	
}
