package data;

import java.io.Serializable;

import com.utils.parser.ParserUtils.Data;

public class DataUploadImageResult implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2586271891487232417L;

	@Data
	public String error;
	
	@Data
	public String icon;
	
}
