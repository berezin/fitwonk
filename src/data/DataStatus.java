package data;

import com.utils.parser.ParserUtils.Data;

public class DataStatus extends DataMedia {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5234022451933615896L;
	
	@Data
	public String icon;
	
	@Data
	public String finished;
	
	@Data
	public Integer from;
	
	@Data
	public Integer to;
	
	
	transient Integer index;
	public int getBageIndex() {
		
		if(index == null)
		{
			try
			{
				index = Integer.parseInt(""+icon.charAt(0)) - 1;
			} catch (Exception e)
			{
				index = 0;
			}
		}

		return index;
	}
	

}
