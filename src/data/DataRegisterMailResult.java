package data;

import java.io.Serializable;

import com.utils.parser.ParserUtils.Data;

public class DataRegisterMailResult implements Serializable
{

	private static final long serialVersionUID = 4768567099381738274L;


	public String email;
	
	@Data
	public String error;
	
	@Data(name="access-token")
	public String accesstoken;
	
	@Override
	public String toString()
	{
		return ""+error+" "+accesstoken+" "+email;
	}
	
}
