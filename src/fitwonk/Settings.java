package fitwonk;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;

import com.azumio.android.common.Log;
import com.utils.App;
import com.utils.parser.ParserUtils.JSONParser;
import com.utils.parser.ParserUtils.JSONParser.Post;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import common.updater.Request;
import data.DataRegisterDict;
import data.DataUploadImageResult;
import data.DataUploadUserResult;

@SuppressWarnings("deprecation")
public class Settings
{

	final public static String TAG = "Settings";
	// http://fitwonk.api.b-co.org/upload.php
	
	
	public Request<DataRegisterDict> Dictionary = new Request<DataRegisterDict>(DataRegisterDict.class, "Settings.Dict")
	{
		
		@Override
		protected DataRegisterDict sendSync(Object... args)
		{
			return standartGet(Api.Url.Dictionary());
		}
		
		protected Integer getCacheType() {
			return STORAGE_TYPE_SINGLE;
		};
		
		protected boolean hasRequestStory() {
			return true;
		};

	};

	public Request<DataUploadImageResult> UploadImage = new Request<DataUploadImageResult>(
			DataUploadImageResult.class, "Settings.UploadImage")
	{

		@SuppressWarnings("resource")
		@Override
		protected DataUploadImageResult sendSync(Object... args)
		{

			String filePath = args[0].toString();

			String url = Api.Url.UserSetting();

			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);

			try
			{
				MultipartEntity entity = new MultipartEntity();

				entity.addPart("icon", new FileBody(new File(filePath)));

				httppost.setEntity(entity);
				HttpResponse response = httpclient.execute(httppost);
				
				DataUploadImageResult result = new DataUploadImageResult();
				
				if(JSONParser.Parse(result, response.getEntity().getContent())) {
					
					
					return result;
				}

			} catch (Exception e)
			{
				e.printStackTrace();
			}

			return null;
		}
		
		protected boolean isSuccess(Request.Responce<DataUploadImageResult> result) {
			if(result.data.icon!=null) {
				return true;
			}
			
			return false;
		};
	};
	
	
	public Request<DataUploadUserResult> UploadUser = new Request<DataUploadUserResult>(
			DataUploadUserResult.class, "Settings.UploadUser")
	{

		@Override
		protected DataUploadUserResult sendSync(Object... args)
		{

			Post p = new Post();
			p.add("name", args[0]);
			p.add("birthday", args[1]);
			p.add("purposeId", args[2]);
			
			String url = Api.Url.UserSetting();

			return standartPut(url, p);
		}
		
		protected boolean isSuccess(Request.Responce<DataUploadUserResult> result) {
			return result.data.error == null || result.data.error.length()==0;
		};
	};

	int PICK_FROM_FILE = 5235;

	public void takePictureFromGallery(Context activity)
	{
		if(activity instanceof Activity)
			((Activity) activity).startActivityForResult(
					Intent.createChooser(new Intent(Intent.ACTION_GET_CONTENT)
							.setType("image/*"), "Выберите изображение"),
					PICK_FROM_FILE);
	}
	
	@SuppressWarnings("unused")
	private Bitmap getBitmapFromUri(Uri uri) throws IOException {
	    ParcelFileDescriptor parcelFileDescriptor =
	            App.get().getContentResolver().openFileDescriptor(uri, "r");
	    FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
	    Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
	    parcelFileDescriptor.close();
	    return image;
	}

	public void handleGalleryResult(int requestCode, Intent data)
	{
		if(requestCode == PICK_FROM_FILE)
		{
			Uri selectedImage = data.getData();
			Log.w(TAG, selectedImage.toString());
			String path = getPath(selectedImage);

			UploadImage.Send(path);
		}
	}

	private String getPath(Uri uri)
	{
		if(uri == null)
		{
			return null;
		}
			
		
		try
		{
			return new File(new URI(uri.toString())).getPath();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
		String[] projection = { MediaStore.Images.Media.DATA };

		Cursor cursor;
		if(Build.VERSION.SDK_INT > 19)
		{
			// Will return "image:x*"
			String wholeID = DocumentsContract.getDocumentId(uri);
			// Split at colon, use second item in the array
			String id = wholeID.split(":")[1];
			// where id is equal to
			String sel = MediaStore.Images.Media._ID + "=?";

			cursor = App.get().getContentResolver().query(
					MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection,
					sel, new String[] { id }, null);
		} else
		{
			cursor = App.get().getContentResolver().query(uri, projection, null,
					null, null);
		}
		String path = null;
		try
		{
			int column_index = cursor
					.getColumnIndex(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			path = cursor.getString(column_index).toString();
			cursor.close();
		} catch (NullPointerException e)
		{

		}
		return path;
	}

	
	public void ClearCacheData()
	{
		
	}

}
