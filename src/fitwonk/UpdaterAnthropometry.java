package fitwonk;

import com.utils.parser.ParserUtils.JSONParser;
import com.utils.parser.ParserUtils.JSONParser.Post;
import common.updater.Request;
import common.updater.UpdaterUrl;
import data.DataAnthropometrys;
import data.DataAnthropometrys.DataAnthropometry;
import data.DataAnthropometrys.DataAnthropometryResult;
import fitwonk.Api.Url;


public class UpdaterAnthropometry extends UpdaterUrl<DataAnthropometrys>
{

	
	
	public Request<DataAnthropometryResult> Saving = new Request<DataAnthropometryResult>(DataAnthropometryResult.class,"SaveAnthropometry")


	{

		protected DataAnthropometryResult sendSync(Object... args)
		{
			
			int id = (int) args[0];
			double value = (double) args[1];

			Post p = Post.create("value", value);
			
			String url = Url.Anthropometry(id);
			
			DataAnthropometryResult result = new DataAnthropometryResult();
			
			if(JSONParser.ParsePost(result, url, p.get())) {
				
				return result;
			}
			
			return null;

		}
		
		@Override
		protected void onSuccess(Request.Responce<DataAnthropometryResult> r) {
			int id = (int) r.args[0];
			
			update(id, r.data);
		}

	};
	
	public Request<DataAnthropometryResult> SavingTarget = new Request<DataAnthropometryResult>(DataAnthropometryResult.class,"SaveAnthropometryTarget")


	{

		protected DataAnthropometryResult sendSync(Object... args)
		{
			
			int id = (int) args[0];
			double value = (double) args[1];

			Post p = Post.create("target", value);
			
			String url = Url.Anthropometry(id);
			
			DataAnthropometryResult result = new DataAnthropometryResult();
			
			if(JSONParser.ParsePut(result, url, p.get())) {
				
				return result;
			}
			
			return null;

		}
		
		@Override
		protected void onSuccess(Request.Responce<DataAnthropometryResult> r) {
			int id = (int) r.args[0];
			
			update(id, r.data);
		}

	};
	

	public void update(int id, DataAnthropometryResult r) {
		

		DataAnthropometry d = search(id);
		
		if(d!=null) {
			

			if(r.logs!=null) {
				
				d.measuredAt = System.currentTimeMillis() / 1000L;
				d.logs = r.logs;
			}
			
			if(r.target!=null) {
				d.target = r.target;
			}
			
			save();
		}
		
	}
	
	

	public DataAnthropometry search(int id) {
		
		if(getData() != null && getData().anthropometry!=null)
		for(DataAnthropometry d: getData().anthropometry) {
			if(d.id.equals(id)) {
				return d;
			}
		}
	
		return null;
	}

	public UpdaterAnthropometry()
	{
		super(DataAnthropometrys.class);

	}

	@Override
	public String getUrl()
	{
		return Url.Anthropometry();
	}



	
	
	


}
