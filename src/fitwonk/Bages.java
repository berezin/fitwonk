package fitwonk;

import java.util.ArrayList;
import java.util.HashMap;
import com.utils.shorts.I;
import android.graphics.Bitmap;

public class Bages
{

	private Bages() {
		
	}
	
	static HashMap<String, Bitmap> map = new HashMap<>();
	static final Integer[] numbers = {0, 5, 20, 50, 150, 200, 300, 500, 750, 1000, null};
	static final ArrayList<DataBage> bages = new ArrayList<>();
	
	static {
		for(int i = 0; i < 9; i++) {
			bages.add(new DataBage(i, numbers[i], numbers[i+1]));
		}
	}
	
	public static class DataBage {
		
		public Integer index;
		public Integer min;
		public Integer max;
		
		
		
		public DataBage(Integer index, Integer min, Integer max)
		{
			super();
			this.index = index;
			this.min = min;
			this.max = max;
			
			getNormalBitmap();
			getHighlightBitmap();
			
		}

		public Bitmap getNormalBitmap() {
			return Bages.GetBitmap(index, false);
		}
		
		public Bitmap getHighlightBitmap() {
			return Bages.GetBitmap(index, true);
		}
		
		public String getInterval() {
			return max==null?(String.valueOf(min)):(""+min+"-"+max);
		}
		
	}
	
	
	
	
	
	public static Bitmap GetBitmap(int index, boolean is_on) {
		
		String key = ""+index+""+is_on;
		
		Bitmap b = map.get(key);
		
		if( b == null || b.isRecycled()) {
			b = I.GetAssetsImage("bages/bage0"+(index+1)+(is_on?"_on":"")+".png");
			map.put(key, b);
		}
		
		return b;
	}
	
	public static Integer CurrentIndex() {
		
		int n = Api.Init.getCacheData().trainingFinished;
		
		for(int i = 9; i > 1; i--) {
			
			if(n >= numbers[i]) {
				return i - 1;
			}
			
		}
		
		return 0;
	}
	
	public static int Count() {
		return 9;
	}
	
	public static DataBage Get(int index) {
		return bages.get(index);
	}
	
	
	public static DataBage Current() {
		return bages.get(CurrentIndex());
	}
	
	public static DataBage Next() {
		try
		{
			return bages.get(CurrentIndex() + 1);
		} catch (Exception e)
		{
			return Current();
		}
	}
	
	public static Bitmap CurrentNormalBitmap() {
		return Current().getNormalBitmap();
	}
	
	public static Bitmap CurrentHighlightBitmap() {
		return Current().getHighlightBitmap();
	}
	
	public static Bitmap NextNormalBitmap() {
		return Next().getNormalBitmap();
	}
	
	public static Bitmap NextHighlightBitmap() {
		return Next().getHighlightBitmap();
	}
	
	public static boolean IsNextBageState() {
		
		int n =  Api.Init.getCacheData().trainingFinished;
		
		DataBage bage = Current();
		
		if(bage.max == null) {
			return false;
		}
		
		return bage.max == n + 1;
	}
	
}
