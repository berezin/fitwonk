package fitwonk;

import java.io.FileDescriptor;
import java.util.ArrayList;

import activity.ActivityMain;
import common.Res;
import common.Test;
import android.content.Context;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.View;
import android.view.View.OnClickListener;

import data.DataTraining;
import data.DataTraining.DataExerciseSet;
import com.utils.InternalService;
import com.utils.shorts.E;
import com.utils.EventsUtils.IBinder;
import ui.main.screens.UIScreenTraining;
import ui.main.screens.training.process.ProcessExercise;
import ui.main.screens.training.process.ProcessNextPulse;
import ui.main.screens.training.process.ProcessPulse;
import ui.main.screens.training.process.ProcessRating;
import ui.main.screens.training.process.ProcessRest;
import ui.main.screens.training.process.ProcessStart;
import ui.main.screens.training.process.ProcessThanks;

public class Training
{

	
	
	static {
		
		E.Bind(new IBinder()
		{
			
			public void handle(Integer event, Object param)
			{
				if(Api.Training.EVENT_SAVED.equals(event)) {
					Api.Init.Send();
				}
			}

		});
	}
	
	
	static public Boolean IN_PROCESS = false;
	
	public static abstract class Process {
		
		
		protected Player player;
		
		public void Set(Player player) {
			this.player = player;
		}
		
		public void onActivate() {
			
		}
		
		public void onRestart() {
			
		}
		
		public boolean confirmNext() {
			return true;
		}
		
		public void onDeactivate()
		{
			
			player.getUI().getContentFrame().removeAllViews();
			player.getUI().getBottomFrame().removeAllViews();
			
			player.getUI().getPlayButton().setVisibility(View.VISIBLE);
			player.getUI().getPlayButton().setClickable(false);
			
			player.getUI().getTitleExercise().Set(null, null);
			player.getUI().getTitleNextExercise().Set(null, null);
			player.getUI().hideControls();
			
			player = null;
		}
		
		public Long getDuration() {
			return 5000L;
		}
		
		public Boolean hasTimer() {
			return true;
		}
		
		public Boolean isDurationPart() {
			return false;
		}
		
		public void onUpdate(Long position) {
			
		}
		
		final public void playNext() {
			player.playNext();
		}
		
		final public void setStandartPlayPause() {
			
			player.getUI().getPlayButton().setImageResource(Res.IMG_PLAYER_PAUSE);
			
			player.getUI().getPlayButton().setOnClickListener(new OnClickListener()
			{
				
				@Override
				public void onClick(View v)
				{
					if(player.isPlay()) {
						player.getUI().getPlayButton().setImageResource(Res.IMG_PLAYER_START);
						player.pause();
					}
					else {
						player.getUI().getPlayButton().setImageResource(Res.IMG_PLAYER_PAUSE);
						player.resume();
					}
				}
			});
			
		}
		
	}
	
	public static class Player {

		
		UIScreenTraining uiTraining;
		DataTraining training;
		ArrayList<Process> processes;
		Process currentProcess;
		
		Long positionBeforeCurrentProcess = 0L;
		Long duration = null;
		Long position = 0L;
		Long last;
		InternalService service = new InternalService()
		{
			
			
			public void start() {

				super.start();
			}
			
			
			@Override
			public void process()
			{
				if(currentProcess.hasTimer()) {
					
					Long current = System.currentTimeMillis();
					
					if(last!=null) {
						position += current - last;
					}
					
					last = current;
					
					if(position < currentProcess.getDuration()) {
						
						currentProcess.onUpdate(position);
					}
					else {
						
						if(currentProcess.confirmNext()) {
							playNext();
						} else {
							stop();
						}
					}
				}
				else {
					stop();
				}
			}
			
			@Override
			public int getTimeout() {
				return 100;
			}
		};
		
		public Player(UIScreenTraining uiTraining, DataTraining training) {
			this.uiTraining = uiTraining;
			this.training = training;
			IN_PROCESS = true;
			processes = new ArrayList<>();
			
			processes.add( new ProcessStart() );
			
			int i = 0;
			int n = Api.Init.getCacheData().currentTraining().sets.size();
			
			for(DataExerciseSet set: training.sets) {
				
				processes.add( new ProcessExercise(i, set));
				if(i + 1 != n) {
					processes.add( new ProcessRest(i, set, training.sets.get(i+1)));
				}
				i++;
			}
			
			processes.add( new ProcessNextPulse() );
			processes.add( new ProcessPulse() );
			processes.add( new ProcessRating());
			processes.add( new ProcessThanks() );
			
			currentProcess = processes.get(0);
			startProcess(currentProcess);
		}
		
		public boolean isPlay() {
			return service.IsStarted();
		}

		public void playNext() {
			
			currentProcess.onDeactivate();
			pause();	
			
			int index = processes.indexOf(currentProcess);
			
			if(index < processes.size() - 1) {
				
				startProcess(processes.get(index+1));
				
			}
			
		}

		public void startProcess(Process process) {
			currentProcess = process;
			calcPositionBeforeCurrentProcess();
			process.Set(this);
			process.onActivate();
			play();
		}
		
		public void onStop()
		{
			service.stop();
			currentProcess.onDeactivate();
			processes.clear();
			IN_PROCESS = false;
		}
		
		public void pause()
		{
			service.stop();
		}
		
		public void resume()
		{
			last = null;
			service.start();
		}
		
		public void play()
		{
			position = 0L;
			last = null;
			service.start();
		}
		
		private void calcPositionBeforeCurrentProcess()
		{
			int i = 0;
			positionBeforeCurrentProcess = 0L;
			
			for (Process process : processes)
			{
				
				if(process == currentProcess) {
					break;
				}
				
				positionBeforeCurrentProcess += process.isDurationPart() ? process.getDuration():0L;
				
				i++;
			}
			
		}
		
		public Long getCurrentPosition()
		{
			return positionBeforeCurrentProcess + position;
		}
		
		public Long getDuration()
		{
			
			if(duration == null) {
				
				duration = 0L;
				int i = 0;
				for (Process process : processes)
				{
					duration += process.isDurationPart() ? process.getDuration():0L;
					i++;
				}
			}
			
			return duration;
		}
		
		
		public UIScreenTraining getUI()
		{
			return uiTraining;
		}
		
		public DataTraining getTraining()
		{
			return training;
		}

		public Context getContext()
		{
			
			return uiTraining.getContext();
		}
		
	}
	
	
	public static Player Activate(UIScreenTraining uiTraining, DataTraining training) {
		
		return new Player(uiTraining, training);
	}
	
	
	public static void Deactivate(Player player) {
		
		player.onStop();
	}
	
	
}
