package fitwonk;

import com.utils.parser.ParserUtils.JSONParser;
import com.utils.parser.ParserUtils.JSONParser.Post;

import android.util.Log;
import common.updater.Request;
import data.DataRegisterDict;
import data.DataRegisterFormResult;
import data.DataRegisterPlanResult;
import data.DataRegisterTrainerResult;
import data.DataRegisterMailResult;

public class Registration
{
	
	public static String TAG = "Registration";

	public Request<DataRegisterMailResult> Mail = new Request<DataRegisterMailResult>(DataRegisterMailResult.class, "Registration.User") {


		@Override
		protected DataRegisterMailResult sendSync(Object... args)
		{
			
			Post p = new Post();
			p.add("email", args[0]);
			
			DataRegisterMailResult data = new DataRegisterMailResult();
			
			if(JSONParser.ParsePost(data, Api.Url.Mail(), p.get())) {
				return data;
			}
			
			return null;
		}
		
		protected boolean isSuccess(Request.Responce<DataRegisterMailResult> result) {
			
			return result.data.error == null || result.data.error.length()==0;
		};
		
		@Override
		protected Integer getCacheType()
		{
			return STORAGE_TYPE_SINGLE;
		}
		
		protected DataRegisterMailResult onMerge(DataRegisterMailResult oldData, Request.Responce<DataRegisterMailResult> r) {
			
			r.data.email = (String) r.args[0];
			
			return r.data;
		};
		
		protected void onSuccess(Request.Responce<DataRegisterMailResult> result) {
			Log.w("Registration", result.data.toString());
		};
		
		protected boolean hasRequestStory() {
			return true;
		};
		
	};
	
	public Request<DataRegisterDict> Dict = new Request<DataRegisterDict>(DataRegisterDict.class, "Registration.Dict")
	{
		
		@Override
		protected DataRegisterDict sendSync(Object... args)
		{
			return standartGet(Api.Url.Register());
		}
		
		protected Integer getCacheType() {
			return STORAGE_TYPE_SINGLE;
		};
		
		protected boolean hasRequestStory() {
			return true;
		};

	};
	
	public Request<DataRegisterFormResult> Form = new Request<DataRegisterFormResult>(DataRegisterFormResult.class, "Registration.Form") {


		@Override
		protected DataRegisterFormResult sendSync(Object... args)
		{
			return standartPost(Api.Url.Register(), (Post) args[0]);
		}
		
		protected boolean isSuccess(Request.Responce<DataRegisterFormResult> result) {
			
			if(result.data.error!=null && result.data.error.length()>0) {
				return false;
			}
			
			return true;
		}
		
		protected void onSuccess(Request.Responce<DataRegisterFormResult> result) {
			
			Post p = (Post) result.args[0];
			
			result.data.password = p.get().get("password").toString();
			
		};
		
		protected Integer getCacheType() {
			return STORAGE_TYPE_SINGLE;
		};
		
		protected boolean hasRequestStory() {
			return true;
		};
		
	};
	
	public Request<DataRegisterTrainerResult> Trainer = new Request<DataRegisterTrainerResult>(DataRegisterTrainerResult.class, "Registration.Trainer") {


		@Override
		protected DataRegisterTrainerResult sendSync(Object... args)
		{
			
			Post p = new Post();
			p.add("trainerId", args[0]);
			
			return standartPut(Api.Url.Register(), p);
		}
		
		protected boolean isSuccess(Request.Responce<DataRegisterTrainerResult> result) {
			
			if(result.data.error!=null && result.data.error.length()>0) {
				return false;
			}
			
			return true;
		}
		
		protected Integer getCacheType() {
			return STORAGE_TYPE_SINGLE;
		};
		
		protected boolean hasRequestStory() {
			return true;
		};
		
	};
	
	public Request<DataRegisterTrainerResult> Plans = new Request<DataRegisterTrainerResult>(DataRegisterTrainerResult.class, "Registration.Plans") {


		@Override
		protected DataRegisterTrainerResult sendSync(Object... args)
		{
			return standartGet(Api.Url.Signup());
		}
		
		protected boolean isSuccess(Request.Responce<DataRegisterTrainerResult> result) {
			
			if(result.data.error!=null && result.data.error.length()>0) {
				return false;
			}
			
			return true;
		}
		
		protected boolean hasRequestStory() {
			return true;
		};
		
	};
	
	public Request<DataRegisterPlanResult> Payment = new Request<DataRegisterPlanResult>(DataRegisterPlanResult.class, "Registration.Payment") {


		@Override
		protected DataRegisterPlanResult sendSync(Object... args)
		{
			
			Post p = new Post();
			
			p.add("planId", args[0]);
			p.add("code", args[1]);
			
			return standartPost(Api.Url.Signup(), p);
		}
		
		protected boolean isSuccess(Request.Responce<DataRegisterPlanResult> result) {
			
			if(result.data.error!=null && result.data.error.length()>0) {
				return false;
			}
			
			return true;
		}
		
		protected Integer getCacheType() {
			return STORAGE_TYPE_SINGLE;
		};
		
		protected boolean hasRequestStory() {
			return true;
		};
		
	};
	
	public String getToken()
	{
		return Mail.getCacheData().accesstoken;
	}

	public void ClearCacheData()
	{
		Api.Registration.Mail.clearCacheData();
		Api.Registration.Form.clearCacheData();
		Api.Registration.Trainer.clearCacheData();
		Api.Registration.Payment.clearCacheData();
	}
	
	
	
}
