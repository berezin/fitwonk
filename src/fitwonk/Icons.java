package fitwonk;

import java.io.File;
import java.util.ArrayList;

import com.utils.App;
import com.utils.ThreadTransanction;
import com.utils.ThreadTransanction.ThreadRunnable;
import com.utils.ThreadTransanction.ThreadStatus;
import com.utils.Utils;
import com.utils.io.IO;
import com.utils.shorts.I;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.Config;
import android.graphics.Color;
import android.util.Log;

public class Icons
{

	final public static String TAG = "Icons";
	
	final public static String STORAGE = "icons";
	
	public static interface ICallback{
		void post(Bitmap bitmap);
	}
	
	public static interface IImageProvider {
		String getKey(Integer color);
		Bitmap requestBitmap();
	}
	
	public static class ResourceImageProvider implements IImageProvider {

		Integer id;

		public ResourceImageProvider(Integer id) {
			this.id = id;
		}
		
		@Override
		public String getKey(Integer color)
		{
			return "res"+id+"_"+color;
		}

		@Override
		public Bitmap requestBitmap()
		{
			return BitmapFactory.decodeResource(App.get().getResources(), id, Utils.getOptions());
		}
		
	}
	
	public static class UrlImageProvider implements IImageProvider {

		String url;

		public UrlImageProvider(String url) {
			this.url = url;
		}
		
		@Override
		public String getKey(Integer color)
		{
			return ""+url.hashCode()+"_"+color;
		}

		@Override
		public Bitmap requestBitmap()
		{
			return IO.BitmapFromUrl(url);
		}
		
	}
	
	static ArrayList<String> keys = new ArrayList<>();
	
	
	public static void Get(String url, Integer color, ICallback callback)
	{
		Get(color, callback, new UrlImageProvider(url));
	}
	
	public static void Get(Integer res, Integer color, ICallback callback)
	{
		Get(color, callback, new ResourceImageProvider(res));
	}
	
	public static void Get(final Integer color, final ICallback callback, final IImageProvider provider)
	{
		
		
		final String name = provider.getKey(color);

		Bitmap b = I.getBitmapFromMemCache(name);
		
		if(b!=null && !b.isRecycled())
		{
			callback.post(b);
		}
		else
		{

				ThreadTransanction.execute_http("icons "+name, new ThreadRunnable<Bitmap>()
				{
	
					@Override
					public void result(ThreadStatus aStatus, Bitmap aResult)
					{
						keys.remove(name);
						
						if(aResult!=null)
						{
							I.addBitmapToMemoryCache(name, aResult);
							callback.post(aResult);
						}

					}

					@Override
					public Bitmap run()
					{

						while(keys.contains(name)) {
							try
							{
								Thread.sleep(100);
							} catch (InterruptedException e)
							{
								
							}
							
							
						}
						
						Bitmap bMem = I.getBitmapFromMemCache(name);
						
						if(bMem!=null)
						{
							return bMem;
						}
						
						keys.add(name);
						
						File f = new File(IO.FileNameFromUrl(STORAGE, name));
						Bitmap bitmap = null;
						
						if(f.exists())
						{
							
							synchronized (TAG)
							{
								bitmap = IO.BitmapFromFile(f);
							}
							
						}
						
						if(bitmap==null)
						{
							
							
							Bitmap b = provider.requestBitmap();
							
							if(b!=null)
							{
								
								int r = Color.red(color);
								int g = Color.green(color);
								int bl = Color.blue(color);
								
								bitmap =  b.copy(Config.ARGB_8888, true);
								
								int w = bitmap.getWidth();
								int h = bitmap.getHeight();
								
								for(int x = 0; x < w; x++)
								{
									for(int y = 0; y < h; y++)
									{
										int a = Color.alpha(bitmap.getPixel(x, y));
										bitmap.setPixel(x, y, Color.argb(a, r, g, bl));
									}
								}
								
								synchronized (TAG)
								{
									IO.BitmapToPNGFile(f, bitmap);
								}
								
								
								return bitmap;
							}
						}

						return bitmap;
					}
				});
		}
	}
	
	
	
}
