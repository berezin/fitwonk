package fitwonk;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.utils.ThreadTransanction;
import com.utils.ThreadTransanction.ThreadRunnable;
import com.utils.ThreadTransanction.ThreadStatus;
import com.utils.parser.ParserUtils.JSONParser;
import com.utils.setting.SettingHelper.Storage;
import com.utils.shorts.E;

import android.util.Log;
import common.updater.Request;
import common.updater.UpdaterUrl;
import data.DataComments;
import data.DataDialogs;
import data.DataDialogs.DataDialog;
import data.DataInit;
import data.DataToken;
import data.DataTraining;
import data.DataTraining.DataExerciseSet;

public class Api
{

	// trainee@b-co.org
	// http://fitwonk.b-co.org/trainee
	// login: berezin
	// Пароль: berezin
	
	//сертификат eB8D2qXjb774R2EVRa
	// ababab@mt2015.com
	//http://fitwonk.api.b-co.org/register?access-token=QWhZFDFqFqK1NkHLTDjaIQU1DYmtUfpL
	//http://fitwonk.api.b-co.org/signup?access-token=QWhZFDFqFqK1NkHLTDjaIQU1DYmtUfpL

	// http://fitwonk.api.b-co.org/init?access-token=6Ha7pRl5zgmYdX99RJC_Mu0wl9ZKCMq9
	// http://fitwonk.api.b-co.org/anthropometry?access-token=6Ha7pRl5zgmYdX99RJC_Mu0wl9ZKCMq9&total=22
	// http://fitwonk.api.b-co.org/training/2?access-token=6Ha7pRl5zgmYdX99RJC_Mu0wl9ZKCMq9
	
	// http://d2lgycgn2xkfkn.cloudfront.net/102_mahi-nogoy-lega-na-boku-720.mp4
	// a_elACvfBF3puttEoIwqDgc43ZKmWIa_
	// http://fitwonk.ru/site/register?token=a_elACvfBF3puttEoIwqDgc43ZKmWIa_

	public static class Url
	{

		public static String Domain = "http://fitwonk.api.b-co.org";

		public static String Auth(String login, String pass)
		{

			return Domain + "/auth?email=" + login + "&password=" + pass;
		}

		public static String PutTraining(Integer id)
		{

			return Domain + "/training/" + id + "?access-token="
					+ getToken();
		}

		public static String Init()
		{
			return Domain + "/init?access-token=" + getToken();
		}

		public static String Dialogs()
		{
			return Domain + "/dialog?access-token=" + getToken();
		}

		public static String Dialogs(int id)
		{
			return Domain + "/dialog/" + id + "?access-token="
					+ getToken();
		}

		public static String Anthropometry()
		{
			return Domain + "/anthropometry?access-token=" + getToken()
					+ "&total=15";
		}
		
		public static String PostAnthropometry()
		{
			return Domain + "/anthropometry?access-token=" + getToken();
		}
		
		public static String Anthropometry(int id)
		{
			return Domain + "/anthropometry/"+id+"?access-token=" + getToken();
		}
		
		public static String Mail()
		{
			return Domain + "/user";
		}
		
		public static String Register()
		{
			return Domain + "/register?access-token=" + Registration.getToken();
		}
		
		public static String Dictionary()
		{
			return Domain + "/register?access-token=" + getToken();
		}
		
		public static String Signup()
		{
			return Domain + "/signup?access-token=" + Registration.getToken();
		}

		
		public static String UserSetting()
		{
			return Domain + "/user/"+Init.getCacheData().id+"?access-token=" + getToken();
		}

	}
	
	
	public static boolean hasToken()
	{
		return !Auth.IsEmpty();
	}
	
	public static String getToken()
	{
		return Auth.getCacheData().accessToken;
	}
	
	public static Request<DataToken> Auth = new Request<DataToken>(DataToken.class, "Api.Auth") {


		@Override
		protected DataToken sendSync(Object... args)
		{
			return standartGet(Api.Url.Auth((String)args[0], (String)args[1]));
		}
		
		protected boolean isSuccess(Request.Responce<DataToken> result) {
			
			if(result.data.accessToken == null || result.data.error!=null && result.data.error.length()>0) {
				return false;
			}
			
			return true;
		}
		
		protected Integer getCacheType() {
			return STORAGE_TYPE_SINGLE;
		};
		
		protected boolean hasRequestStory() {
			return true;
		};
		
	};


	public static class Training
	{

		public static Integer EVENT_SAVED = E.Id("Training.EVENT_SAVED");
		public static Integer EVENT_ERROR = E.Id("Training.EVENT_ERROR");

		public static void SaveTest()
		{

			DataTraining training = Api.Init.getCacheData().trainings.get(0);

			Integer id = training.id;
			Long finishedAt = System.currentTimeMillis() / 1000L;
			Long startedAt = finishedAt - 300L;
			Integer rate = 5;
			String comment = "Test";

			for (DataExerciseSet set : training.sets) {
				set.fact = set.plan;
				set.finishedAt = finishedAt;
				set.startedAt = startedAt;
			}

			Save(id, training.sets, rate, comment, startedAt, finishedAt);

		}

		public static void Save(final Integer id,
				final ArrayList<DataExerciseSet> sets, final Integer rate,
				final String comment, final Long startedAt,
				final Long finishedAt)
		{

			ThreadTransanction.execute_http("Training Save",
					new ThreadRunnable<Integer>()
					{

						JSONObject parent;

						@Override
						public void result(ThreadStatus aStatus,
								Integer aResult)
						{
							if(aResult == 1)
							{
								E.Event(EVENT_SAVED, id);
							} else
							{
								E.Event(EVENT_ERROR, id);
							}
						}

						@Override
						public Integer run()
						{
							parent = new JSONObject();
							return SaveSync(parent, id, sets, rate, comment,
									startedAt, finishedAt);
						}
					});

		}

		public static int SaveSync(JSONObject parent, Integer id,
				ArrayList<DataExerciseSet> sets, Integer rate, String comment,
				Long startedAt, Long finishedAt)
		{

			try
			{

				String url = Url.PutTraining(id);

				if(rate != null)
					parent.put("rate", rate);
				if(comment != null)
					parent.put("comment", comment);

				parent.put("startedAt", startedAt);
				parent.put("finishedAt", finishedAt);

				JSONArray arraySets = new JSONArray();

				for (DataExerciseSet set : sets)
				{

					JSONObject child = new JSONObject();
					child.put("id", set.id);
					child.put("exerciseId", set.exerciseId);
					child.put("order", set.order);
					child.put("startedAt", set.startedAt);
					child.put("finishedAt", set.finishedAt);

					JSONObject fact = new JSONObject();
					fact.put("duration", set.fact.duration);
					fact.put("repeats", set.fact.repeats);
					fact.put("resttime", set.fact.resttime);
					child.put("fact", fact);

					arraySets.put(child);

				}

				parent.put("sets", arraySets);
				Object data = new Object();
				if(JSONParser.ParsePut(data, url, parent))
				{
					return 1;
				}

			} catch (Exception e)
			{
				e.printStackTrace();
			}

			return 0;

		}

		public static void Save()
		{
			DataTraining training = Api.Init.getCacheData().currentTraining();

			Save(training.id, training.sets, training.rate, training.comment,
					training.startedAt, training.finishedAt);
		}

	}

	public static class Dialog
	{

		public static Integer SETTING_DIALOGS = E.Id("Dialog.SETTING_DIALOGS");
		public static Integer EVENT_ERROR = E.Id("Dialog.EVENT_ERROR");

		private static Storage<DataDialogs> DIALOGS = new Storage<DataDialogs>(
				SETTING_DIALOGS)
		{
			{
				setGZIP(true);
			}
		};

		public static void sendComment(final int idDialog, final String comment)
		{

			ThreadTransanction.execute_http("sendComment",
					new ThreadRunnable<DataComments>()
					{

						@Override
						public void result(ThreadStatus aStatus,
								DataComments aResult)
						{
							if(aResult != null)
							{
								for (DataDialog d : getData().dialog)
								{
									if(d.id.equals(idDialog))
									{

										d.Comments = aResult.Comments;
										DIALOGS.Save();
										break;
									}
								}
							} else
							{
								E.Event(EVENT_ERROR);
							}
						}

						@Override
						public DataComments run()
						{

							return sendCommentSync(idDialog, comment);
						}
					});

		}

		public static DataComments sendCommentSync(int idDialog, String comment)
		{

			try
			{

				String url = Url.Dialogs(idDialog);

				JSONObject parent = new JSONObject();
				parent.put("comment", comment);

				DataComments data = new DataComments();
				if(JSONParser.ParsePost(data, url, parent))
				{
					return data;
				}

			} catch (Exception e)
			{
				e.printStackTrace();
			}

			return null;
		}

		public static DataDialogs requestDataSync()
		{

			String url = Url.Dialogs();
			Log.w("Api", url);
			DataDialogs data = new DataDialogs();

			if(JSONParser.Parse(data, url))
			{
				return data;
			}

			return null;
		}

		static boolean isRequest = false;

		public static void requestData()
		{

			if(isRequest)
			{
				return;
			}

			isRequest = true;

			ThreadTransanction.execute_http("Get Dialog",
					new ThreadRunnable<DataDialogs>()
					{

						@Override
						public void result(ThreadStatus aStatus,
								DataDialogs aResult)
						{
							isRequest = false;

							if(aResult != null)
							{
								DIALOGS.Save(aResult);
							}
						}

						@Override
						public DataDialogs run()
						{
							return requestDataSync();
						}
					});

		}

		public static DataDialogs getData()
		{
			return DIALOGS.Get();
		}
		
		public static void clearData()
		{
			DIALOGS.Clear();
		}
		

	}
	
	public static Settings Settings = new Settings();

	public static Registration Registration = new Registration();
	
	public static UpdaterAnthropometry Anthropometry = new UpdaterAnthropometry();

	public static Request<DataInit> Init = new Request<DataInit>(DataInit.class, "Api.Init")
	{
		
		@Override
		protected DataInit sendSync(Object... args)
		{
			
			Log.w("Api.Init", Url.Init());
			return standartGet(Url.Init());
		}
		
		protected boolean isSuccess(Request.Responce<DataInit> result) {
			
			if(result.data.error!=null && result.data.error.length()>0) {
				return false;
			}
			
			return true;
		}
		
		protected Integer getCacheType() {
			return STORAGE_TYPE_SINGLE;
		};
		
		protected boolean hasRequestStory() {
			return true;
		};
		
	};
	
	
	public static void onAppStart() {
		Anthropometry.requestData();
		Dialog.requestData();
		Settings.Dictionary.Send();
	}
	
	public static void ClearUserData() {
		Anthropometry.clear();
		Settings.ClearCacheData();
		Dialog.clearData();
		Registration.ClearCacheData();
	}
	

}
