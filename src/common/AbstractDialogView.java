package common;

import com.utils.AnimationUtils.IEvent;

import java.util.ArrayList;

import com.utils.Dimen;
import com.utils.EventsUtils.BindedRelativeLayout;
import com.utils.shorts.A;
import com.utils.shorts.LPR;

import android.R;
import android.content.Context;
import android.content.DialogInterface.OnKeyListener;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

public abstract class AbstractDialogView extends BindedRelativeLayout
{

	ArrayList<AbstractDialogView> list = new ArrayList<>();

	public static class Dialog extends DialogFragment
	{
		AbstractDialogView ui;
		View back;
		
		boolean isStartAnimation = false;
		boolean isNeedCloseAfterStartAnimation = false;
		
		public static Dialog newInstance(AbstractDialogView ui,
				String className)
		{
			Dialog dialog = new Dialog();
			dialog.ui = ui;

			Bundle b = new Bundle();
			b.putString("className", className);

			ui.createBundle(b);

			dialog.setArguments(b);
			dialog.setStyle(DialogFragment.STYLE_NORMAL, R.style.Theme_Black);

			return dialog;
		}

		@SuppressWarnings("rawtypes")
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState)
		{

			Bundle b = getArguments();

			if(ui == null)
			{

				String className = b.getString("className");

				try
				{
					Class cl = Class.forName(className);
					ui = AbstractDialogView.createView(cl, getActivity());
				} catch (Exception e)
				{

				}

				if(ui == null)
				{
					dismiss();
					return new View(getActivity());
				}

			}

			ui.setBundle(b);
			isStartAnimation = true;
			A.fromy_and_show(ui, 300, Dimen.d10, false, new IEvent()
			{
				
				@Override
				public void onEnd()
				{
					isStartAnimation = false;
					if(isNeedCloseAfterStartAnimation) {
						dismiss();
					}
				}
			});

			RelativeLayout frame = new RelativeLayout(getActivity());

			back = new View(getActivity());
			back.setBackgroundColor(0x44000000);

			frame.addView(back, LPR.create().get());
			A.fadein(back, 300);
			
			if(ui.isCancelable()) {
				
				frame.setOnClickListener(new OnClickListener()
				{
					
					@Override
					public void onClick(View v)
					{
						dismiss();
					}
				});
				
				ui.setOnClickListener(new OnClickListener()
				{
					
					@Override
					public void onClick(View v)
					{
						
					}
				});
				
			}
			

			frame.addView(ui,
					LPR.create(ui.getDialogWidth(), ui.getDialogHeight())
							.center().get());

			getDialog().getWindow()
					.setBackgroundDrawable(new ColorDrawable(0x00000000));
			getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

			return frame;
		}

		@Override
		public void onResume()
		{

			super.onResume();

			getDialog().setOnKeyListener(
					new android.content.DialogInterface.OnKeyListener()
					{
						public boolean onKey(
								android.content.DialogInterface dialog,
								int keyCode, android.view.KeyEvent event)
						{
							if((keyCode == android.view.KeyEvent.KEYCODE_BACK))
							{
								if(ui != null && ui.isCancelable())
								{
									dismiss();
								}

								return true;
							}

					else
								return false;
						}
					});

		}

		boolean isDismissing = false;

		
		private void postDismiss() {
			
			if(back!=null) {
				back.clearAnimation();
				back.setVisibility(INVISIBLE);
				back = null;
			}
			
			if(ui != null) { 
				
				ui.clearAnimation();
				ui.setVisibility(INVISIBLE);

				ui.post(new Runnable()
				{
					
					@Override
					public void run()
					{
						Dialog.super.dismiss();
					}
				});
				
				
				ui = null;
				
			}
			else {
				Dialog.super.dismiss();
			}
		}
		
		@Override
		public void dismiss()
		{
			
			if(isStartAnimation) {
				isNeedCloseAfterStartAnimation = true;
				return;
			}

			if(isDismissing)
			{
				return;
			}

			isDismissing = true;
			
			

			if(ui != null)
			{

				if(back != null)
				{
					A.fadeout(back, 250);

				}

				A.to_up_and_hide(ui, 300, Dimen.d10, true, new IEvent()
				{

					@Override
					public void onEnd()
					{
						
						postDismiss();
						
					}
				});

			} else
			{
				super.dismiss();
			}

		}

	}

	public AbstractDialogView(Context context)
	{
		super(context);
	}

	Dialog dialog;
	
	@Override
	protected void onAttachedToWindow()
	{
		
		super.onAttachedToWindow();
		list.add(this);
	}
	
	@Override
	protected void onDetachedFromWindow()
	{
		super.onDetachedFromWindow();
		list.remove(this);
	}

	public void show()
	{

		boolean isNeedDelay = list.size()!=0;
		
		if(isNeedDelay) {
			postDelayed(new Runnable()
			{
				
				@Override
				public void run()
				{
					show();
				}
			}, 100);
		}
		else {
			
			dialog = Dialog.newInstance(this, getClass().toString());

			android.support.v4.app.FragmentManager fm = ((FragmentActivity) getContext())
					.getSupportFragmentManager();

			dialog.show(fm, getClass().toString());

		}
		
		

	}

	public void hide()
	{
		if(dialog != null)
		{
			dialog.dismiss();
			dialog = null;
		}
	}
	
	protected void onHide() {
		list.remove(this);
	}

	public abstract void createBundle(Bundle b);

	public abstract void setBundle(Bundle b);

	public int getDialogWidth()
	{
		return LPR.WRAP;
	}

	public boolean isCancelable()
	{
		return true;
	}

	public int getDialogHeight()
	{
		return LPR.WRAP;
	}

	@SuppressWarnings("rawtypes")
	private static AbstractDialogView createView(Class cl, Context context)
	{

		try
		{
			return (AbstractDialogView) cl.getConstructors()[0]
					.newInstance(context);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

}
