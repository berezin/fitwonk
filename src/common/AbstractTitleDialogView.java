package common;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import ui.common.UITitle;

import com.utils.shorts.LPL;
import com.utils.shorts.LPR;
import com.utils.shorts.Q;
import com.utils.Dimen;

public abstract class AbstractTitleDialogView extends AbstractDialogView
{

	UITitle uiTitle;
	String strTitle = "";
	View content;
	RelativeLayout buttons;
	
	public AbstractTitleDialogView(Context context)
	{
		super(context);
		
		uiTitle = new UITitle(getContext());
		addView(uiTitle, LPR.create(UITitle.Height).get());
		uiTitle.Set(strTitle);
		
		setBackgroundColor(Palette.GRAY_BACKGROUND);
		
		
		content = getContentView();
		content.setId(1);
		addView(content, LPR.createMW().marginTop(UITitle.Height).get());
		
		buttons = new RelativeLayout(getContext());
		addView(buttons, LPR.createMW().below(1).get());

		
	}
	
	
	@Override
	public int getDialogWidth()
	{
		
		return (int) (Q.getW() - Dimen.d40);
	}

	public void setTitle(String title) {
		strTitle = title;
		uiTitle.Set(title);
	}

	@Override
	public final void createBundle(Bundle b)
	{
		b.putString("title", strTitle);
		createDialogBundle(b);
	}

	@Override
	public final void setBundle(Bundle b)
	{
		setTitle(b.getString("title"));
		setDialogBundle(b);
	}
	
	public abstract void createDialogBundle(Bundle b);
	
	public abstract void setDialogBundle(Bundle b);
	
	public abstract View getContentView();
	
	public TextView addButton(String str) {
		
		
		TextView text = new TextView(getContext());
		Paints.SetText(text, Type.Futura, 12, Palette.BLUE);
		text.setGravity(Gravity.CENTER);
		text.setText(str);
		text.setId(buttons.getChildCount() + 1);
		int padH = Dimen.d15;
		text.setPadding(padH, 0 , padH, 0);
		
		LPR lpr = LPR.create(LPL.WRAP, Dimen.d50);
		
		if(buttons.getChildCount() == 0) {
			lpr.right();
		} else {
			lpr.leftOf(buttons.getChildCount());
		}
		
		buttons.addView(text, lpr.get());
		
		return text;
	}
	

}
