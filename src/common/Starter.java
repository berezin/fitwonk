package common;

import activity.ActivityMain;
import activity.ActivitySplash;
import android.content.Context;
import android.content.Intent;


public class Starter
{

	public static void Main(Context context)
	{
		Intent intent = new Intent(context, ActivityMain.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		context.startActivity(intent);
	}
	
	public static void Splash(Context context)
	{
		Intent intent = new Intent(context, ActivitySplash.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		context.startActivity(intent);
	}
	
	
}
