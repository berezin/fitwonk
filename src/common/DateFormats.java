package common;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormats
{
	static public SimpleDateFormat OnlyDate = new SimpleDateFormat("dd.MM.yy");
	static public SimpleDateFormat OnlyDate2 = new SimpleDateFormat("yyyy-MM-dd");
	static public SimpleDateFormat SimpleDate = new SimpleDateFormat("dd/MM");
	static public SimpleDateFormat Duration = new SimpleDateFormat("mm:ss");
	static public SimpleDateFormat TIME = new SimpleDateFormat("HH:mm");
	
	static public String Duration(Long date) {
		return Duration.format(new Date(date));
	}
	
	static public String Time(Long date) {
		return TIME.format(new Date(date));
	}
	
	static public String OnlyDate(Long date) {
		return OnlyDate.format(new Date(date));
	}
	
	static public String SimpleDate(Long date) {
		return SimpleDate.format(new Date(date));
	}
	
}
