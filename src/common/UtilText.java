package common;

public class UtilText
{
	public static CharSequence trim(CharSequence source)
	{

		try
		{
			if(source == null)
				return "";
			
			

			int i = source.length();

			if(i == 0)
			{
				return null;
			}

			while (--i >= 0
					&& (Character.isWhitespace(source.charAt(i)) || source
							.charAt(i) == '\n'))
			{
				
				
			}
			
			int j = 0;
			
			while (j < source.length()
					&& (Character.isWhitespace(source.charAt(j)) || source
							.charAt(j) == '\n'))
			{
				
				j++;
			}
			
			

			if(i == 0 || i - j <= 0)
			{
				return null;
			}

			return source.subSequence(j, i + 1 - j);
		} catch (Exception e)
		{

			e.printStackTrace();
		}

		return source;
	}
}
