package common;

import com.utils.TypeUtils;

import android.graphics.Typeface;

public class Type
{
	public static String Futura = "futurafuturisc.ttf";

	public static Typeface Futura() {
		return TypeUtils.Get(Futura);
	}
	
	public static String FuturaLight = "futurafuturisclight.ttf";

	public static Typeface FuturaLight() {
		return TypeUtils.Get(FuturaLight);
	}
	
	
}
