package common;

import com.fitwonk.BuildConfig;

public class Test
{

	public static boolean hasTest = true;

	//
	public static boolean isNextBageState = true;
	//
	public static boolean isSpeedTraining = true;
	public static Long trainingFirstDuration = 5L;
	public static Long trainingExerciseDuration = 5L;
	public static Long trainingRestDuration = 5L;

	
	//
	public static boolean isFirstTraining = true;
	public static boolean isAutoInputRepeats = true;
	//
	public static boolean isFakePulse = true;
	public static Integer fakePulse = 60;
	//
	public static boolean isTodayTraining = true;
	//
	public static boolean isFakeAnthGraphs = false;
	//
	public static boolean isCompleteAllTrainings = false;
	//
	public static boolean isLogoutByAvatarClick = false;

	static
	{
		if(!hasTest || !BuildConfig.DEBUG)
		{
			isNextBageState = false;
			isSpeedTraining = false;
			isFirstTraining = false;
			isAutoInputRepeats = false;
			isFakePulse = false;
			isTodayTraining = false;
			isFakeAnthGraphs = false;
			isCompleteAllTrainings = false;
			isLogoutByAvatarClick = false;
		}
	}

}
