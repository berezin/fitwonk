package common;

import com.utils.shorts.Q;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.fitwonk.R;

//resource hub
public class Res
{

	//надпись fitwonk в шапке
	public static Integer TITLE = R.drawable.title;
	
	//кнопка меню
	public static Integer MENU_BUTTON = R.drawable.menu;
	
	//иконки меню
	public static Integer IC_ITEM_MENU_MAIN = R.drawable.item_menu_main;
	public static Integer iC_ITEM_MENU_CALENDAR = R.drawable.item_menu_calendar;
	public static Integer IC_ITEM_MENU_EATING = R.drawable.item_menu_eating;
	public static Integer IC_ITEM_MENU_TRAINER = R.drawable.item_menu_trainer;
	public static Integer IC_ITEM_MENU_GOAL = R.drawable.item_menu_goal;
	public static Integer IC_ITEM_MENU_SETTINGS = R.drawable.item_menu_settings;
	//иконки подразделов
	public static Integer IC_BACK = R.drawable.ic_back;
	public static Integer IC_DIALOGS = R.drawable.ic_dialogs;
	public static Integer IC_BAGES = R.drawable.ic_bages;
	public static Integer IC_SIZE = R.drawable.ic_size;
	public static Integer IC_WEIGHT = R.drawable.ic_weight;
	public static Integer IC_TRAININGS = R.drawable.ic_trainings;
	//иконки настроек
	public static Integer IC_PROFILE = R.drawable.ic_profile;
	public static Integer IC_ANTH = R.drawable.ic_anth_set;
	public static Integer IC_TARGETS = R.drawable.ic_targets_set;
	public static Integer IC_NOTIFICATION = R.drawable.ic_notification;
	
	//иконки плейера
	public static Integer IC_PAUSE = R.drawable.ic_pause;
	public static Integer IC_PLAY = R.drawable.ic_play_arrow;
	public static Integer IC_SKIP_PREVIOUS = R.drawable.ic_skip_previous;
	
	//заголовок
	public static Integer IMG_TITLE_LEFT = R.drawable.title_left;
	public static Integer IMG_TITLE_RIGHT = R.drawable.title_right;
	public static Integer IMG_TITLE_FILL = R.drawable.title_fill;
	
	//плэйер
	public static Integer IMG_PLAYER_BEGIN = R.drawable.player_begin;
	public static Integer IMG_PLAYER_START = R.drawable.player_start;
	public static Integer IMG_PLAYER_PAUSE = R.drawable.player_pause;
	public static Integer IMG_PLAYER_OK = R.drawable.player_ok;
	public static Integer IMG_PLAYER_PULSE = R.drawable.player_pulse;
	
	public static Integer IMG_PLAYER_MUTE = R.drawable.player_mute;
	public static Integer IMG_PLAYER_HELP = R.drawable.player_help;
	
	public static Integer IMG_PLAYER_HEART = R.drawable.player_heart;
	
	public static Integer[] IMG_PLAYER_RATES = {R.drawable.rate_1,R.drawable.rate_2,R.drawable.rate_3,R.drawable.rate_4,R.drawable.rate_5};
	//splash
	public static Integer IMG_SPLASH = R.drawable.splashlogo;
	public static Integer IMG_PLACEHOLDER_CIRCLE_GRAY = R.drawable.placeholder_circle;
	public static Integer IMG_PLACEHOLDER_CIRCLE_WHITE = R.drawable.placeholder_circle_white;
	
	public static Drawable BACKGROUND_BLUE () {
		Bitmap bmp = BitmapFactory.decodeResource(Q.getResources(), R.drawable.background_blue);
	    BitmapDrawable bitmapDrawable = new BitmapDrawable(bmp);
	    bitmapDrawable.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
	    
	    return bitmapDrawable;
	}
	
	
	
	//тексты 
	//меню
	public static Integer STR_ITEM_MENU_MAIN = R.string.item_menu_main;
	public static Integer STR_ITEM_MENU_CALENDAR = R.string.item_menu_calendar;
	public static Integer STR_ITEM_MENU_CALENDAR_DESC = R.string.item_menu_calendar_desc;
	public static Integer STR_ITEM_MENU_EATING = R.string.item_menu_eating;
	public static Integer STR_ITEM_MENU_TRAINER = R.string.item_menu_trainer;
	public static Integer STR_ITEM_MENU_GOAL = R.string.item_menu_goal;
	public static Integer STR_ITEM_MENU_SETTINGS = R.string.item_menu_settings;
	//info
	public static Integer STR_INFO_CALENDAR_DAY = R.string.info_calendar_day;
	//error
	public static String STR_ERROR_CONNECTION = "Ошибка соединения";
}
