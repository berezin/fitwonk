package common;

import java.util.HashMap;

import com.utils.TypeUtils;
import com.utils.shorts.D;
import com.utils.shorts.Q;
import android.text.TextPaint;
import android.widget.TextView;

public class Paints
{

	
	
	static HashMap<Integer, TextPaint> hash = new HashMap<>();
	
	public static void SetText(TextView text, int size, int color) {
		SetText(text, null, size, color);
	}
	
	public static TextPaint GetPaint(String type, int size, int color) {
		
		int key = (type!=null?type.hashCode():0) + size + color;

		TextPaint p = hash.get(key);
		
		if(p == null) {
			
			p = new TextPaint();
			
			p.setAntiAlias(true);
			p.setTextSize(Q.getRealSize(size));
			p.setColor(color);
			
			if(type!=null)
			{
				p.setTypeface(TypeUtils.Get(type));
			}
			
			hash.put(key, p);
		}
		
		return p;
		
	}
	
	public static void SetText(TextView text, String type, int size, int color) {
		

		TextPaint p = GetPaint(type, size, color);
		
		text.getPaint().set(p);
		text.setTextColor(color);
		
	}
	
}
