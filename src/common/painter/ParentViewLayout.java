package common.painter;

import java.util.ArrayList;

import com.utils.ThreadTransanction;
import com.utils.ThreadTransanction.ThreadRunnable;
import com.utils.ThreadTransanction.ThreadStatus;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Looper;
import android.view.MotionEvent;
import android.view.View;
import common.painter.ViewDrawer.ILayoutParent;

public abstract class ParentViewLayout extends View implements ILayoutParent
{

	ArrayList<ViewDrawer> drawers = new ArrayList<>();
	ArrayList<ViewDrawer> drawersAsync = new ArrayList<>();
	private Integer color;
	
	public ParentViewLayout(Context context)
	{
		super(context);
		
	}
	
	@Override
	public void setBackgroundColor(int color)
	{

		this.color = color;
		
		if(Looper.getMainLooper() == Looper.myLooper()) {
			invalidate();
		}
		
	}
	
	
	static ArrayList<ParentViewLayout> listParents = new ArrayList<>();
	static boolean isRun = false;
	static ThreadRunnable<Object> runnable = new ThreadRunnable<Object>()
	{
		
		@Override
		public Object run()
		{
			
			
			while(listParents.size()!=0) {
				
				while(listParents.size()!=0) {

					ParentViewLayout parent = listParents.remove(0);
					
					try
					{
						parent.asyncSet();
						parent.postInvalidate();
					} catch (Exception e)
					{
						e.printStackTrace();
					}
				}
				
				try
				{
					Thread.sleep(100);
				} 
				catch (Exception e)
				{
					
				}
			}
			
			
			return 1;
		}
		
		@Override
		public void result(ThreadStatus aStatus, Object aResult)
		{
			
			
			if(listParents.size()!=0) {
				ThreadTransanction.execute("async set ParentViewLayout repeat", this);
			}
			else {
				isRun = false;
			}
			
			
			
			
		}
	};
	
	final public void runAsyncSet() {

		listParents.add(this);
		
		if(!isRun) {
			isRun = true;
			ThreadTransanction.execute("async set ParentViewLayout repeat", runnable);
		}
		
	}
	
	
	protected void asyncSet() {
		
	}
	
	
	
	protected void dispatchDrawBackground(Canvas canvas)
	{
		if(color!=null) {
			canvas.drawColor(color);
		}

	}
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		
		
		
		for(int i = drawers.size() - 1; i >= 0; i--) {
			
			if(drawers.get(i).onTouchEvent(event))
			{
				return true;
			}
		}
		
		return super.onTouchEvent(event);
	}
	
	@Override
	protected void onDraw(Canvas canvas)
	{
		
		dispatchDrawBackground(canvas);
		
		while(drawersAsync.size()!=0) {
			drawers.add(drawersAsync.remove(0));
		}
		
		for(ViewDrawer drawer:drawers) {
			drawer.draw(canvas);
		}
		
		for(ViewDrawer drawer:drawers) {
			drawer.drawAfter(canvas);
		}
		
	}
	
	public void addDrawer(ViewDrawer drawer) {
		
		if(Looper.getMainLooper() == Looper.myLooper()) {
			drawers.add(drawer);
		}
		else {
			drawersAsync.add(drawer);
		}
		
		
	}
	
	public void removeDrawer(ViewDrawer drawer) {
		drawers.remove(drawer);
	}

}
