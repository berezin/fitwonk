package common.painter;

import java.util.ArrayList;
import java.util.HashMap;

import com.utils.CanvasUtils;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Rect;
import android.graphics.Region.Op;

public class RippleAddon
{

	public static class RippleController {
		
		
		public static Integer DEFAULT_COLOR = 0xffaaaaaa;
		public static Integer DEFAULT_DURATION = 300;
		
		
		private ArrayList<RippleAddon> actives;
		private ViewDrawer view;
		private boolean isClip = true;
		private int color = DEFAULT_COLOR;
		private long dur = DEFAULT_DURATION;
		private boolean isActiveLight = false;
		private boolean isActiveClick = false;
		private boolean isClickDelayed = false;
		
		public RippleController(ViewDrawer view) {
			this.view = view;
			actives = new ArrayList<>();
		}
		
		public void setActive(boolean isActive) {
			this.isActiveClick = isActive;
			this.isActiveLight = isActive;
		}
		
		public void setActive(boolean isActiveLight, boolean isActiveClick) {
			this.isActiveClick = isActiveClick;
			this.isActiveLight = isActiveLight;
		}
		
		public void setClickDelayed(boolean isClickDelayed) {
			this.isClickDelayed = isClickDelayed;
		}
		
		public boolean isClickDelayed() {
			return isClickDelayed;
		}
		
		public boolean isActiveClick() {
			return isActiveClick;
		}
		
		public boolean isActiveLight() {
			return isActiveLight;
		}
		
		public boolean hasRipples() {
			return actives.size()!=0;
		}
		
		public void setClip(boolean isClip) {
			this.isClip = isClip;
		}
		
		public void setColor(int color) {
			this.color = color;
		}
		
		public void setDuration(long dur) {
			this.dur = dur;
		}
		
		public void draw(Canvas canvas) {
			
			if(actives.size() == 0 || view == null) {
				return;
			}
			
			boolean isNeedInvalidate = false;
			
			for(int i = 0; i < actives.size(); i++) {
				
				RippleAddon a = actives.get(i);
				
				a.increase(actives.size() - i - 1);
				
				if(!a.draw(canvas, view.getBound())) {
					actives.remove(i--);
				}
				else {
					isNeedInvalidate = true;
				}
				
			}
			
			if(isNeedInvalidate) {
				view.parent.invalidate();
			}

		}
		
		public void add(float x, float y) {
			
			RippleAddon ripple = new RippleAddon();
			ripple.start(x, y);
			ripple.setClip(isClip);
			ripple.setColor(color);
			ripple.setDuration(dur);
			
			actives.add(ripple);
			
			view.parent.invalidate();
		}
		
		public void addLight(float x, float y) {
			
			RippleAddon ripple = new RippleAddon();
			ripple.start(x, y);
			ripple.setClip(isClip);
			ripple.setColor(color);
			ripple.setAlpha(30);
			ripple.setDuration(250);
			
			actives.add(ripple);
			
			view.parent.invalidate();
		}

		public long getDuration()
		{
			return dur;
		}

		public void clear()
		{
			actives.clear();
		}
		
		
	}
	

	Paint paint = new Paint() {{
		setAntiAlias(false);
	}};
	private float duration = 300;
	private float position;
	private int maxAlpha = 200;
	private float centerX;
	private float centerY;
	private float rMax;
	private boolean isClip = true;
	private Long lastDraw = 0L;
	private int color;
	
	private RippleAddon() {
		position = 0;
	}
	
	public void increase(int i)
	{
		duration -= i;
	}

	public void setClip(boolean isClip)
	{
		this.isClip = isClip;
	}

	public void setColor(int color) {
	
		this.color = color;
		paint.setColor(color);
		maxAlpha = paint.getAlpha();
	}
	
	public void setAlpha(int alpha) {
		maxAlpha = alpha;
	}
	
	public int getAlpha() {
		return maxAlpha;
	}
	
	public void start(float x, float y) {
		lastDraw = null;
		centerX = x;
		centerY = y;
		position = 0;
	}
	
	public void setDuration(long duration) {
		this.duration = duration;
	}
	
	
	static HashMap<Integer, Bitmap> bitmaps = new HashMap<>();
	static Rect rect = new Rect();
	static Paint paintBitmap = new Paint() {{
		setAntiAlias(true);
	}};
	public boolean draw(Canvas canvas, RectF bound) {

		Bitmap bitmap = bitmaps.get(color);
		
		if(bitmap==null) {
			bitmap = Bitmap.createBitmap(50, 50, Config.ARGB_4444);
			Canvas c = new Canvas(bitmap);
			
			paintBitmap.setColor((255 << 24) | ( ( (color >> 16) & 0xFF ) << 16) | ( ((color >> 8) & 0xFF ) << 8) | (color & 0xFF));
			c.drawCircle(25, 25, 24, paintBitmap);
			
			bitmaps.put(color, bitmap);

		}
		
		if(lastDraw == null) {
			rMax = (float) (1.2f * Math.sqrt(Math.pow(bound.width(), 2) + Math.pow(bound.height(), 2)));
		}
		else {
			position += (System.currentTimeMillis() - lastDraw);
		}
		
		
		float k = Math.min(1f, position / duration);

		float r =  rMax * k;

		lastDraw = System.currentTimeMillis();
		
		if(position > duration) {
			return false;
		}
		else {
			
			canvas.save(Canvas.CLIP_SAVE_FLAG);
			
			if(isClip) {
				canvas.clipRect(bound, Op.INTERSECT);
			}
			else {
				canvas.clipRect(new RectF(0, 0, canvas.getWidth(), canvas.getHeight()), Op.REPLACE);
			}
			
			int alpha = (int) (maxAlpha - maxAlpha * k);
			//paint.setAlpha(alpha);
			rect.left = (int) (centerX - r);
			rect.right = (int) (centerX + r);
			rect.top = (int) (centerY - r);
			rect.bottom = (int) (centerY + r);
			
			CanvasUtils.DrawImage(canvas, bitmap, rect, true,true, alpha, false);
			//canvas.drawRect(rect, paint);
			//canvas.drawPath(path, paint);
			
			canvas.restore();
			
		}
		
		return true;
	}
	
	public boolean draw2(Canvas canvas, ViewDrawer drawer, RectF bound) {

		if(lastDraw == null) {
			rMax = (float) (1.1f * Math.sqrt(Math.pow(bound.width(), 2) + Math.pow(bound.height(), 2)));
		}
		else {
			position += (System.currentTimeMillis() - lastDraw);
		}
		
		float r =  rMax * position / duration;

		lastDraw = System.currentTimeMillis();
		
		if(position > duration) {
			return false;
		}
		else {
			canvas.save(Canvas.CLIP_SAVE_FLAG);
			
			if(isClip) {
				canvas.clipRect(bound, Op.INTERSECT);
			}
			else {
				canvas.clipRect(new RectF(0, 0, canvas.getWidth(), canvas.getHeight()), Op.REPLACE);
			}
			int alpha = (int) Math.max(0, maxAlpha - maxAlpha * position / duration);
			paint.setAlpha(alpha);
			canvas.drawCircle(centerX, centerY, r, paint);
			//canvas.drawPath(path, paint);
			
			canvas.restore();
		}
		
		return true;
	}
	
}
