package common.painter;

import android.graphics.Canvas;
import android.graphics.RectF;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.Layout.Alignment;
import android.view.View;
import android.view.View.OnClickListener;

public class TextDrawer extends ViewDrawer
{

	
	
	
	StaticLayout sl;
	Integer width;
	float kAlpha = 1f;
	Integer colorText;
	
	public TextDrawer(View parent) {
		super(parent);
	}
	
	public void set(CharSequence str, int width, TextPaint paint, Alignment align) {
		layout = null;
		this.width = width;
		sl = new StaticLayout(str, paint, width, align, 1f, 0f, false);
	}
	
	public void set(CharSequence str, int width, TextPaint paint) {
		layout = null;
		this.width = width;
		sl = new StaticLayout(str, paint, width, Alignment.ALIGN_NORMAL, 1f, 0f, false);
	}
	
	public void set(CharSequence str, TextPaint paint) {
		layout = null;
		sl = new StaticLayout(str, paint, Integer.MAX_VALUE, Alignment.ALIGN_NORMAL, 1f, 0f, false);
		width = (int) sl.getLineWidth(0);
	}
	
	public void setTextColor(Integer colorText) {
		this.colorText = colorText;
	}
	
	public TextDrawer(View parent, CharSequence str, int width, TextPaint paint, Alignment align)
	{
		super(parent);
		set(str, width, paint, align);
	}
	
	public TextDrawer(View parent, CharSequence str, int width, TextPaint paint)
	{
		this(parent, str, width, paint, Alignment.ALIGN_NORMAL);
	}
	
	public TextDrawer(View parent, CharSequence str, TextPaint paint)
	{
		super(parent);
		set(str, paint);
	}
	
	@Override
	public Layout layout() {
		
		Layout l = super.layout();
		
		l.height = Height();
		l.width = Width();
		
		return layout;
	}
	
	
	
	
	public int Height() {
		return sl.getHeight() + padTop + padBottom;
	}
	
	public int Width() {
		return width + padLeft + padRight;
	}
	
	
	@Override
	public void setAlpha(int alpha)
	{
		super.setAlpha(alpha);
		
		kAlpha = alpha / 255f;
	}
	
	@Override
	public void draw(Canvas canvas)
	{
		super.draw(canvas);
		
		if(isVisible && sl!=null) {
			
			RectF bound = getBound();
			
			canvas.save();
			canvas.translate(bound.left + padLeft, bound.top + padTop);
			int a = 255;
			int c = 0;
			
			if(colorText!=null) {
				c = sl.getPaint().getColor();
				sl.getPaint().setColor(colorText);
			}
			
			if(alpha!=255) {
				a = sl.getPaint().getAlpha();
				sl.getPaint().setAlpha((int) (a * kAlpha));
			}
			
			
			sl.draw(canvas);
			
			if(alpha!=255) {
				sl.getPaint().setAlpha(a);
			}
			
			if(colorText!=null) {
				sl.getPaint().setColor(c);
			}
			
			
			canvas.restore();
		}
	}

	
	
	
	
	

}
