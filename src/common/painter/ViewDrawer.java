package common.painter;

import com.azumio.android.common.Log;
import com.utils.Dimen;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Looper;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import common.painter.RippleAddon.RippleController;

public class ViewDrawer
{

	protected Integer color;
	protected Integer colorHighlight;
	protected RippleController rippleController;

	protected View parent;
	protected int alpha = 255;

	public ViewDrawer(View parent)
	{
		this.parent = parent;
		rippleController = new RippleController(this);

	}

	public RippleController getRipple()
	{
		return rippleController;
	}

	public void onAttach()
	{

	}

	public void onDetach()
	{
		rippleController.clear();
	}

	public void setAlpha(int alpha)
	{
		this.alpha = alpha;
	}

	public int getAlpha()
	{
		return alpha;
	}

	public void setPadding(int pad)
	{
		setPadding(pad, pad, pad, pad);
	}

	int padTop = 0;
	int padBottom = 0;
	int padLeft = 0;
	int padRight = 0;

	public void setPadding(int left, int top, int right, int bottom)
	{
		padTop = top;
		padBottom = bottom;
		padLeft = left;
		padRight = right;
	}

	public void setBackgroundColor(int color)
	{
		if(Color.alpha(color) == 0)
		{
			this.color = null;
		} else
		{
			this.color = color;
		}

		if(Looper.getMainLooper() == Looper.myLooper())
		{
			parent.invalidate();
		}

	}

	public void setBackgroundColor(int color, int colorHighlight)
	{

		this.color = color;
		this.colorHighlight = colorHighlight;

		if(Looper.getMainLooper() == Looper.myLooper())
		{
			parent.invalidate();
		}

	}

	public RectF getBound()
	{
		return layout().getBound();
	}

	static Paint paintBackground = new Paint();

	public void draw(Canvas canvas)
	{

		if(isVisible)
		{

			layout().validateBound();

			if(color != null)
			{
				paintBackground.setAlpha(alpha);
				paintBackground.setColor(color);
				canvas.drawRect(getBound(), paintBackground);
			}

			if(rippleController.hasRipples())
			{
				rippleController.draw(canvas);
			}

		}
	}

	public void drawAfter(Canvas canvas)
	{

	}

	boolean isVisible = true;

	public void show()
	{
		isVisible = true;

		if(Looper.getMainLooper() == Looper.myLooper())
		{
			parent.invalidate();
		}

	}

	public void hide()
	{
		isVisible = false;
		parent.invalidate();
	}

	public static class Layout
	{

		final public static int MATCH = -1;
		int width = MATCH;
		int height = MATCH;
		int[] margins = { 0, 0, 0, 0 };
		boolean isRight = false;
		boolean isBottom = false;
		ViewDrawer view;

		private Layout(ViewDrawer view, int width, int height)
		{
			this.view = view;
			this.height = height;
			this.width = width;
		}

		public Layout right()
		{
			needBound = true;
			isRight = true;

			return this;
		}

		public Layout bottom()
		{
			needBound = true;
			isBottom = true;

			return this;
		}

		public Layout marginTop(int margin)
		{
			needBound = true;
			this.margins[1] = margin;

			return this;
		}

		public Layout marginBottom(int margin)
		{
			needBound = true;
			this.margins[3] = margin;

			return this;
		}

		public Layout marginLeft(int margin)
		{
			needBound = true;
			this.margins[0] = margin;

			return this;
		}

		public Layout marginRight(int margin)
		{
			needBound = true;
			this.margins[2] = margin;

			return this;
		}

		public Layout margin(int margin)
		{
			needBound = true;
			for (int i = 0; i < 4; i++)
			{
				this.margins[i] = margin;
			}

			return this;
		}

		public Layout margins(int... margins)
		{
			needBound = true;
			for (int i = 0; i < margins.length; i++)
			{
				this.margins[i] = margins[i];
			}

			return this;
		}

		public Layout calcLineH(int height)
		{
			needBound = true;
			this.height = height;
			this.width = MATCH;

			return this;
		}

		public Layout calcLineV(int width)
		{
			needBound = true;
			this.height = MATCH;
			this.width = width;

			return this;
		}

		int lastParentWidth = 0;
		int lastParentHeight = 0;
		boolean needBound = true;
		private RectF bound = new RectF();

		public int getParentWidth()
		{

			int w;

			if(view.parent instanceof ILayoutParent)
			{
				ILayoutParent lp = (ILayoutParent) view.parent;
				w = lp.FixWidth();
			} else
			{
				w = view.parent.getMeasuredWidth();
			}

			return w;
		}

		public int getParentHeight()
		{

			int h;

			if(view.parent instanceof ILayoutParent)
			{
				ILayoutParent lp = (ILayoutParent) view.parent;
				h = lp.FixHeight();
			} else
			{
				h = view.parent.getMeasuredHeight();
			}

			return h;
		}

		public void validateBound()
		{
			int w = getParentWidth();
			int h = getParentHeight();

			if(lastParentWidth != w || lastParentHeight != h)
			{
				needBound = true;
			}
		}

		public RectF getBound()
		{

			if(needBound)
			{

				int w = getParentWidth();
				int h = getParentHeight();

				if(width == MATCH)
				{
					bound.left = margins[0];
					bound.right = w - margins[2];
				} else
				{

					if(isRight)
					{
						bound.right = w - margins[2];
						bound.left = bound.right - width;
					} else
					{
						bound.left = margins[0];
						bound.right = width + bound.left;
					}
				}

				if(height == MATCH)
				{
					bound.top = margins[1];
					bound.bottom = h - margins[3];
				} else
				{

					if(isBottom)
					{
						bound.bottom = h - margins[3];
						bound.top = bound.bottom - height;
					} else
					{
						bound.top = margins[1];
						bound.bottom = height + bound.top;
					}
				}

			}

			return bound;
		}

		public Layout belowOf(Layout layout)
		{
			if(!isBottom)
			{
				margins[1] += layout.getBound().bottom;
			}

			return this;
		}

		public Layout rightOf(Layout layout)
		{

			if(!isRight)
			{
				margins[0] += layout.getBound().right;
			}

			return this;
		}

		public Layout calcCenterV(Integer forHeight)
		{
			if(isBottom)
			{
				margins[3] -= (forHeight - height) / 2;
			} else
			{
				margins[1] += (forHeight - height) / 2;
			}

			return this;
		}

		public Layout calcCenterH(Integer forWidth)
		{

			if(isRight)
			{
				margins[2] -= (forWidth - width) / 2;
			} else
			{
				margins[0] += (forWidth - width) / 2;
			}

			return this;
		}

		public static void calcCenterLayoutsV(int forHeight, Layout... layouts)
		{
			int i = 0;
			Layout last = null;
			for (Layout l : layouts)
			{

				if(i == 0)
				{

					int h = 0;

					for (Layout l2 : layouts)
					{
						h += l2.height;
					}
					int hSave = l.height;
					l.height = h;

					l.calcCenterV(forHeight);

					l.height = hSave;

				} else
				{
					l.belowOf(last);
				}

				i++;
				last = l;
			}

		}

	}

	public static interface ILayoutParent
	{

		int FixWidth();

		int FixHeight();

	}

	protected Layout layout;

	public Layout layout()
	{

		if(layout == null)
		{
			layout = new Layout(this, Layout.MATCH, Layout.MATCH);
		}

		return layout;
	}

	public Layout layout(int w, int h)
	{

		if(layout == null)
		{
			layout = new Layout(this, w, h);
		}

		return layout;
	}

	private float eventX = 0;
	private float eventY = 0;
	private boolean isTouchMe = false;
	private boolean isPerformClick = false;

	public boolean onTouchEvent(MotionEvent event)
	{

		boolean isDown = event.getAction() == MotionEvent.ACTION_DOWN;
		boolean isUp = event.getAction() == MotionEvent.ACTION_UP;
		boolean isCancel = event.getAction() == MotionEvent.ACTION_CANCEL;
		boolean isMove = event.getAction() == MotionEvent.ACTION_MOVE;

		if(isDown)
		{
			isTouchMe = false;
		}

		boolean isInRect = isTouchMe
				|| (isDown && getBound().contains(event.getX(), event.getY()));

		if(isInRect)
		{

			if(isUp)
			{

				float eX = event.getX();
				float eY = event.getY();

				if(isTouchMe && Math.abs(eX - eventX) < Dimen.d10
						&& Math.abs(eY - eventY) < Dimen.d10)
				{

					if(!isPerformClick)
					{

						if(rippleController.isActiveClick())
						{
							rippleController.add(eventX, eventY);
						}

						if(onClickListener != null)
						{

							if(rippleController.isClickDelayed())
							{

								isPerformClick = true;
								parent.postDelayed(new Runnable()
								{

									@Override
									public void run()
									{
										isPerformClick = false;
										onClickListener.onClick(parent);
									}
								}, rippleController.getDuration() + 100);
							} else
							{
								onClickListener.onClick(parent);
							}

						}
					}

					if(onClickListener != null)
						return true;
				}

			} else if(isDown)
			{

				isTouchMe = true;
				eventX = event.getX();
				eventY = event.getY();

				if(rippleController.isActiveLight())
				{
					rippleController.addLight(eventX, eventY);
				}

				if(onClickListener != null)
				{

					return true;
				}
			} else if(isMove)
			{

			}

		}

		if(isUp || isCancel)
		{

			if(isTouchMe)
			{

			}

			isTouchMe = false;
		}

		return false;
	}

	private OnClickListener onClickListener;

	public void setOnClickListener(OnClickListener onClickListener)
	{
		rippleController.setActive(onClickListener != null);
		this.onClickListener = onClickListener;
	}

}
