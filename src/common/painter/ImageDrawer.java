package common.painter;

import com.utils.CanvasUtils;
import com.utils.image.Const;
import com.utils.ui.IImage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Looper;
import android.view.View;

public class ImageDrawer extends ViewDrawer implements IImage
{

	protected Bitmap bitmap;
	protected Rect rectSrc;
	protected Boolean is_crop = true;
	protected Rect rectImg = new Rect();
	protected Rect rectForDawing = null;
	protected boolean isFilterBitmap = true;
	protected Boolean isXferModeSrc = false;
	
	public ImageDrawer(View parent) {
		super(parent);
	}
	
	public Bitmap getBitmap() {
		return bitmap;
	}
	
	public boolean hasBitmap() {
		return bitmap != null;
	}
	
	@Override
	public void setPadding(int left, int top, int right, int bottom)
	{
		rectForDawing = null;
		super.setPadding(left, top, right, bottom);
	}
	
	@Override
	public void draw(Canvas canvas)
	{
		super.draw(canvas);
		
		if(isVisible && bitmap != null)
		{

			if(rectForDawing == null) {
				RectF bound = getBound();
				
				rectImg.left = (int) bound.left + padLeft;
				rectImg.right = (int) bound.right - padRight;
				rectImg.top = (int) bound.top + padTop;
				rectImg.bottom = (int) bound.bottom - padBottom;

				rectForDawing = CanvasUtils.DrawImage(canvas, bitmap,
						rectImg, is_crop, isFilterBitmap, alpha, isXferModeSrc);

			} else
			{
				CanvasUtils.DrawImage(canvas, bitmap, rectImg,
						rectForDawing, rectSrc, isFilterBitmap, alpha,
						isXferModeSrc);
			}

		} 
		
	}

	@Override
	public void Set(Bitmap b, Boolean is_crop)
	{
		if(b == bitmap)
		{
			return;
		}

		bitmap = b;

		rectSrc = new Rect(0, 0, b.getWidth(), b.getHeight());

		setTag("");

		rectForDawing = null;

		this.is_crop = is_crop;

		if(Looper.getMainLooper() == Looper.myLooper())
		{
			parent.invalidate();
		} else
		{
			parent.postInvalidate();
		}
	}

	@Override
	public void Clear()
	{
		bitmap = null;
		
		rectForDawing = null;

		setTag("");

		parent.invalidate();
	}

	Object tag;
	@Override
	public void setTag(Object tag)
	{
		this.tag = tag;
	}

	@Override
	public Object getTag()
	{
		return tag;
	}

	@Override
	public void onAsyncSet(Bitmap bitmap)
	{
		
	}

	@Override
	public void onAnimate()
	{
		
	}


	@Override
	public Context getContext()
	{
		return parent.getContext();
	}

}
