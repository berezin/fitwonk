package common.painter;

import java.util.ArrayList;

import com.utils.ui.ViewRelativeLayout;

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;
import common.painter.ViewDrawer.ILayoutParent;

public abstract class ParentRelativeLayout extends ViewRelativeLayout implements ILayoutParent
{

	ArrayList<ViewDrawer> drawers = new ArrayList<>();
	
	@Override
	protected void onAttachedToWindow()
	{
		super.onAttachedToWindow();
		
		for(ViewDrawer drawer:drawers) {
			
			drawer.onAttach();
		}
	}
	
	@Override
	protected void onDetachedFromWindow()
	{
		super.onDetachedFromWindow();
		
		for(ViewDrawer drawer:drawers) {
			
			drawer.onDetach();
		}
	}
	
	public ParentRelativeLayout(Context context)
	{
		super(context);
		
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		
		for(ViewDrawer drawer:drawers) {
			
			if(drawer.onTouchEvent(event))
			{
				return true;
			}
		}
		
		return super.onTouchEvent(event);
	}
	
	@Override
	protected void dispatchDraw(Canvas canvas)
	{
		
		super.dispatchDraw(canvas);
		
		for(ViewDrawer drawer:drawers) {
			drawer.draw(canvas);
		}
		
	}
	
	public void addDrawer(ViewDrawer drawer) {
		drawers.add(drawer);
	}
	
	public void removeDrawer(ViewDrawer drawer) {
		drawers.remove(drawer);
	}

}
