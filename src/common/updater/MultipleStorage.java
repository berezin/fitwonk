package common.updater;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import com.utils.setting.SettingHelper.Storage;
import com.utils.shorts.E;

public class MultipleStorage<IndexClass extends Serializable, DataClass extends Serializable>
{

	final Integer SETTING;
	
	final Storage<ArrayList<IndexClass>> indexes;
	final HashMap<IndexClass, Storage<DataClass>> mapStorages;
	
	public MultipleStorage(String name) {
		
		SETTING = E.Id(name);
		indexes = new Storage<ArrayList<IndexClass>>(SETTING, new ArrayList<IndexClass>());
		mapStorages = new HashMap<>();
	}
	
	
	public Storage<DataClass> getStorage(IndexClass index) {
		
		Storage<DataClass> storage = mapStorages.get(index);
		
		if(storage==null) {
			
			if(!indexes.Get().contains(index)) {
				indexes.Get().add(index);
			}
			
			storage = new Storage<>(SETTING + 1 + indexes.Get().indexOf(index));
			mapStorages.put(index, storage);
		}
		
		return storage;
	}
	
	
	
	
	
}
