package common.updater;

import java.io.Serializable;
import java.util.ArrayList;

import com.utils.ThreadTransanction;
import com.utils.ThreadTransanction.ThreadRunnable;
import com.utils.ThreadTransanction.ThreadStatus;
import com.utils.parser.ParserUtils.JSONParser;
import com.utils.parser.ParserUtils.JSONParser.Post;
import com.utils.setting.SettingHelper.Storage;
import com.utils.shorts.E;

import android.util.Log;

public abstract class Request<D extends Serializable>
{

	final public static Integer STORAGE_TYPE_NOTHING = 0;
	final public static Integer STORAGE_TYPE_SINGLE = 1;
	final public static Integer STORAGE_TYPE_MULTIPLE_BY_ARGS = 2;

	final public Integer EVENT_OK;
	final public Integer EVENT_ERROR;
	private String name;
	private Class<D> cl;
	
	private MultipleStorage<Integer, D> multiStorage;
	private Storage<D> singleStorage;
	private ArrayList<Responce<D>> requestStory = new ArrayList<>();
	
	private ArrayList<Integer> activeTokens = new ArrayList<>();
	
	public Request(Class<D> cl, String name)
	{
		this.cl = cl;
		this.name = name;
		EVENT_OK = E.Id(name + ".EVENT_SEND_OK");
		EVENT_ERROR = E.Id(name + ".EVENT_SEND_ERROR");
	}
	
	final public Responce<D> getLastRequest()
	{
		if(hasRequestStory() && requestStory.size()>0) {
			return requestStory.get(requestStory.size() - 1);
		}
		
		return null;
	}

	final public Integer TryLast()
	{
		if(hasRequestStory() && requestStory.size()>0) {
			return Send(requestStory.get(requestStory.size() - 1).args);
		}
		
		return 0;
	}
	
	final public Integer Send(final Object... args)
	{

		final Integer token = getToken(args);

		if(activeTokens.contains(token))
		{
			return token;
		}

		activeTokens.add(token);

		ThreadTransanction.execute_http("send " + getClass(),
				new ThreadRunnable<Responce<D>>()
				{

					@Override
					public void result(ThreadStatus aStatus, Responce<D> aResult)
					{

						if(aResult.isOk)
						{
							onSuccess(aResult);
							onSave(aResult);
							E.Event(EVENT_OK, aResult);
						}
						else
						{
							onError(aResult);
							E.Event(EVENT_ERROR, aResult);
						}

						if(hasRequestStory()) {
							requestStory.add(aResult);
						}
						
						activeTokens.remove((Object) token);

					}

					@Override
					public Responce<D> run()
					{
						Responce<D> r = new Responce<D>();
						r.token = token;
						r.args = args;

						try
						{
							r.data = sendSync(args);
							r.isOk = r.data != null && isSuccess(r);
						} catch (Exception e)
						{
							e.printStackTrace();
						}

						return r;
					}
				});

		return token;

	}

	protected void onError(Responce<D> result)
	{

	}

	protected void onSuccess(Responce<D> result)
	{

	}
	
	
	
	protected boolean isSuccess(Responce<D> result)
	{
		return true;
	}
	
	@SuppressWarnings("unchecked")
	final public D getData(Object param)
	{
		
		if(param!=null && param instanceof Responce<?>) {
			return ((Responce<D>)param).data;
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	final public Responce<D> getResult(Object param)
	{
		
		if(param!=null && param instanceof Responce<?>) {
			return ((Responce<D>)param);
		}
		
		return null;
	}
	
	final public D standartGet(String url)
	{
		
		try
		{
			
			Log.w(name, url);
			
			D data = cl.newInstance();
			
			if(JSONParser.Parse(data, url)) {
				return data;
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}
	
	final public D standartPost(String url, Post p)
	{
		
		try
		{
			D data = cl.newInstance();
			
			if(JSONParser.ParsePost(data, url, p.get())) {
				return data;
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}
	
	final public D standartPut(String url, Post p)
	{
		
		try
		{
			D data = cl.newInstance();
			
			if(JSONParser.ParsePut(data, url, p.get())) {
				return data;
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

	final protected void onSave(Responce<D> r)
	{

		Integer type = getCacheType();

		if(type.equals(STORAGE_TYPE_NOTHING))
		{
			return;
		}

		D data = getCacheData(r.args);
		
		if(data == null) {
			data = r.data;
		}
		else {
			data = onMerge(data, r);
		}
		
		getCacheStorage(r.args).Save(data);
	}
	
	protected D onMerge(D oldData, Responce<D> r) {
		
		return r.data;
	}

	protected Integer getCacheType()
	{
		return STORAGE_TYPE_NOTHING;
	}
	
	protected boolean hasRequestStory()
	{
		return false;
	}
	
	final public void clearCacheData(Object... args)
	{
		Storage<D> storage = getCacheStorage(args);
	
		if(storage!=null)
			storage.Clear();
	}
	
	final public D getCacheData(Object... args) {
		
		Storage<D> storage = getCacheStorage(args);
		
		return storage!=null?storage.Get():null;
	}

	final public Storage<D> getCacheStorage(Object... args)
	{

		if(getCacheType().equals(STORAGE_TYPE_MULTIPLE_BY_ARGS))
		{
			if(multiStorage == null) {
				multiStorage = new MultipleStorage<>(name);
			}
			
			return multiStorage.getStorage(getToken(args));
		}
		else if(getCacheType().equals(STORAGE_TYPE_SINGLE))
		{
			if(singleStorage == null) {
				singleStorage = new Storage<>(E.Id(name));
			}
			
			return singleStorage;
		}

		return null;

	}

	final protected Integer getToken(Object... args)
	{
		int token = 0;

		for (int i = 0; i < args.length; i++)
		{
			if(args[i]!=null)
				token += args[i].hashCode();
		}

		return token;
	}

	protected abstract D sendSync(Object... args);

	final public boolean hasOKEvent(Integer event)
	{
		return EVENT_OK.equals(event);
	}

	final public boolean hasErrorEvent(Integer event)
	{
		return EVENT_ERROR.equals(event);
	}

	public static class Responce<D>
	{
		public Integer token;
		public boolean isOk = false;
		public Object[] args = null;
		public D data;
	}

	public boolean IsEmpty(Object... args)
	{
		return getCacheData(args) == null;
	}

	

}
