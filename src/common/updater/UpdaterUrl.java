package common.updater;

import java.io.Serializable;
import com.utils.parser.ParserUtils.JSONParser;

public abstract class UpdaterUrl<D extends Serializable> extends Updater<D>
{
	
	public UpdaterUrl(Class<D> cl)
	{
		super(cl);
	}

	public abstract String getUrl();
	
	@Override
	D requestDataSync()
	{
		
		try
		{
			D d = cl.newInstance();
			
			if(JSONParser.Parse(d, getUrl())) {
				
				return d;
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
	}

	
}
