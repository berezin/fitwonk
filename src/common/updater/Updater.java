package common.updater;

import java.io.Serializable;

import com.utils.ThreadTransanction;
import com.utils.ThreadTransanction.ThreadRunnable;
import com.utils.ThreadTransanction.ThreadStatus;
import com.utils.setting.SettingHelper.Storage;
import com.utils.shorts.E;

public abstract class Updater<D extends Serializable>
{

	public Integer SETTING;
	public Integer EVENT_ERROR;
	
	protected Storage<D> STORAGE;
	Class<D> cl;
	
	public Updater(Class<D> cl) {
		
		this.cl = cl;
		SETTING = E.Id(getStorageSettingName());
		EVENT_ERROR = SETTING+1;
		
		STORAGE = new Storage<D>(SETTING) {{
			setGZIP(true);
		}};
		
	}
	
	public String getStorageSettingName() {
		return cl.getCanonicalName();
	}
	
	
	abstract D requestDataSync();

	boolean isRequest = false;

	public void requestData()
	{

		if(isRequest)
		{
			return;
		}

		isRequest = true;

		ThreadTransanction.execute_http("requestData "+getStorageSettingName(), new ThreadRunnable<D>()
		{

			@Override
			public void result(ThreadStatus aStatus, D aResult)
			{
				isRequest = false;

				if(aResult != null) {
					STORAGE.Save(aResult);
				}
				else {
					E.Event(EVENT_ERROR);
				}
			}

			@Override
			public D run()
			{
				return requestDataSync();
			}
		});

	}
	
	public boolean hasChangedEvent(Integer event) {
		return SETTING.equals(event);
	}
	
	public boolean hasErrorEvent(Integer event) {
		return EVENT_ERROR.equals(event);
	}

	public D getData()
	{
		return STORAGE.Get();
	}
	
	public void clear()
	{
		STORAGE.Clear();
	}
	
	public void save()
	{
		STORAGE.Save();
	}
	
}
