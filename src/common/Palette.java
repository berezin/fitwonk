package common;

public class Palette
{


	public static Integer BLUE = 0xff2275bb;
	
	public static Integer BLUE_RIPPLE = 0x882275bb;
	public static Integer WHITE_RIPPLE = 0x88ffffff;
	
	public static Integer DARK_BLUE = 0xff1b5c92;
	
	public static Integer GRAY = 0xff999999;
	
	public static Integer DARK_GRAY = 0xff444444;
	
	public static Integer GRAY_BACKGROUND = 0xfff5f6f8;
	
}
